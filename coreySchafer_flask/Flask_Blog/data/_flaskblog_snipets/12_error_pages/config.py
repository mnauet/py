import os

class Config:
    # SECRET_KEY = 'f8fd49081affe715d0ca75c1efadf716'   exported to ~/.bashrc file
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')

    MAIL_SERVER = 'smtp.googlemail.com'
    #MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587  #TLS
    MAIL_USE_TLS = True
    #MAIL_PORT = 465   # SSL
    #MAIL_USE_SSL = True
    MAIL_USERNAME = os.environ.get('EMAIL_USER')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')
    #MAIL_DEFAULT_SENDER = ('Malik Akram', 'dev.malik.akram@gmail.com')
