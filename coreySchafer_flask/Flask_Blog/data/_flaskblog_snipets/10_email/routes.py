import secrets
import os
import json
from PIL import Image
from flask import  render_template, url_for, flash, redirect, request, abort
from flaskblog import app, db, bcrypt, mail
from flaskblog.forms import (RegistrationForm, LoginForm, UpdateAccountForm, PostForm,
                             RequestResetForm,ResetPasswordForm)
from flaskblog.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required
from flask_mail import Message




@app.route('/')
@app.route('/home')
def home():
    # url to input from browser
    # http://127.0.0.1:5000/?page=2
    page = request.args.get('page', 1, type=int)

    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page,per_page=5)
    return render_template('home.html', posts=posts)

@app.route('/about')
def about():
    return render_template('about.html', title='About')

@app.route('/test')
def test():
    return render_template('test.html', title='Test')

@app.route('/test2')
def test2():
    return render_template('test2.html', title='Test')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user :
            flash(f' email already exists, please try another one!', 'success')
        else:
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            user = User(username=form.username.data, email=form.email.data, password=hashed_password)
            db.session.add(user)
            db.session.commit()
            flash(f' your Account has been created, {form.username.data}, please login!', 'success')
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')    # p6/44:21
            return redirect(next_page) if next_page else redirect(url_for('home'))
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))

def save_picture(form_picture_data):
    random_hex = secrets.token_hex(2)
    f_name, f_ext = os.path.splitext(form_picture_data.filename)
    # _, f_ext = os.path.splitext(form_picture_data.filename)   # if a variable is not desired, use _
    picture_fn = random_hex + '_'+ f_name + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size=(125,125)
    i = Image.open(form_picture_data)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route('/account', methods = ['GET', 'POST'])
@login_required  # p6/39:10
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        # db.session.add(current_user)
        db.session.commit()
        flash ('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename = 'profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account', image_file=image_file, form=form)

@app.route('/post/new', methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('home'))
    elif request.method == 'GET':
        #form.title.data = cu
        #form.content.data =
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)

@app.route('/post/<int:post_id>/update', methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your message has been updated', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title= 'Update Post',
                           form=form, legend='Update Post')

@app.route('/post/<int:post_id>/delete', methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Post deleted successfully!', 'success')
    return redirect(url_for('home'))


@app.route('/post/load_json', methods=['GET', 'POST'])
def load_json():
# json file must be in Flask_Blog folder, not inside
    form = PostForm()
    fname = 'post.json'
    str_data = open(fname).read()
    json_data = json.loads(str_data)

    if form.validate_on_submit():
        for item in json_data:
            form.title.data = item["title"]
            form.content.data =item["content"]
            author = current_user
            post = Post(title=form.title.data, content=form.content.data, author=current_user)
            db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('home'))
    elif request.method == 'GET':
        form.title.data = 'post-data_1'
        form.content.data ='content-data-1'
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')

@app.route('/user/<string:username>')
def user_posts(username):
    # url to input from browser
    # http://127.0.0.1:5000/?page=2
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user)\
        .order_by(Post.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user)


@app.route('/show_json', methods=['GET', 'POST'])
def show_json():
    return flaskblog/post.json

def send_reset_email(user):
    # pass
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                  sender='mechatron@gmail.com',
                  recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('reset_token', token=token, _external=True)}

if you did not make this request then simply ignore this email and no change required.
'''
    mail.send(msg)


@app.route('/reset_password', methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        #EMAIL_USER = os.environ.get('EMAIL_USER')
        #EMAIL_PASS = os.environ.get('EMAIL_PASS')
        #flash(f'{EMAIL_USER} {EMAIL_PASS}')
        flash('An email is sent with instruction to reset your password', 'info')
        return redirect(url_for('login'))

    return render_template('reset_request.html', title='Reset Password', form=form)

@app.route('/reset_password/<token>', methods=['GET', 'POST'])  #17:28
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for(reset_request))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success' )
        return redirect(url_for('login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

'''
2020 08 15


2020 08 05

>>> from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
>>> s = Serializer('secret', 30)
>>> token = s.dumps({'user_id': 1 }).decode('utf-8')
>>> token
'eyJhbGciOiJIUzUxMiIsImlhdCI6MTU5NjgyMDMyMywiZXhwIjoxNTk2ODIwMzUzfQ.eyJ1c2VyX2lkIjoxfQ.FB1cME7mehaRlfS4PDxCRDIuG1OqqDig6J8TcEpWdDBcjcVVWMwvusiSZl0p-2ajkwdXhxVuVD0NT8kQyojBjQ'
>>> s.loads(token)
'''