import secrets
import os
import json
from PIL import Image
from flask import  render_template, url_for, flash, redirect, request, abort
from flaskblog import app,db,bcrypt
from flaskblog.forms import RegistrationForm, LoginForm, UpdateAccountForm, PostForm
from flaskblog.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required




@app.route('/')
@app.route('/home')
def home():
    # url to input from browser
    # http://127.0.0.1:5000/?page=2
    page = request.args.get('page', 1, type=int)

    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page,per_page=5)
    return render_template('home.html', posts=posts)

@app.route('/about')
def about():
    return render_template('about.html', title='About')

@app.route('/test')
def test():
    return render_template('test.html', title='Test')

@app.route('/test2')
def test2():
    return render_template('test2.html', title='Test')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user :
            flash(f' email already exists, please try another one!', 'success')
        else:
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            user = User(username=form.username.data, email=form.email.data, password=hashed_password)
            db.session.add(user)
            db.session.commit()
            flash(f' your Account has been created, {form.username.data}, please login!', 'success')
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')    # p6/44:21
            return redirect(next_page) if next_page else redirect(url_for('home'))
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))

def save_picture(form_picture_data):
    random_hex = secrets.token_hex(2)
    f_name, f_ext = os.path.splitext(form_picture_data.filename)
    # _, f_ext = os.path.splitext(form_picture_data.filename)   # if a variable is not desired, use _
    picture_fn = random_hex + '_'+ f_name + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size=(125,125)
    i = Image.open(form_picture_data)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route('/account', methods = ['GET', 'POST'])
@login_required  # p6/39:10
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        # db.session.add(current_user)
        db.session.commit()
        flash ('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename = 'profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account', image_file=image_file, form=form)

@app.route('/post/new', methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('home'))
    elif request.method == 'GET':
        #form.title.data = cu
        #form.content.data =
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)

@app.route('/post/<int:post_id>/update', methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your message has been updated', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title= 'Update Post',
                           form=form, legend='Update Post')

@app.route('/post/<int:post_id>/delete', methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Post deleted successfully!', 'success')
    return redirect(url_for('home'))


@app.route('/post/load_json', methods=['GET', 'POST'])
def load_json():
# json file must be in Flask_Blog folder, not inside
    form = PostForm()
    fname = 'post.json'
    str_data = open(fname).read()
    json_data = json.loads(str_data)

    if form.validate_on_submit():
        for item in json_data:
            form.title.data = item["title"]
            form.content.data =item["content"]
            author = current_user
            post = Post(title=form.title.data, content=form.content.data, author=current_user)
            db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('home'))
    elif request.method == 'GET':
        form.title.data = 'post-data_1'
        form.content.data ='content-data-1'
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')

@app.route('/user/<string:username>')
def user_posts(username):
    # url to input from browser
    # http://127.0.0.1:5000/?page=2
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user)\
        .order_by(Post.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user)


@app.route('/show_json', methods=['GET', 'POST'])
def show_json():
    return flaskblog/post.json
'''

https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/
paginate(page=None, per_page=None, error_out=True, max_per_page=None)
iter_pages(left_edge=2, left_current=2, right_current=5, right_edge=2)


>>> dir(post)
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__mapper__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__table__', '__tablename__', '__weakref__', '_decl_class_registry', '_sa_class_manager', '_sa_instance_state', 'author', 'content', 'date_posted', 'id', 'metadata', 'query', 'query_class', 'title', 'user_id']
>>> posts = Post.query.paginate()
>>> for post in posts.items:
>>>     print(post)
>>> posts.total
30
>>> posts.page
1
>>> posts.pages
2
>>> posts.items
[Post('My first post', '2020-08-03 12:47:30.109406'), Post('My first post', '2020-08-03 12:48:40.751603'), Post('post 4 from gmail', '2020-08-03 14:55:26.181717'), Post('Post 5 from gamil user - updated', '2020-08-03 17:03:28.474880'), Post('Post 6 from gamil user - updated', '2020-08-03 17:04:08.095832'), Post('aaaa', '2020-08-04 06:10:53.392897'), Post('My Updated Post', '2020-08-04 06:16:57.860280'), Post('A Second Post', '2020-08-04 06:16:57.860864'), Post('Top 5 Programming Lanaguages', '2020-08-04 06:16:57.861019'), Post('Sublime Text Tips and Tricks', '2020-08-04 06:16:57.861165'), Post('Best Python IDEs', '2020-08-04 06:16:57.861314'), Post('Flask vs Django - Which Is Better?', '2020-08-04 06:16:57.861458'), Post('You Won't Believe These Clickbait Titles!', '2020-08-04 06:16:57.861605'), Post('These Beers Will Improve Your Programming', '2020-08-04 06:16:57.861774'), Post('List of PyCon 2018 Talks', '2020-08-04 06:16:57.861930'), Post('How Dogs in the Workplace Boosts Productivity', '2020-08-04 06:16:57.862083'), Post('The Best Programming Podcasts', '2020-08-04 06:16:57.862224'), Post('Tips for Public Speaking', '2020-08-04 06:16:57.862363'), Post('Best Programmers Throughout History', '2020-08-04 06:16:57.862537'), Post('How To Create A YouTube Channel', '2020-08-04 06:16:57.862710')]

>>> post = Post.query.paginate(page=2)
>>> for p in post.items:
...     print(post)

>>> post = Post.query.paginate(per_page=5)
>>> for p in post.items:
...     print(p)

>>> post.page
1

>>> post = Post.query.paginate(per_page=5, page=2)
>>> for p in post.items:
...     print(p)

>>> post = Post.query.paginate(per_page=2, page=2)
>>> for p in post.items:
...     print(p)
... 
Post('post 4 from gmail', '2020-08-03 14:55:26.181717')
Post('Post 5 from gamil user - updated', '2020-08-03 17:03:28.474880')
>>> for page in post.iter_pages():
...     print(page)
... 
1
2
3
4
5
6
None
14
15
>>> 

>>> post.page
2
>>> for page in post.iter_pages(left_edge=1, right_edge=1, left_current=1,  right_current=2):
...     print(page)
... 
1
2
3
None
15
>>> 
>>> post.total
30
>>> post.items
[Post('post 4 from gmail', '2020-08-03 14:55:26.181717'), Post('Post 5 from gamil user - updated', '2020-08-03 17:03:28.474880')]



'''