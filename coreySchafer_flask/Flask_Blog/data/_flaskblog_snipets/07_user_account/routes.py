import secrets
import os
from PIL import Image
from flask import  render_template, url_for, flash, redirect, request
from flaskblog import app,db,bcrypt
from flaskblog.forms import RegistrationForm, LoginForm, UpdateAccountForm
from flaskblog.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required

posts = [
    {
        'author': 'Naveed',
        'title': 'Blog Post 1',
        'content': 'First Blog Post',
        'date_posted': 'July 24, 2020'
    },
    {
        'author': 'Akram',
        'title': 'Blog Post 2',
        'content': '2nd  Blog Post',
        'date_posted': 'July 24, 2020'
    }
]


@app.route('/')
@app.route('/home')
def home():
    return render_template('/home.html', posts=posts)

@app.route('/about')
def about():
    return render_template('about.html', title='About')

@app.route('/test')
def test():
    return render_template('test.html', title='Test')

@app.route('/test2')
def test2():
    return render_template('test2.html', title='Test')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user :
            flash(f' email already exists, please try another one!', 'success')
        else:
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            user = User(username=form.username.data, email=form.email.data, password=hashed_password)
            db.session.add(user)
            db.session.commit()
            flash(f' your Account has been created, {form.username.data}, please login!', 'success')
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')    # p6/44:21
            return redirect(next_page) if next_page else redirect(url_for('home'))
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))

def save_picture(form_picture_data):
    random_hex = secrets.token_hex(2)
    f_name, f_ext = os.path.splitext(form_picture_data.filename)
    # _, f_ext = os.path.splitext(form_picture_data.filename)   # if a variable is not desired, use _
    picture_fn = random_hex + '_'+ f_name + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size=(125,125)
    i = Image.open(form_picture_data)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route('/account', methods = ['GET', 'POST'])
@login_required  # p6/39:10
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        # db.session.add(current_user)
        db.session.commit()
        flash ('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename = 'profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account', image_file=image_file, form=form)