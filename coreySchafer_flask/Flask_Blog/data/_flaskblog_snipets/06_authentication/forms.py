from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import  DataRequired, Length, Email, EqualTo, ValidationError
from flaskblog.models import User

class RegistrationForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired(), Length(min=3, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):  # p6/14:30
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is taken. Please choose a different one')

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')




# Flask WTF, WT Forms, Validators documentation
#   https://flask-wtf.readthedocs.io/en/stable/
#   https://wtforms.readthedocs.io/en/2.3.x/            # API WTForms
#   https://wtforms.readthedocs.io/en/2.3.x/fields/     # API  fields
#   https://wtforms.readthedocs.io/en/2.3.x/validators/ # API  Validators
#   https://wtforms.readthedocs.io/en/2.3.x/widgets/    # API Widgets

#   https://flask.palletsprojects.com/en/1.1.x/patterns/flashing/

