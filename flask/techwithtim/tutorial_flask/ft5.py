from flask import  Flask, redirect, url_for, render_template, request,session
from datetime import timedelta

app = Flask(__name__)
app.secret_key = "malik"
app.permanent_session_lifetime = timedelta(days=5)
#  http://127.0.0.1:5000/login

@app.route('/')
def home():
    return render_template('index4.html')

@app.route("/login", methods= ["POST","GET"])
def login():
    if request.method == "POST":
        session.permanent = True
        user = request.form["nm"]
        session["usr"] = user
        return redirect(url_for("user"))
    else:
        if "usr" in session:
            return  redirect(url_for("user"))
        return  render_template("login.html")

@app.route("/user")
def user():
    if "usr" in session:
        user = session["usr"]
        return f"<h1>{user}</h1>"
    else:
        return redirect(url_for("login"))

@app.route("/logout")
def logout():
    session.pop("usr", None)
    return redirect(url_for("login"))


if __name__ == "__main__":
    app.run(debug = True)

'''
Flask Tutorial #5 - Sessions
https://www.youtube.com/watch?v=iIhAfX4iek0
n this flask tutorial I will discuss sessions and how they can be used to store data 
temporarily on the web server. I will also talk about creating permanent sessions 
and deleting old session data.

'''