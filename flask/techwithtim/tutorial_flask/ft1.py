from flask import  Flask, redirect, url_for
##Python #Flask #WebDevelopment
#Flask Tutorial #1 - How to Make Websites with Python
# https://www.youtube.com/watch?v=mqhxxeeTbu0
app = Flask(__name__)

@app.route('/')
def index():
    return " def index: Hallo Flask application <h1>Hallo<h1/>"

@app.route('/home')
def home():
    return '<h>def home: Home 1</h1>'

@app.route('/<name>')
def user(name):
    return f'Hallo {name}'

@app.route('/admin/')
def admin():
    return redirect(url_for('user', name = "redirect from Admin!"))

if __name__ == "__main__":
    app.run()

