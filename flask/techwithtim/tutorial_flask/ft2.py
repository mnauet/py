from flask import  Flask, redirect, url_for, render_template
# Flask Tutorial #2 - HTML Templates
# https://www.youtube.com/watch?v=xIgPMguqyws

app = Flask(__name__)

@app.route('/<name>')
def home(name):
    return render_template('index2.html', content=name, r=2, lst=['a','b','c'])


if __name__ == "__main__":
    app.run()

