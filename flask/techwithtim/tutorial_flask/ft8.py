from flask import  Flask, redirect, url_for, render_template, request,session,flash
from datetime import timedelta
from flask_sqlalchemy import SQLAlchemy
#  http://127.0.0.1:5000/login
app = Flask(__name__)
app.secret_key = "malik"
app.permanent_session_lifetime = timedelta(days=5)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.sqlite3'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class users(db.Model):
    _id = db.Column("id", db.INTEGER, primary_key=True)
    name = db.Column(db.TEXT(100))
    email = db.Column(db.TEXT(100))

    def __init__(self, name, email):
        self.name = name
        self.email = email

@app.route('/')
def home():
    if "usr" in session:
        user = session["usr"]
        flash("Already logged in")
    else:
        flash("You are not Logged in")
    return render_template('index4.html')

@app.route("/view")
def view():
    return render_template("view.html", values = users.query.all())


@app.route("/login", methods= ["POST","GET"])
def login():
    if request.method == "POST":
        session.permanent = True
        user = request.form["nm"]
        session["usr"] = user
        flash(f"Login Successful, {user} logged in")
        found_user = users.query.filter_by(name=user).first()
        if found_user:
            session["email"] = found_user.email
        else:
            rec = users(user, "")
            db.session.add(rec)
            db.session.commit()
        return redirect(url_for("user"))
    else:
        if "usr" in session:
            flash("Already logged in")
            return  redirect(url_for("user"))
        return  render_template("login.html")

@app.route("/user", methods = ["POST", "GET"])
def user():
    email = None
    if "usr" in session:
        user = session["usr"]
        if request.method == "POST":
            email = request.form["email"]
            session["email"] = email
            found_user = users.query.filter_by(name=user).first()
            found_user.email = email
            db.session.commit()
            flash(("email saved successfully"))
        else:
            if "email" in session:
                email = session["email"]
        return render_template("user.html", email=email)
    else:
        return redirect(url_for("login"))

@app.route("/logout")
def logout():

    flash("you have been logged out ", "info")
    session.pop("usr", None)
    session.pop("email", None)
    return redirect(url_for("login"))


if __name__ == "__main__":
    db.create_all()
    app.run(debug = True)

'''
#ql
'''

'''
Flask Tutorial #8 - Adding, Deleting & Updating Users w/ SQLAlchemy
https://www.youtube.com/watch?v=1nxzOrLWiic

This flask SQLAlchemy tutorial will show you how to update, delete and add rows in an SQLite3 database. 
It will also discuss how to properly query and search for entries in a table.

'''