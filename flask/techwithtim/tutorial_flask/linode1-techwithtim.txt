
##########################################################################
##########################################################################
##########################################################################
##########################################################################
linode 1 - TechwithTim

2020 07 10

# how to build a flask application on  a ubuntu VPS
https://www.digitalocean.com/community/tutorials/how-to-deploy-a-flask-application-on-an-ubuntu-vps

PuTTY is the most popular Windows SSH client.
It supports flexible terminal setup, mid-session reconfiguration using Ctrl-rightclick,
multiple X11 authentication protocols, and various other interesting things not provided
by ssh in an xterm. Putty also supports various protocols like Telnet, SCP, rlogin, SFTP and Serial.
sudo apt-get update

sudo apt-get install -y putty
putty -version
putty

This video will show you how to deploy your flask app to a headless linux server that is running ubuntu.
We will be using apache and wsgi to do so.

Thanks to Linode for sponsoring this video! Deploy your linux server by clicking the link or
using the promo code TWT19 for $20 free credit!
https://linode.com/techwithtim

Playlist: https://www.youtube.com/watch?v=mqhxx...

Download Links:
https://www.putty.org/
https://winscp.net/eng/download.php

# iInstall fileZilla
sudo apt install filezilla
start filezilla

Credits:
https://www.digitalocean.com/communit...

**Procedure (refer to video for missing details)**

####################################################
####################################################
####################################################
####################################################
####################################################
2020 07 12
created account using gmx mail.
https://cloud.linode.com/
https://status.linode.com/

Step 1: Setup a server on linode
Create Linode, latest Ubuntu
linode label- test
define root password
Nanode 1GB storage 25GB, Ram1GB
Networking:
SSH Access: note IP to use for putty

Step 2: Download Putty and SSH in

Step 3: Download and Install Apache
- sudo apt update                   #done - 2020 07 12
- sudo apt install apache2          #done - 2020 07 12
- apache2 -version                  #done - 2020 07 12  Apache/2.4.41

Step 4: Configure Firewall
- sudo ufw app list                 #done - 2020 07 12
- sudo ufw allow ‘Apache’   #done - 2020 07 12

Step 5: Configure apache
- sudo systemctl status apache2   #done - 2020 07 12
active running - green highligted.
exit - CTRL +C

Step 6: Install and enable mod_wsgi
-       sudo apt-get install libapache2-mod-wsgi python-dev   #done - 2020 07 12

Step 7:  Creating flask app
-       cd /var/www/                #done - 2020 07 12
-       sudo mkdir webApp           #done - 2020 07 12
-       cd webApp                   #done - 2020 07 12

Step 8: Install flask
-        sudo apt-get install python-pip    #done - 2020 07 12 - did not work
-        sudo apt install python3-pip    #done - 2020 07 12 -
pip3 --version
-        sudo pip3 install flask             #done - 2020 07 12
-        sudo pip install flask_sqlalchemy  #done - 2020 07 12

Step 9: Use winSCP to transfer python files to server
use
Host Name : Network IP  139.162.55.148
User Name : root
Leave Password empty

add new folder webApp
/var/www/webApp/webApp    # copy inside main app folder with including __init__.py

Step 10: configure and enable virtual host - in first main webApp folder
-       sudo nano /etc/apache2/sites-available/webApp.conf   #done - 2020 07 12

server IP need to be changed   #done - 2020 07 12
  #done - 2020 07 12 as nano does not support copying from system clipboard, make file on laptop and use fileZilla to shift.
<VirtualHost *:80>
		ServerName 139.162.55.148
		ServerAdmin email@mywebsite.com
		WSGIScriptAlias / /var/www/webApp/webapp.wsgi
		<Directory /var/www/webApp/webApp/>
			Order allow,deny
			Allow from all
		</Directory>
		Alias /static /var/www/webApp/webApp/static
		<Directory /var/www/webApp/webApp/static/>
			Order allow,deny
			Allow from all
		</Directory>
		ErrorLog ${APACHE_LOG_DIR}/error.log
		LogLevel warn
		CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>


CLICK TO DOWNLOAD THE CODE TO PUT IN webApp.conf
https://techwithtim.net/wp-content/up...

-      sudo a2ensite webApp         #done - 2020 07 12
-      systemctl reload apache2     #done - 2020 07 12

Step 11: Create .wsgi file
-      sudo nano webapp.wsgi   #done - 2020 07 12 - inside folger /var/www/webApp   - first webApp folder
Place the below code in the wsgi file # put in /var/www/webApp


#!/usr/bin/python
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/webApp/")

from webApp import app as application
application.secret_key = 'Add your secret key'  #done - 2020 07 12 - add secret key text, maliknaveedakram

Step 12: Restart apache
-      sudo service apache2 restart #done - 2020 07 12

Step 13: Visit the ip address of your server in the browser to  access your website!




email text from linode:
We recommend the following guides to help get you started:

Getting Started with Linode
Linode Beginner's Guide

Newly created Linode accounts have restrictions on ports 25, 465, and 587. If you'd like to send email from a Linode, first review this guide, then open a ticket with our Support team.

The Linode Community is a great place to get the answers you are looking for. Many of our customers utilize the Community to ask questions and find solutions while setting up and configuring their Linodes.

If you haven't previously connected with our Community, you can get started by following these steps:

    Navigate to Linode Community
    Click the green 'Log In' button
    Sign in using your Linode Account information
    Once you are logged in, you will be prompted to create a unique Community display name

For site migrations, server installs, one-off sysadmin tasks and nearly anything else you need, check out our Professional Services team:
Professional Services - Linode

You can also join us via #linode on irc.oftc.net or on webchat:
IRC Channel - Chat with Linode

For Linode system status and maintenance updates, please subscribe to our status page:
Linode Status

We're happy to have you on board! Please let us know if we can answer any questions.


######################################################
Where is Apache virtual host configuration file?
By default on Ubuntu systems, Apache Virtual Hosts configuration files are stored in
/etc/apache2/sites-available directory and can be enabled by creating symbolic links to the
/etc/apache2/sites-enabled directory
