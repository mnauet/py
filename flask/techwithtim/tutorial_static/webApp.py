from flask import Flask, render_template

app = Flask(__name__)

@app.route('/home')
@app.route('/')
def home():
    return render_template('home.html')


if __name__ == "__main__":
    app.run(debug=True)

'''
Flask Tutorial #9 - Static Files (Custom CSS, Images & Javascript)
https://www.youtube.com/watch?v=tXpFERibRaU&t=1s

This flask tutorial focuses on how to use custom CSS, images and javascript in your
HTML files from within a flask app. It discusses how to render and where to place
static files so that they work with
pythons flask module.
'''