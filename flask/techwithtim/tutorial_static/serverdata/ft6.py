from flask import  Flask, redirect, url_for, render_template, request,session,flash
from datetime import timedelta

app = Flask(__name__)
app.secret_key = "malik"
app.permanent_session_lifetime = timedelta(days=5)
#  http://127.0.0.1:5000/login

@app.route('/')
def home():
    if "usr" in session:
        user = session["usr"]
        flash("Already logged in")
    else:
        flash("You are not Logged in")
    return render_template('index4.html')

@app.route("/login", methods= ["POST","GET"])
def login():
    if request.method == "POST":
        session.permanent = True
        user = request.form["nm"]
        session["usr"] = user
        flash(f"Login Successful, {user} logged in")
        return redirect(url_for("user"))
    else:
        if "usr" in session:
            flash("Already logged in")
            return  redirect(url_for("user"))
        return  render_template("login.html")

@app.route("/user")
def user():
    if "usr" in session:
        user = session["usr"]
        return render_template("user.html", user=user)
    else:
        return redirect(url_for("login"))

@app.route("/logout")
def logout():

    flash("you have been logged out ", "info")
    session.pop("usr", None)
    return redirect(url_for("login"))


if __name__ == "__main__":
    app.run(debug = True)

'''
Flask Tutorial #6 - Message Flashing
https://www.youtube.com/watch?v=qbnqNWXf_tU
In this flask tutorial I will be showing how to flash messages on the screen.
Flashing messages is very easy and a good way to make your website more responsive.
'''