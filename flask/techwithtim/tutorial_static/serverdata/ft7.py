from flask import  Flask, redirect, url_for, render_template, request,session,flash
from datetime import timedelta
from flask_sqlalchemy import SQLAlchemy
#  http://127.0.0.1:5000/login
app = Flask(__name__)
app.secret_key = "malik"
app.permanent_session_lifetime = timedelta(days=5)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.sqlite3'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class users(db.Model):
    _id = db.Column("id", db.INTEGER, primary_key=True)
    name = db.Column(db.TEXT(100))
    email = db.Column(db.TEXT(100))

    def __init__(self, name, email):
        self.name = name
        self.email = email

@app.route('/')
def home():
    if "usr" in session:
        user = session["usr"]
        flash("Already logged in")
    else:
        flash("You are not Logged in")
    return render_template('index4.html')

@app.route("/login", methods= ["POST","GET"])
def login():
    if request.method == "POST":
        session.permanent = False
        user = request.form["nm"]
        session["usr"] = user
        flash(f"Login Successful, {user} logged in")
        return redirect(url_for("user"))
    else:
        if "usr" in session:
            flash("Already logged in")
            return  redirect(url_for("user"))
        return  render_template("login.html")

@app.route("/user", methods = ["POST", "GET"])
def user():
    email = None
    if "usr" in session:
        user = session["usr"]
        if request.method == "POST":
            email = request.form["email"]
            session["email"] = email
            flash(("email saved successfully"))
        else:
            if "email" in session:
                email = session["email"]
        return render_template("user.html", email=email)
    else:
        return redirect(url_for("login"))

@app.route("/logout")
def logout():

    flash("you have been logged out ", "info")
    session.pop("usr", None)
    session.pop("email", None)
    return redirect(url_for("login"))


if __name__ == "__main__":
    app.run(debug = True)

'''
2020 07 05 - Sonntag
instruction to install SQL Alchemy
pip install flask-sqlalchemy
 Flask_SQLAlchemy-2.4.3-py2.py3-none-any.whl (17 kB)

'''

'''
Flask Tutorial #7 - Using SQLAlchemy Database
https://www.youtube.com/watch?v=uZnp21fu8TQ
In this flask tutorial I will teach you how to setup a database with flask using SqlAlchemy,
SQLAlchemy allows you to execute SQL queries directly with python code. You can create models
that represent database information and easily modify and delete existing entries.
'''