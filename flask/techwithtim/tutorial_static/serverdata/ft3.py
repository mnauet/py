from flask import  Flask, redirect, url_for, render_template
#Python #Flask #Bootstrap
#Flask Tutorial #3 - Adding Bootstrap and Template Inheritance
# https://www.youtube.com/watch?v=4nzI4RKwb5I
app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index3.html')

@app.route('/relationships')
def test():
    return render_template('new3.html')



if __name__ == "__main__":
    app.run(debug = True)

