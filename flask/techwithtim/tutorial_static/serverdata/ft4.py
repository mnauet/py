from flask import  Flask, redirect, url_for, render_template, request

app = Flask(__name__)
#  http://127.0.0.1:5000/login
@app.route('/')
def home():
    return render_template('index4.html')

@app.route("/login", methods= ["POST","GET"])
def login():
    if request.method == "POST":
        user = request.form["nm"]
        return redirect(url_for("user", usr=user))
    else:
        return  render_template("login.html")

@app.route("/<usr>")
def user(usr):
    return f"<h1>{usr}</h1>"


if __name__ == "__main__":
    app.run(debug = True)

'''
Flask Tutorial #4 - HTTP Methods (GET/POST) & Retrieving Form Data
https://www.youtube.com/watch?v=9MHYHgh4jYc
In this flask tutorial I show you how to use the HTTP request methods Post and Get. 
The POST method will allow us to retrieve data from forms on our web page. 
In later videos we'll get into more advanced topics relating to login sessions 
and using POST methods to retrieve secure information like passwords.
'''