from flask import Flask, render_template
from admin.second import second
from sales.third  import third

app = Flask(__name__)
app.register_blueprint(second, url_prefix="/admin")
app.register_blueprint(third, url_prefix="/sales")

@app.route('/')
def test():
    return "<h1>main testing page</h1>"

if __name__ == "__main__":
    app.run(debug=True)






'''
http://exploreflask.com/en/latest/templates.html
https://www.youtube.com/watch?v=WteIH6J9v64

This flask tutorial will cover how to use Blueprints in a flask application. 
A blueprint allows you to separate your app into different files and different components. 
This can be useful when you re trying to reuse certain parts of your flask app later on.


'''