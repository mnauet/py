from flask import Blueprint, render_template

third = Blueprint("third", __name__, static_folder='static',template_folder='templates')

@third.route('/home')
@third.route('/')
def home():
    return render_template('home2.html')