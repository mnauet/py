from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

# FLASK -SQLALCHEMY Getting Started
# https://flask-sqlalchemy.palletsprojects.com/en/2.x/quickstart/#a-minimal-application
# text version
# https://dev.to/nahidsaikat/flask-with-sqlalchemy-marshmallow-5aj2

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
# Database
#app.permanent_session_lifetime = timedelta(days=5)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.sqlite3'
#app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
#db = SQLAlchemy(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# Init db
db = SQLAlchemy(app)
# Init ma
ma = Marshmallow(app)

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.String(100))
    price = db.Column(db.Float)
    qty = db.Column(db.Integer)

    def __init__(self, name, description, price, qty):
        self.name = name
        self.description = description
        self.price = price
        self.qty = qty

# Product Schema
class ProductSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description', 'price', 'qty')

# Init Schema
# https://marshmallow.readthedocs.io/en/3.0/upgrading.html
#product_schema = ProductSchema(strict=True)
#products_schema = ProductSchema(many=True, strict=True)
product_schema = ProductSchema()
products_schema = ProductSchema(many=True)

# Creat DB
# To create the initial database, just import the db object from an interactive Python shell and
# run the SQLAlchemy.create_all() method to create the tables and database:
# from app import db
# db.create_all()

#  Create a Product
@app.route('/product', methods=['POST'])
def add_product():
    try:
        name = request.json["name"]
        description = request.json["description"]
        price = request.json["price"]
        qty = request.json["qty"]

        new_product = Product(name,description,price,qty)

        db.session.add(new_product)
        db.session.commit()
        return product_schema.jsonify(new_product)
    except Exception as err:
        print('Error: ', err)


# http://127.0.0.1:5000/product
# POST POSTMAN Headers  :  KEY = Content-Type; VALUE= application/json

# Get All Products
@app.route('/product', methods=['GET'])
def get_products():
    all_products = Product.query.all()
    result = products_schema.dump(all_products)
    return jsonify(result)

# Get Single Product
@app.route('/product/<id>', methods=['GET'])
def get_product(id):
    product = Product.query.get(id)
    #result = products_schema.dump(all_products)
    #return jsonify(product)
    return product_schema.jsonify(product)

# Update a Product
@app.route('/product/<id>', methods=['PUT'])
def update_product(id):
    product = Product.query.get(id)
    product.name = request.json['name']
    product.description = request.json['description']
    product.price = request.json['price']
    product.qty = request.json['qty']

    #new_product = Product(name, description, price, qty)
    #db.session.add(new_product)
    db.session.commit()

    return  product_schema.jsonify(product)

# Delete a Product
@app.route('/product/<id>', methods=['DELETE'])
def delete_product(id):
    product = Product.query.get(id)
    db.session.delete(product)
    db.session.commit()
    return  product_schema.jsonify(product)
@app.route('/', methods=['GET'])
def get():
    return jsonify({'msg': 'Hello World'})

# Run Server
if __name__ == '__main__':
    app.run(debug=True)


'''
POSTMAN
    "name": "Product_1",
    "description": "This is Product one",
    "price":  100.10,
    "qty": 100
'''

'''
CREATE TABLE Ages ( 
  name VARCHAR(128), 
  age INTEGER
)
SELECT * from product
DELETE FROM product;
INSERT INTO product (name, description, price, qty) VALUES ('product_1', 'Product One', 100.0, 100);
INSERT INTO product (name, description, price, qty) VALUES ('product_2', 'Product Two', 200.0, 200);
INSERT INTO product (name, description, price, qty) VALUES ('product_3', 'Product Three', 300.0, 300);

import sqlite3
conn = sqlite3.connect("emaildb.sqlite")
cur = conn.cursor()
cur.execute('DROP TABLE if EXISTS Counts ')
cur.execute('CREATE TABLE Counts (email TEXT, Count INTEGER)')
cur.execute('INSERT INTO Counts (email,count) VALUES (?,1)', (email,))
conn.commit()
sqlstr = 'SELECT email, count from Counts ORDER BY Count DESC LIMIT 10'
for row in cur.execute(sqlstr):
    print(row[0], row[1])
cur.close()
'''

'''
We will be able to create new note or get all the notes by doing a POST or a GET request to 
/note/ api endpoint. Not only that we will be able to get the details of the note or update 
the note or delete the note with a GET or a PATCH or a DELETE request to /note/<id>/ api endpoint.

First of all, we will give a brief descriptions of the libraries that we will be using in this article.

Flask is a lightweight WSGI web application framework in Python. It is designed to make getting 
started very quickly and very easily.

marshmallow is an ORM/ODM/framework-agnostic library for converting complex datatypes, 
such as objects, to and from native Python datatypes.

Flask-Marshmallow is a thin integration layer for Flask and marshmallow that adds additional features 
to marshmallow.

SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers 
the full power and flexibility of SQL.

Flask-SQLAlchemy is an extension for Flask that adds support for SQLAlchemy to your application. 
It aims to simplify using SQLAlchemy with Flask.

marshmallow-sqlalchemy An SQLAlchemy integration with the marshmallow (de)serialization library.
'''