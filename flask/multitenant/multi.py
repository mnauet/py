import json
from flask import Flask, render_template,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import UserMixin, LoginManager, current_user, login_user, logout_user
import click
from flask.cli import with_appcontext

app = Flask(__name__)
Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SQLALCHEMY_BINDS'] = {'akram' : 'sqlite:///akram.db',
                                'akram2' : 'sqlite:///akram2.db',
                                'akram3' : 'sqlite:///akram3.db'}

app.config['SECRET_KEY'] = 'mysecretkey'

db = SQLAlchemy(app)
login = LoginManager(app)

class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def users_to_json(self):
        return {
            "id": self.id,
            "name": self.name
        }

class Tenant(db.Model, UserMixin):
    __tablename__ = 'tenants'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    def tenants_to_json(self):
        return {
            "id": self.id,
            "name": self.name
        }

class Books(db.Model, UserMixin):
    __tablename__ = 'books'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20))

class Akram(db.Model,UserMixin):
    __bind_key__='akram'
    id = db.Column(db.Integer, primary_key=True)

class Akram2(db.Model,UserMixin):
    __bind_key__='akram2'
    id = db.Column(db.Integer, primary_key=True)

class Akram3(db.Model,UserMixin):
    __bind_key__='akram3'
    id = db.Column(db.Integer, primary_key=True)


class MyModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated


admin = Admin(app, index_view=MyAdminIndexView())
admin.add_view(MyModelView(User, db.session))
admin.add_view(MyModelView(Books, db.session))
admin.add_view(MyModelView(Akram, db.session))
admin.add_view(MyModelView(Akram2, db.session))
admin.add_view(MyModelView(Akram3, db.session))

@app.route('/')
def index():
    users = User.query.all()
    tenants = Tenant.query.all()
    return render_template('06_index.html', users=users, tenants=tenants)

@app.route('/jusers')
def json_users():
    user_ids = User.query.all()
    return jsonify([u.users_to_json() for u in user_ids])

@app.route('/jtenants')
def json_tenants():
    tenant_ids = Tenant.query.all()
    return jsonify([u.tenants_to_json() for u in tenant_ids])

@app.route("/<tenant_name>/users")
def tenant_index(tenant_name):
    tenant_session = get_tenant_session(tenant_name)
    if not tenant_session:
        abort(404)
    users = tenant_session.query(User).all()
    return jsonify({tenant_name: [i.username for i in users]})

@app.route('/1')
def index_1():
    #second = One(id=1111)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the first table '
    #one = One.query.filter_by(id=111).first()
    akram = Akram.query.all()
    return render_template('06_index.html', users=akram)

@app.route('/2')
def index_2():
    #second = Two(id=222)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the second table '
    #one = Two.query.filter_by(id=222).first()
    ids = Akram2.query.all()
    return render_template('06_index.html', ids=ids)

@app.route('/3')
def index_3():
    #second = Three(id=333)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the third table '
    #one = Three.query.filter_by(id=333).first()
    ids = Akram3.query.all()
    return render_template('06_index.html', ids=ids)

@login.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@app.route('/login')
def login():
    user = User.query.get(1)
    login_user(user)
    return  'User Logged In'

@app.route('/logout')
def logout():
    logout_user()
    return 'User Logged Out'


@app.cli.command("initdb")
def reset_db():
    """Drops and Creates fresh database"""
    db.drop_all()
    db.create_all()

    print("initdb command executed in shell: - Initialized default DB")


@app.cli.command("bootstrap")
def bootstrap_data():
    db.drop_all()
    db.create_all()

    u1 = User(id=1, name='malik.akram@gmx.de')
    u2 = User(id=2, name='malik2.akram@gmail.com')
    u3 = User(id=3, name='malik3.akram@gmail.com')

    t1 = Tenant(id=1, name='Akram')
    t2 = Tenant(id=2, name='Akram2')
    t3 = Tenant(id=3, name='Akram3')
    t4 = Tenant(id=4, name='Akram4')

    a1 = Akram(id=1)
    a2 = Akram(id=2)
    a3 = Akram(id=3)

    a2_1 = Akram2(id=1)
    a2_2 = Akram2(id=2)
    a2_3 = Akram2(id=3)

    a3_1 = Akram3(id=1)
    a3_2 = Akram3(id=2)
    a3_3 = Akram3(id=3)

    db.session.add(u1)
    db.session.add(u2)
    db.session.add(u3)

    db.session.add(t1)
    db.session.add(t2)
    db.session.add(t3)
    db.session.add(t4)

    db.session.add(a1)
    db.session.add(a2)
    db.session.add(a3)

    db.session.add(a2_1)
    db.session.add(a2_2)
    db.session.add(a2_3)

    db.session.add(a3_1)
    db.session.add(a3_2)
    db.session.add(a3_3)

    db.session.commit()

    print("bootstrap command executed in shell: -Added development dataset")

if __name__ == '__main__' :
    app.run(debug=True)

'''


2020 08 28

# first set app.py as FLASK_APP=hello
export FLASK_APP=multi.py
flask run

$ flask initdb
export FLASK_DEBUG=1

####################################################################
2020 08 20
https://www.youtube.com/watch?v=SB5BfYYpXjE
https://flask-sqlalchemy.palletsprojects.com/en/2.x/binds/
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3
$ sqlite3
sqlite> .tables
sqlite> 
sqlite> .exit

python
from multiple_databases import db
# db.create_all(bind=['two', 'three']
# db.create_all(bind='two')
db.create_all(bind='three')
>>> db.create_all(bind=['two', 'three'])


sqlite3 three.db
.tables
three
.exit

db.create_all()


# create dbs
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 one.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 two.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 three.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit


>>> db.create_all()
>>> db.create_all(bind=['users'])
>>> db.create_all(bind='appmeta')
>>> db.drop_all(bind=None)
'''