CREATE DATABASE General;

DROP TABLE IF EXISTS tenants;
CREATE TABLE tenants (
  id INT NOT NULL,
  name varchar(100) NOT NULL
)
ALTER TABLE tenants ADD CONSTRAINT tenenats_id_pk1 PRIMARY KEY (id);
CREATE INDEX tenants_id_idx
  ON tenants(id);
INSERT INTO tenants VALUES (1,'TenantA'),(2,'TenantB');

CREATE DATABASE TenantA;


DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id int NOT NULL,
  username varchar(100) NOT NULL
);

ALTER TABLE users ADD CONSTRAINT users_id_pk1 PRIMARY KEY (id);
CREATE INDEX users_id_idx
  ON users(id);


INSERT INTO users VALUES (1,'userA'),(2,'userB');


CREATE DATABASE TenantB;

DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id int NOT NULL,
  username varchar(100) NOT NULL
);

ALTER TABLE users ADD CONSTRAINT users_id_pk1 PRIMARY KEY (id);
CREATE INDEX users_id_idx
  ON users(id);

INSERT INTO users VALUES (1,'userC'),(2,'userD');
