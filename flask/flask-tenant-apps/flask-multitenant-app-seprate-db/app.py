import os
from flask import Flask, jsonify, abort
from models import db
from multi_db_management import get_tenant_session
from models import User

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://uk:uk@localhost/general'
app.config['SQLALCHEMY_BINDS'] = {'TenantB': 'postgresql+psycopg2://uk:uk@localhost/tenantb',
                                  'TenantA': 'postgresql+psycopg2://uk:uk@localhost/tenanta'}

db.init_app(app)


@app.route("/<tenant_name>/users")
def index(tenant_name):
    tenant_session = get_tenant_session(tenant_name)
    if not tenant_session:
        abort(404)
    users = tenant_session.query(User).all()
    return jsonify({tenant_name: [i.username for i in users]})


if __name__ == '__main__':
    app.run(debug=True)
