from models import Tenant
from models import db
from flask import current_app

PG_URI = 'postgresql+psycopg2://uk:uk@localhost/{}'


def get_known_tenants():
    tenants = Tenant.query.all()
    return [i.name for i in tenants]


def get_tenant_session(tenant_name):
    if tenant_name not in get_known_tenants():
        return None
    engine = db.get_engine(current_app, bind=tenant_name)
    session_maker = db.sessionmaker()
    session_maker.configure(bind=engine)
    session = session_maker()
    return session
