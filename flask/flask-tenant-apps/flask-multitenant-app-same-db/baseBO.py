from sqlalchemy import Column, event, inspect
from sqlalchemy.orm import Query

from auth import get_current_tenant
from models import db


class TenantTable(object):
    """
    Provides [is_deleted] Column definition
    """
    tenant_id = Column(db.Integer, nullable=False)


@event.listens_for(Query, "before_compile", retval=True)
def before_compile(query):
    """A query compilation rule that will add limiting criteria for every
    subclass of SoftDeletable"""

    if query._execution_options.get("include_deleted", False):
        return query
    for ent in query.column_descriptions:
        entity = ent['entity']
        if entity is None:
            continue
        insp = inspect(ent['entity'])
        mapper = getattr(insp, 'mapper', None)
        if mapper and issubclass(mapper.class_, TenantTable):
            query = query.enable_assertions(False).filter(
                ent['entity'].tenant_id == get_current_tenant())

    return query


# the recipe has a few holes in it, unfortunately, including that as given,
# it doesn't impact the JOIN added by joined eager loading.   As a guard
# against this and other potential scenarios, we can check every object as
# its loaded and refuse to continue if there's a problem
@event.listens_for(TenantTable, "load", propagate=True)
def load(obj, context):
    if obj.tenant_id and not \
            context.query._execution_options.get("include_deleted", True):
        raise TypeError(
            "deleted object %s was loaded, did you use "
            "joined eager loading?" % obj)


