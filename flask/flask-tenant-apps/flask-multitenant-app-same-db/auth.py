from flask import g, request

from models import Tenant


def get_tenant_from_request():
    tenant_id = request.headers.get('tenant_id')
    return Tenant.query.get(tenant_id).id


def get_current_tenant():
    rv = getattr(g, 'current_tenant', None)
    if rv is None:
        rv = get_tenant_from_request()
        g.current_tenant = rv
    return rv