from flask import Flask, jsonify, abort
from models import User, db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://uk:uk@localhost/general'
app.config['SQLALCHEMY_BINDS'] = {}

db.init_app(app)


@app.route("/users")
def index():
    users = User.query.all()
    return jsonify({'result': [i.username for i in users]})


if __name__ == '__main__':
    app.run(debug=True)
