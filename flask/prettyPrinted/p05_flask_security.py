from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///security.db'

db = SQLAlchemy(app)

roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
    )

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime)

class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    description = db.Column(db.String(255))

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


if __name__ == '__main__' :
    app.run(debug=True)

'''
2020 08 19

pip install flask-security  #Babel-2.8.0 Flask-BabelEx-0.9.4 Flask-Mail-0.9.1 Flask-Principal-0.4.0 Flask-WTF-0.14.3 blinker-1.4 flask-security-3.0.0 passlib-1.7.2 speaklater-1.3
pip install flask-sqlalchemy
pip install email_validator #dnspython-2.0.0 email-validator-1.1.1 idna-2.10


>>> from p05_flask_security import db
>>> db.create_all()
>>> exit()
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 security.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> 

sqlite> .tables
role         roles_users  user       
sqlite> 
sqlite> .exit



https://www.youtube.com/watch?v=LsHf3JSDBVc
 Pretty Printed
56.700 Abonnenten
Flask-Security is a library you can use to handle things like authentication and authorization 
in your app. In this video I show you how to get started with the Flask-Security library.
'''