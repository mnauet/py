from flask import Flask, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import UserMixin, LoginManager, current_user, login_user, logout_user

app = Flask(__name__)
Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///example.db'
app.config['SECRET_KEY'] = 'mysecretkey'

db = SQLAlchemy(app)
login = LoginManager(app)



class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))

class Books(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20))


class MyModelView(ModelView):
    def is_accessible(self):
        #return current_user.is_authenticated
        return True

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        #return current_user.is_authenticated
        return True

admin = Admin(app, index_view=MyAdminIndexView())
admin.add_view(MyModelView(User, db.session))
admin.add_view(MyModelView(Books, db.session))

@login.user_loader
def load_user(user_id):
    #return User.query.get(user_id)
    return User.query.get(1)

@app.route('/login')
def login():
    user = User.query.get(1)
    login_user(user)
    return  'User Logged In'

@app.route('/logout')
def logout():
    logout_user()
    return 'User Logged Out'

if __name__ == '__main__' :
    app.run(debug=True)

'''
2020 08 19
https://www.youtube.com/watch?v=NYWEf9bZhHQ
(venv) malik@ubuntu11:~/repos/py/flask$ 
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ pip install -U Flask-SQLAlchemy
$ pip install -U Flask-SQLAlchemy   #Flask-SQLAlchemy-2.4.4
$ pip install flask-login           #flask-login-0.5.0
$ pip install Flask-Admin           #Flask-Admin-1.5.6 wtforms-2.3.3
pip install --upgrade pip           #pip-20.2.2
sudo apt-get update

>>> from p04_Flask_Admin_login import db, User
>>> db.create_all()
>>> s= User(name='malik')
>>> db.session.add(s)
>>> db.session.commit()
>>> exit()

(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 example.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite>

sqlite> select * from user;
1|malik
sqlite> .exit

http://127.0.0.1:5000/admin/
http://127.0.0.1:5000/login
http://127.0.0.1:5000/logout
http://127.0.0.1:5000/admin/
http://127.0.0.1:5000/admin/user/

source activate
deactivate


$ source venv/bin/activate

Pretty Printed
56.600 Abonnenten
In this video I talk about how you can integrate Flask-Login into Flask-Admin
so you can protect your admin routes. This can also be done using Flask-User
or Flask-Security since they're both based off Flask-Logi
'''