from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///relationships.db'
db = SQLAlchemy(app)

subs = db.Table ('subs',
    db.Column('user_id', db.Integer, db.ForeignKey('user.user_id')),
    db.Column('channel_id', db.Integer, db.ForeignKey('channel.channel_id'))
)
#   >>> user1 = User(user_name='malik')
#   >>> channel1 = Channel(channel_name= 'Pretty Printed')

class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(20))
    subscriptions = db.relationship('Channel', secondary=subs, backref=db.backref('subscribers', lazy='dynamic'))
#   >>> channel1.subscribers.append(user3)

class Channel(db.Model):
    channel_id = db.Column(db.Integer, primary_key=True)
    channel_name = db.Column(db.String(20))

if __name__ == '__main__':
    app.run(debug=True)

'''
2020 08 03
^C(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/testing/prettyPrinted$ python
>>> from _02 import *
>>> db.create_all()

>>> user1 = User(user_name='malik')
>>> user2 = User(user_name='naveed')
>>> user3 = User(user_name='akram')
>>> user4 = User(user_name='mubeen')
>>> user5 = User(user_name='wali')
>>> db.session.add(user1)
>>> db.session.add(user2)
>>> db.session.add(user3)
>>> db.session.add(user4)
>>> db.session.add(user5)
>>> db.session.commit()

>>> channel1 = Channel(channel_name= 'Pretty Printed')
>>> channel2 = Channel(channel_name= 'Cat Videos')
>>> db.session.add(channel1)
>>> db.session.add(channel2)
>>> db.session.commit()

>>> channel1.subscribers.append(user1)
>>> db.session.commit()
>>> channel1.subscribers.append(user3)
>>> channel1.subscribers.append(user5)
>>> channel2.subscribers.append(user2)
>>> channel2.subscribers.append(user4)
>>> channel2.subscribers.append(user5)
>>> db.session.commit()

>>> for user in  channel1.subscribers:
...     user.user_name
... 
'malik'
'akram'
'wali'
>>> 

'''