from flask import Flask, render_template_string, url_for
from flask_sqlalchemy import  SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter
from flask_bootstrap import Bootstrap

# Class-based application configuration
class ConfigClass(object):
# https://flask-user.readthedocs.io/en/latest/quickstart_app.html
    CSRF_ENABLED = True

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///db7_user2.db'    # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids SQLAlchemy warning

    # Flask-User settings
    USER_APP_NAME = "Flask-User QuickStart App"      # Shown in and email templates and page footers
    USER_ENABLE_USERNAME = True    # Enable username authentication
    USER_ENABLE_EMAIL = False      # Disable email authentication
    USER_REQUIRE_RETYPE_PASSWORD = False    # Simplify register form


def create_app():
    """ Flask application factory """

    # Create Flask app load app.config
    app = Flask(__name__)
    app.config.from_object(__name__ + '.ConfigClass')
    Bootstrap(app)

    # Initialize Flask-SQLAlchemy
    db = SQLAlchemy(app)


    # Define the User data-model.
    # NB: Make sure to add flask_user UserMixin !!!
    class User(db.Model, UserMixin):
        __tablename__ = 'users'
        id = db.Column(db.Integer, primary_key=True)
        active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')

        username = db.Column(db.String(50, collation='NOCASE'), nullable=False,  unique=True)
        password = db.Column(db.String(255), nullable=False, server_default='')
        email_confirmed_at = db.Column(db.DateTime())

        first_name = db.Column(db.String(100, collation='NOCASE'), nullable=True, server_default='')
        last_name =  db.Column(db.String(100, collation='NOCASE'), nullable=True, server_default='')

    # Create all database tables
    db.create_all()

    # Setup Flask-User and specify the User data-model
    #user_manager = UserManager(app, db, User)
    db_adapter = SQLAlchemyAdapter(db,User)
    user_manager = UserManager(db_adapter, app)

    #   http://127.0.0.1:5000/user/register
    #   http://127.0.0.1:5000/user/sign-out
    #   http://127.0.0.1:5000/user/sign-in

    @app.route('/')
    def home_page():
        # String-based templates
        # {% extends "flask_user_layout.html" %}
        return render_template_string("""
            {% extends "bootstrap/base.html" %}
            {% block content %}
            <h2>Home page</h2>
            <p><a href={{ url_for('user.register') }}>Register</a></p>
            <p><a href={{ url_for('user.login') }}>Sign in</a></p>
            <p><a href={{ url_for('home_page') }}>Home page</a> (accessible to anyone)</p>
            <p><a href={{ url_for('member_page') }}>Member page</a> (login required)</p>
            <p><a href={{ url_for('user.logout') }}>Sign out</a></p>
            {% endblock %}
            """)

    @app.route('/members')
    @login_required  # User must be authenticated
    def member_page():
        # String-based templates
        return render_template_string("""
            {% extends "bootstrap/base.html" %}
            {% block content %}
                <h2>Members page</h2>
                <p><a href={{ url_for('user.register') }}>Register</a></p>
                <p><a href={{ url_for('user.login') }}>Sign in</a></p>
                <p><a href={{ url_for('home_page') }}>Home page</a> (accessible to anyone)</p>
                <p><a href={{ url_for('member_page') }}>Member page</a> (login required)</p>
                <p><a href={{ url_for('user.logout') }}>Sign out</a></p>
            {% endblock %}
            """)
    return app

# Start development web server
# if __name__ == '__main__' :
# app.run(debug=True)
if __name__=='__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)

#cd ~/dev/my_app
#python p07_flask_user_2.py

'''
2020 08 22
# This file contains an example Flask-User application.
# To keep the example simple, we are applying some unusual techniques:
# - Placing everything in one file
# - Using class-based configuration (instead of file-based configuration)
# - Using string-based templates (instead of file-based templates)

https://flask-user.readthedocs.io/en/latest/installation.html
https://flask-user.readthedocs.io/en/latest/installation.html
https://flask-user.readthedocs.io/en/latest/quickstart_app.html

pip install Flask-User  # Flask-User-1.0.2.2 bcrypt-3.2.0 cffi-1.14.2 cryptography-3.0 pycparser-2.20

(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ pip install "Flask-User<0.7"  #Flask-User-0.6.21 pycryptodome-3.9.8

$ sqlite3 db7_user.db #create db

$ sqlite3 db7_user

$ sqlite3 db7_user.db
sqlite> .tables
user

'''