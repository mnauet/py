from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///relationships.db'
db = SQLAlchemy(app)

class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name= db.Column(db.String(20))
    pets = db.relationship('Pet', backref='owner')

class Pet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))

if __name__ ==  "__main__" :
    app.run(debug=True)

    if __name__ == "__main__":
        app.run(debug=True)  # option 1 to restart server automatically as code changed.

'''
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/testing/prettyPrinted$ python
from _01 import db
>>> from _01 import Person, Pet
>>> db.create_all()
>>> anthony =  Person(name='Anthony')
>>> db.session.add(anthony)
>>> michelle= Person(name='Michelle')
>>> db.session.add(michelle)
>>> db.session.commit()
>>> spot = Pet(name='Spot', owner=anthony)
>>> db.session.add(spot)
>>> db.session.commit()
>>> brian = Pet(name='Brain', owner=michelle)
>>> db.session.commit()


>>> db.session.add(brian)
>>> db.session.commit()
>>> some_owner = Person.query.filter_by(name= 'Anthony').first()    
>>> some_owner
<Person 1>
>>> some_owner.name
'Anthony'
>>> some_owner.id
1
>>> some_owner.pets
[<Pet 1>]
>>> some_owner.pets[0].name
'Spot'
>>> some_owner = Person.query.filter_by(name='Anthony').first()
>>> some_owner
<Person 1>
>>> some_owner.pets
[<Pet 1>, <Pet 3>]
>>> some_owner.pets[1].name
'Clifford'

>>> michelle = Person.query.filter_by(name='Michelle').first()
>>> michelle
<Person 2>
>>> michelle.pets
[<Pet 2>]
>>> michelle.pets[0].name
'Brain'
>>> 
'''











# https://www.youtube.com/watch?v=juPQ04_twtA