from flask import Flask, render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

@app.route('/')
def index():
    return render_template('03_index.html')

if __name__ == '__main__':
    app.run(debug=True)


'''
(venv) malik@ubuntu11:~/repos/py/flask$ pip install flask-bootstrap

'''