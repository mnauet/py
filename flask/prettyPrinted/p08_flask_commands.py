from flask import Flask
import click
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db8_commands.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class MyTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)

# Method 1
@click.command(name='initdb')
@with_appcontext
def initdb():
    #db.create_all()
    click.echo('Init the db 1')
app.cli.add_command(initdb)

#Method 2
@app.cli.command("initdb")
def initdb2():
    click.echo('Init the db 2')


if __name__ ==  '__main__':
    app.run(debug=True)

'''
2020 08 22
https://www.youtube.com/watch?v=wyG3BiL-E5c
https://flask.palletsprojects.com/en/0.12.x/cli/
# first set app.py as FLASK_APP=hello
export FLASK_APP=hello
flask run
Or with a filename:
export FLASK_APP=/path/to/hello.py
flask run
# test command lines
$ flask initdb
export FLASK_DEBUG=1
'''