from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import UserMixin, LoginManager, current_user, login_user, logout_user

app = Flask(__name__)
Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///example.db'
app.config['SQLALCHEMY_BINDS'] = {'one' : 'sqlite:///one.db',
                                'two' : 'sqlite:///two.db',
                                'three' : 'sqlite:///three.db'}
app.config['SECRET_KEY'] = 'mysecretkey'

db = SQLAlchemy(app)
login = LoginManager(app)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))

class Books(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20))

class One(db.Model,UserMixin):
    __bind_key__='one'
    id = db.Column(db.Integer, primary_key=True)

class Two(db.Model,UserMixin):
    __bind_key__='two'
    id = db.Column(db.Integer, primary_key=True)

class Three(db.Model,UserMixin):
    __bind_key__='three'
    id = db.Column(db.Integer, primary_key=True)


class MyModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated


admin = Admin(app, index_view=MyAdminIndexView())
admin.add_view(MyModelView(User, db.session))
admin.add_view(MyModelView(Books, db.session))
admin.add_view(MyModelView(One, db.session))
admin.add_view(MyModelView(Two, db.session))
admin.add_view(MyModelView(Three, db.session))

@app.route('/')
def index():
    #one = User.query.filter_by(id=1).first()
    ids = User.query.all()

    return render_template('06_index.html', ids=ids)

@app.route('/1')
def index_1():
    #second = One(id=1111)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the first table '
    #one = One.query.filter_by(id=111).first()
    ids = One.query.all()
    return render_template('06_index.html', ids=ids)

@app.route('/2')
def index_2():
    #second = Two(id=222)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the second table '
    #one = Two.query.filter_by(id=222).first()
    ids = Two.query.all()
    return render_template('06_index.html', ids=ids)

@app.route('/3')
def index_3():
    #second = Three(id=333)
    #db.session.add(second)
    #db.session.commit()
    #return 'Added a value to the third table '
    #one = Three.query.filter_by(id=333).first()
    ids = Three.query.all()
    return render_template('06_index.html', ids=ids)

@login.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@app.route('/login')
def login():
    user = User.query.get(1)
    login_user(user)
    return  'User Logged In'

@app.route('/logout')
def logout():
    logout_user()
    return 'User Logged Out'



if __name__ == '__main__' :
    app.run(debug=True)

'''

2020 08 20
https://www.youtube.com/watch?v=SB5BfYYpXjE
https://flask-sqlalchemy.palletsprojects.com/en/2.x/binds/
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3
$ sqlite3
sqlite> .tables
sqlite> 
sqlite> .exit

python
from multiple_databases import db
# db.create_all(bind=['two', 'three']
# db.create_all(bind='two')
db.create_all(bind='three')
>>> db.create_all(bind=['two', 'three'])


sqlite3 three.db
.tables
three
.exit

db.create_all()


# create dbs
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 one.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 two.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit
(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ sqlite3 three.db
SQLite version 3.26.0 2018-12-01 12:34:55
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit


>>> db.create_all()
>>> db.create_all(bind=['users'])
>>> db.create_all(bind='appmeta')
>>> db.drop_all(bind=None)
'''