from flask import Flask
from flask_sqlalchemy import  SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db7_user.db'
app.config['SECRET_KEY'] = 'secretkey'
app.config['CSRF_ENABLED'] = True
app.config['USER_ENABLE_EMAIL'] = False

db = SQLAlchemy(app)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')

    username = db.Column(db.String(50, collation='NOCASE'), nullable=False,  unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')

    first_name = db.Column(db.String(100, collation='NOCASE'), nullable=True, server_default='')
    last_name =  db.Column(db.String(100, collation='NOCASE'), nullable=True, server_default='')

db_adapter = SQLAlchemyAdapter(db,User)
user_manager = UserManager(db_adapter, app)
#   http://127.0.0.1:5000/user/register
#   http://127.0.0.1:5000/user/sign-out
#   http://127.0.0.1:5000/user/sign-in

@app.route('/')
def index():
    return '<h1> This is the home page</h1>'

@app.route('/profile')
@login_required
def profile():
    return '<h1> This is the protected profile page</h1>'

if __name__ == '__main__' :
    app.run(debug=True)

'''
2020 08 22
https://flask-user.readthedocs.io/en/latest/installation.html
https://flask-user.readthedocs.io/en/latest/installation.html
https://flask-user.readthedocs.io/en/latest/quickstart_app.html

pip install Flask-User  # Flask-User-1.0.2.2 bcrypt-3.2.0 cffi-1.14.2 cryptography-3.0 pycparser-2.20

(venv) malik@ubuntu11:~/repos/py/flask/prettyPrinted$ pip install "Flask-User<0.7"  #Flask-User-0.6.21 pycryptodome-3.9.8

$ sqlite3 db7_user.db #create db

$ sqlite3 db7_user

$ sqlite3 db7_user.db
sqlite> .tables
user

'''