from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method =='POST' :
        some_json = request.get_json()
        return jsonify({'you sent': some_json}), 201
    else:
        return jsonify({"about": "Hello World"})

# curl --request POST 'http://127.0.0.1:5000' --header 'Content-Type: application/json' --data-raw '{"result": 1000}'
# curl -X POST 'http://127.0.0.1:5000' -H 'Content-Type: application/json' -d '{"result": 1000}'
# curl  -H 'Content-Type: application/json' -X POST  -d '{"result": 1000}' http://127.0.0.1:5000

@app.route('/multi/<int:num>', methods=['GET'])
def get_multiply_10(num):
    return jsonify({'result': num * 10})
# curl --location --request GET 'http://127.0.0.1:5000/multi/10' \ --header 'Content-Type: application/json'
# curl http://127.0.0.1:5000/multi/10


if __name__ == '__main__' :
    app.run(debug=True)

    '''
What curl means?
Client URL
cURL (pronounced 'curl') is a computer software project providing a library (libcurl) 
and command-line tool (curl) for transferring data using various network protocols. 
The name stands for "Client URL", which was first released in 1997.
    
    '''