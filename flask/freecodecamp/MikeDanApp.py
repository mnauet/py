from math import *
#from useful_tools import *
import useful_tools
from Student import Student
from Question import Question
from Chef import Chef
from ChineseChef import ChineseChef
#import student
'''
name = "George"
age =  70
is_Male = True
print("Name: " + name)
print("Age: " , age )
print("Girafe\nAcademy")
phrase = "Giraffe Academy"
print (phrase + "is cool")
print(phrase.lower())
print(phrase.upper())
print(phrase.isupper())
print(phrase.lower().islower())
print(len(phrase))
print(phrase[0])
print(phrase.index("G"))
print(phrase.replace("Giraffe" , "Elephant"))

print(3 * (4+5))
print (10%3)
my_num = -5
print(str(my_num) + " my favourite num")
print(abs(my_num))
print(pow(3,2))
print(max(4,6))
print(round(3.47))
print(floor(3.7))
print(ceil(3.7))
print(sqrt(36))

#i= input("input: ")
#age = input("age")

#print(i)
#print(age)

# Building a calculator
#num1 = input("get first number ")
#num2 = input(" enter another number ")
#result = float(num1) + float(num2)
#print(result)
'''

# Mad Libs game

'''
color = input("color ")
plural_noun= input("plural noun ")
celebrity = input("celebrity ")

print(" Roses are " + color)
print (plural_noun + " are blue")
print("i love" + celebrity)
'''

#lists
'''
friends = ["malik", "naveed", "akram", "sehrish", "mubeen", "fatima", "zaynab"]
lucky_numbers = [1,2,3,4,5,8,7,6]
print(friends[0])
print(friends[-1])
print(friends[-3:-1]) # -1 not included
print(friends[1:]) # start from 1
print(friends[0:2]) # not include 3

friends.extend(lucky_numbers)
print(friends)
friends.insert(0, "wali")
friends.remove(5)
print(friends)
friends.pop() # removes last element
print(friends)
print(friends.index("wali"))
print(friends.count("wali"))
print(lucky_numbers.sort())

'''

# Tuples
'''
coordinate = ({1,2}, {3,4}) # tuples are immutable, can not be modified as created.
tuple_list = [(1,2), (3,4), (5,6)]
print(coordinate[1])
coordinate[0] = {2,4}
'''

# function
def say_hi(name) :
    print("say hallo" + name)

'''
print(say_hi(" malik"))
say_hi(" akram")
'''

# return statement
'''
def cube(num) :
    return  num*num*num

result = cube(3)
print (result)
'''

# if statements
'''
is_male = True
is_tall = False
if is_male and is_tall:
    print("male")
elif is_male and not is_tall:
    print(" male but not tall")
else:
    print("female")
'''
# comparisons
'''
def max_num(num1,num2,num3):
    if num1>=num2 and num1>=num3 :
        return num1
    elif num2>=num1 and num2>=num3 :
        return num2
    else :
        return num3

print(max_num(1,2,3))
print(max_num(3,2,1))
print(max_num(2,3,1))

'''

#Building a better calculator
'''
num1= float(input("enter first number"))
num2= float(input("enter 2nd number"))

op = input ("enter operator ")

if op== "+" :
    print (num1 + num2)
elif op == "-" :
    print(num1-num2)
elif op == "*" :
    print(num1*num2)
elif op == "/":
    print (num1/num2)
else:
    print("not valid operation")
'''
# Dictionaries - store informatin as key value pairs
'''
monthConversions = {
    "jan" : "january",
    "feb" : "february",
    "mar" : "march"
}(
print(monthConversions["jan"])
print(monthConversions.get("janu", "not a valid key"))
'''

# while loop
'''
i = 1
while i <= 5 :
    print(i)
    i += 1
'''

# guessing game
#2:38:00
'''
secret_word = "giraffe"
guess = ""
i = 3
while guess != secret_word and i > 0:
    guess = input("enter guess: ")
    i -= 1
    print(i)
    if guess == secret_word:
        print("you won")
'''

# For Loop
'''
for letter in "Giraffe Academy" :
    print(letter)
friends = ["malik", "naveed", "akram"]
for friend in friends:
    print(friend)
    
for index in range(5): # not including last
    print(index)
for index in range(3,5): # range, not including last
    print(index)
for index  in range(len(friends)):
    print(friends[index])
'''
# Exponent Function
'''
def raise_to_power(base, power):
    result = 1
    for i in range(power):
        result = result * base
    return result

print (raise_to_power(2,10))
'''

# 2D lists and nested loops
'''
#2:50:00
number_grid = [ # 4 rows,s 3 cols
    [1,2,3],
    [4,5,6],
    [7,8,9],
    [0]
]
print(number_grid[0][-1])
print(number_grid[-1][-1])
for row in number_grid :
    print(row)
    for col in row :
        print(col)
'''

# Basic translator , englis to giraffe language
'''
def translate(phrase):
    translation = ""
    for letter in phrase:
        if letter.lower() in "aeiou" :
            if letter.isupper():
                translation = translation + "G"
            else :
                translation = translation + "g"
        else :
            translation = translation + letter
    return translation

print(translate("MAlik"))
'''
# Comments
# hash tag
''' 
    tripple quotation marks
''' # block comments

# Try except
'''
try:
    number = int(input("enter a number"))
    print (number/2)
except ZeroDivisionError:
    print("Div by zero error")
except ValueError as err:
    print(err)
except :
    print(err)
'''

# Reading from external files
'''
#3:13:00
file = open("./employee.txt", "r")     # r = read, w = write, a = append at end of file. r+ = read & write
if file.readable():
    print("file is readable")
lines = file.readlines()
print(lines)
print(lines[0])
for row in lines :
    print(row)
#print(file.readlines()[1])



file.close()
'''

# Writing to new files or  Append to files
'''
file = open("employee.txt", "+r")
file2 = open("emp1.txt", "w") # create a new file
file.write("\nmalik - customer service\n")
print(file.read())
file.close()
'''

# Modules & PiP - Package manager
# https://docs.python.org/3/py-modindex.html
#print(useful_tools.roll_dice(6))

#classes & objects
'''
student1 = Student("malik", "Business", 4.0, False)
print(student1.name)
print(student1.on_honour_roll())
'''

# multiple choice questions
'''
question_prompts = [
    "What color are apples? \n(a) Red/Green\n(b)Purple\n(c) Orange \n\n",
    "What color are bananas? \n(a) Teal\n(b) Magenta\n(c) Yellow\n\n",
    "What color are strawberries?\n(a) Yellow\n(b) Red\n(c) Blue\n\n"
]

questions = [
    Question(question_prompts[0], "a"),
    Question(question_prompts[1], "c"),
    Question(question_prompts[2], "b")

]

def run_test(list_of_questions):
    score = 0
    for question in list_of_questions :
        #print(question.prompt + "answer :" + question.answer)
        answer = input(question.prompt + "\n Write your answer: " )
        if answer == question.answer:
            score +=1
    print(" you got " + str(score) + "/" + str(len(list_of_questions)), " correct")

print(run_test(questions))
'''

# Class Function

# Inheritance class
'''
myChef = ChineseChef()
myChef.make_chicken()
myChef.make_salad()
myChef.make_special_dish()
myChef.make_fried_rice()
'''

# python interpreter
# sand box
# type inside termin python3 and command prompt comes to test any of python command.
