import urllib.request, urllib.parse, urllib.error
import json
import ssl

api_key = False
# If you have a Google Places API key, enter it here
# api_key = 'AIzaSy___IDByT70'
# https://developers.google.com/maps/documentation/geocoding/intro

if api_key is False:
    api_key = 42
    serviceurl = 'http://py4e-data.dr-chuck.net/json?'
else :
    serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True:
    address = input('Enter location: ')
    address = "lahore"
    if len(address) < 1: break

    parms = dict()
    parms['address'] = address
    if api_key is not False: parms['key'] = api_key
    url = serviceurl + urllib.parse.urlencode(parms)
    print("params:", parms) #params: {'address': 'lahore', 'key': 42}

    print('Retrieving', url)    #Retrieving http://py4e-data.dr-chuck.net/json?address=lahore&key=42
    uh = urllib.request.urlopen(url, context=ctx)
    print("uh", uh) #uh <http.client.HTTPResponse object at 0x7fc015de6cc0>
    data = uh.read().decode()
    print('Retrieved', len(data), 'characters') #Retrieved 1747 characters

    try:
        js = json.loads(data)
    except:
        js = None
    print("js", js)   #{'results': [{'address_components': [{'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['locality', 'political']}, {'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Punjab', 'short_name': 'Punjab', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Pakistan', 'short_name': 'PK', 'types': ['country', 'political']}], 'formatted_address': 'Lahore, Punjab, Pakistan', 'geometry': {'bounds': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}, 'location': {'lat': 31.5203696, 'lng': 74.35874729999999}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}}, 'place_id': 'ChIJ2QeB5YMEGTkRYiR-zGy-OsI', 'types': ['locality', 'political']}], 'status': 'OK'}
    if not js or 'status' not in js or js['status'] != 'OK':
        print('==== Failure To Retrieve ====')
        print(data)
        continue
    print(json.dumps(js, indent=4))


    lat = js['results'][0]['geometry']['location']['lat']   # list[0][dict1][dict2][sub element]
    lng = js['results'][0]['geometry']['location']['lng']
    print('lat', lat, 'lng', lng)
    location = js['results'][0]['formatted_address']
    print(location)

    print('----------------------------------------------------')

    #print(" js[0][address_components]-len()", len(js['results'][0]["address_components"]), js['results'][0]["address_components"])
    print(" js[0][address_components]-len()",   len(js['results'][0]["address_components"]),    js['results'][0]["address_components"])
    print(" js[0][formatted_address]-len()",    len(js['results'][0]["formatted_address"]),     js['results'][0]["formatted_address"])
    print(" js[0][geometry]-len()",             len(js['results'][0]["geometry"]),              js['results'][0]["geometry"])
    print(" js[0][place_id]-len()",             len(js['results'][0]["place_id"]),              js['results'][0]["place_id"])
    print(" js[0][types]-len()",                len(js['results'][0]["types"]),                 js['results'][0]["types"])
    print(" js[0][types]- len():",      len(js['results'][0]))
    print(" js[0][types]- keys():",      js['results'][0].keys())
    lst = list()
    print('------------------------------------------------------')
    '''
     js[0][address_components]-len() 4 [{'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['locality', 'political']}, {'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Punjab', 'short_name': 'Punjab', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Pakistan', 'short_name': 'PK', 'types': ['country', 'political']}]
     js[0][formatted_address]-len() 24 Lahore, Punjab, Pakistan
     js[0][geometry]-len() 4 {'bounds': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}, 'location': {'lat': 31.5203696, 'lng': 74.35874729999999}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}}
     js[0][place_id]-len() 27 ChIJ2QeB5YMEGTkRYiR-zGy-OsI
     js[0][types]-len() 2 ['locality', 'political']
     js[0][types]- len(): 5
     js[0][types]- keys(): dict_keys(['address_components', 'formatted_address', 'geometry', 'place_id', 'types'])
    '''

    for k, v in js['results'][0].items():
        print('------------------------------------------------------')
        newtup = (k, v)
        lst.append(newtup)
        lst = sorted(lst, reverse=False)
    for l in lst:
        print(l[0], l[1])
    print('------------------------------------------------------')


    '''
    address_components [{'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['locality', 'political']}, {'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Punjab', 'short_name': 'Punjab', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Pakistan', 'short_name': 'PK', 'types': ['country', 'political']}]
    formatted_address Lahore, Punjab, Pakistan
    geometry {'bounds': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}, 'location': {'lat': 31.5203696, 'lng': 74.35874729999999}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}}
    place_id ChIJ2QeB5YMEGTkRYiR-zGy-OsI
    types ['locality', 'political']
    ------------------------------------------------------
    '''
    print("")
    print("address_components ")
    # address_components [{'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['locality', 'political']}, {'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Punjab', 'short_name': 'Punjab', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Pakistan', 'short_name': 'PK', 'types': ['country', 'political']}]
    print("len():", len(js['results'][0]["address_components"]))
    for i in range(len(js['results'][0]["address_components"])):
        print("address_component list item nr:",i)
        for k, v in js['results'][0]["address_components"][i].items():
            print('------------------------------------------------------')
            newtup = (k, v)
            lst.append(newtup)
            lst = sorted(lst, reverse=False)
        for l in lst:
            print("l[0], l[1]:", l[0], l[1])

        print('------------------------------------------------------')



'''

print(json.dumps(js, indent=4))
"results": [
        {
            "address_components": [
                {
                    "long_name": "Lahore",
                    "short_name": "Lahore",
                    "types": [
                        "locality",
                        "political"
                    ]
                },
                {
                    "long_name": "Lahore",
                    "short_name": "Lahore",
                    "types": [
                        "administrative_area_level_2",
                        "political"
                    ]
                },
                {
                    "long_name": "Punjab",
                    "short_name": "Punjab",
                    "types": [
                        "administrative_area_level_1",
                        "political"
                    ]
                },
                {
                    "long_name": "Pakistan",
                    "short_name": "PK",
                    "types": [
                        "country",
                        "political"
                    ]
                }
            ],
            "formatted_address": "Lahore, Punjab, Pakistan",
            "geometry": {
                "bounds": {
                    "northeast": {
                        "lat": 31.69848649999999,
                        "lng": 74.5532399
                    },
                    "southwest": {
                        "lat": 31.2673942,
                        "lng": 74.1155387
                    }
                },
                "location": {
                    "lat": 31.5203696,
                    "lng": 74.35874729999999
                },
                "location_type": "APPROXIMATE",
                "viewport": {
                    "northeast": {
                        "lat": 31.69848649999999,
                        "lng": 74.5532399
                    },
                    "southwest": {
                        "lat": 31.2673942,
                        "lng": 74.1155387
                    }
                }
            },
            "place_id": "ChIJ2QeB5YMEGTkRYiR-zGy-OsI",
            "types": [
                "locality",
                "political"
            ]
        }
    ],
    "status": "OK"
}
'''
