import re
import socket
import os
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl
'''
Metacharacter 	Meaning
. 	matches any single character
* 	matches zero or more occurrences of the previous RE
+ 	matches one or more occurrences of the preceding RE
? 	matches zero or one occurrences of the preceding RE
| 	matches RE before or RE after bar (logical OR)
^ 	means beginning of line (“^D” matches all strings starting with D)
$ 	means end of line (“d$” matches all strings ending with d)
\ 	used to quote other metacharacters
[ and ] 	indicate a set/range of characters. Any characters in the set will match. 
            ^ before the set means “not”. 
            - between characters indicates a range
( and ) 	used to group REs when using other metacharacters.
{ and } 	specify the number of repetitiimport re
import socket
import os
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import sslons of an RE


Python Regular Expression Quick Guide

^        Matches the beginning of a line
$        Matches the end of the line
.        Matches any character
\s       Matches whitespace
\S       Matches any non-whitespace character
*        Repeats a character zero or more times
*?       Repeats a character zero or more times 
         (non-greedy)
+        Repeats a character one or more times
+?       Repeats a character one or more times 
         (non-greedy)
[aeiou]  Matches a single character in the listed set
[^XYZ]   Matches a single character not in the listed set
[a-z0-9] The set of characters can include a range
(        Indicates where string extraction is to start
)        Indicates where string extraction is to end
'''


lst = [
"X-Sieve: CMU Sieve 2.3",
"X-DSPAM-Result: Innocent",
"S-DSPAM-Confidence: 0.8475",
"X-Content-Type-Message-Body: text/plain",
"X-Plane is behind schedule: two weeks"
]
pattern = "^X.*:"
for l in lst:
    if re.match( pattern,l):
        print("MATCHED", pattern, l)
    else:
        print("NO Match", pattern, l)
print("")
'''
MATCHED ^X.*: X-Sieve: CMU Sieve 2.3
MATCHED ^X.*: X-DSPAM-Result: Innocent
NO Match ^X.*: S-DSPAM-Confidence: 0.8475
MATCHED ^X.*: X-Content-Type-Message-Body: text/plain
MATCHED ^X.*: X-Plane is behind schedule: two weeks
'''
pattern = "^X-\S+:"
for l in lst:
    if re.match( pattern,l):
        print("MATCHED", pattern, l)
    else:
        print("NO Match", pattern, l)
'''
MATCHED ^X-\S+: X-Sieve: CMU Sieve 2.3
MATCHED ^X-\S+: X-DSPAM-Result: Innocent
NO Match ^X-\S+: S-DSPAM-Confidence: 0.8475
MATCHED ^X-\S+: X-Content-Type-Message-Body: text/plain
NO Match ^X-\S+: X-Plane is behind schedule: two weeks
'''
s = " my 2 favourite numbers are 9 and 42 "
s2 = " my 2 fAvourite numbers are 9 and 42 "

pattern = "[0-9]+" # one or more digits
pattern2= "[AEIOU]+"
#extract digits
print(re.findall(pattern,s))
# ['2','9','42']
print(re.findall(pattern2,s))
print(re.findall(pattern2,s2))


print("")
'''

dic = {
"p3": "\s",   #Matc whitespaces
"p4": "\S",   #Match any non-white space
"p5": "*",    #repeats a character zero or more time
"p6": "*?",   #repeats a character zero or more time, non greedy
"p7": "*",    #repeats a character one or more time
"p8": "*?",  #repeats a character one or more time, non greedy
"p9": "[aeiou]",  #Matches a single character in the listed
"p10": "^XYZ",    #Matches a single character not in the list
"p11": "[a-z0-9]", # set of character include a range
"p12": "(",   #indicates where string extraction need to start
"p13": ")",   #indicates where string extraction need to end
"p14": "F.+:",
"p15":  "F+?:",
"p16": "[0-9]+", # one or more digits
"p17": "[AEIOU]+"
}
#for k,v in dic.items():
#    print(k,v)
#print(dic)
print("list of patterns")
s1 = "From using the : character"
s2 = " my 2 favourite numbers are 9 and 42 "
s3 = " my 2 fAvourite numbers are 9 and 42 "
s2 = " my 2 fAvourite numbers are 9 and 422 "
for k,v in dic.items():
    #print(re.search(v,s1))
    #print(re.findall(v,s1))
    print(v)
    #print(re.findall(dic["p16"], s2))
    print(re.findall(v, s2))


#print(re.findall("[0-9]+",s2))
print(dic["p16"])
#for i in range(1,20): # not included last number
    #p0 = "p" + str(i)
    #print("p"+str(i))
    #print(i,"- pattern:", "p"+str(i),"- string:",re.search("p"+str(i),s3))
        #print(pattern1, re.findall(pattern1,s)) # F.+: ['From using the :']
'''
s= "From: Using the : character"
y = re.findall("^F.+:",s)
print("1", y)    #['From: Using the :']
y = re.findall("^F.:",s)
print("2", y)    #[]
y = re.findall("^F.",s)
print("3", y)    #3 ['Fr']
y = re.findall("^F+:",s)
print("4", y)    #[]
y = re.findall("^F.",s)
print("5", y)    #['Fr']
y = re.findall("^F.+",s)
print("6", y)    #['From: Using the : character']

# Non greedy matching
y = re.findall("^F.+?",s)
print("7", y)    #7 ['Fr']
y = re.findall("^F.+?:",s)
print("8", y)    #8 ['From:']

print("")
s = "From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008"
y = re.findall("\S+@\S+",s)
print("11", y)    #11 ['stephen.marquard@uct.ac.za']
y = re.findall("\S@\S",s)
print("12", y)    #12 ['d@u']
y = re.findall("\S.@\S......",s)
print("122", y)    #122 ['rd@uct.ac.']
y = re.findall(".@.",s)
print("13", y)    #13 ['d@u']
y = re.findall(".+@.+",s)
print("14", y)    #14 ['From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008']
y = re.findall(".+@+",s)
print("15", y)    #15 ['From stephen.marquard@']

print("")
y = re.findall("^From (\S+@\S+)",s)
print("21", y)    #21 ['stephen.marquard@uct.ac.za']

#double split
words = s.split()
email = words[1]
pieces = email.split("@")
print (pieces[0])   #stephen.marquard
print(pieces[1])    #uct.ac.za

#Regex version
print("")
y = re.findall("@([^ ]*)",s)
print("31", y)    #31 ['uct.ac.za']

y = re.findall("@([ ]*)",s)
print("32", y)    #32 ['']

y = re.findall("@([^ ])",s)
print("33", y)    #33 ['u']

print("")
#using re.search() like find()
count = 0
#pythonForEveryOne/data/mbox-short.txt
print (os.getcwd())
#handle = open("mbox-short.txt")
handle =open("mbox-short.txt")
#handle = open("../PY4E/data/mbox-short.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    if line.find("From: ") >= 0 :
        count +=1
print("find() Method to search - count:", count)

#using re.search() like find()
count = 0
handle = open("mbox-short.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    if re.search("From: ", line)  :
        count += 1
print("re.search() Method to search - count:", count)

#using startswith()
count = 0
handle = open("mbox-short.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    if line.startswith("From: ")  :
        count += 1
print("line.startswith() Method to search - count:", count)

print()
# Spam Conf ^X-DSPAM-Confidence: ([0-9.]+)idence
count = 0
sval =""
fval =0.0
numlist = list()
handle = open("mbox-short.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    if line.find("X-DSPAM-Confidence") >= 0 :
        count +=1
        #print(line)     #X-DSPAM-Confidence: 0.8475
        sval = re.findall("^X-DSPAM-Confidence: ([0-9.]+)", line)   #['0.8475']
        #sval = re.findall("^X-DSPAM-Confidence: ([0-9.])", line)   #['0']
        #sval = re.findall("^X-DSPAM-Confidence: ([0-9]+)", line)   #['0']
        #sval = re.findall("^X-DSPAM-Confidence: ([.0-9.]+)", line)   #['0.8475']
        #sval = re.findall("^X-DSPAM-Confidence: ([.0-9]+)", line)   #['0.8475']
        #sval = re.findall("X-DSPAM-Confidence", line)
        #fval = float(sval)
        #print(len(sval))    # 1
        if len(sval) != 1: continue
        #print(sval) # ['0.8475'] , it is a list, with one value at index 0
        fval = float(sval[0])
        numlist.append(fval)
print("max spam confidence value:", max(numlist))
print("X-DSPAM-Confidence using find() -  count :", count)  #max spam confidence value: 0.9907


print("")
# Escape Characters

x = " We just received $100.5 for cookies"
'''
y = re.findall("[0-9]",x)       #['1', '0', '0', '5']
print(y)
y = re.findall("[0-9.]",x)      #['1', '0', '0', '.', '5']
print(y)
y = re.findall("([0-9])",x)     #['1', '0', '0', '5']
print(y)
y = re.findall("([0-9]+)",x)    #['100', '5']
print(y)
y = re.findall("([0-9.]+)",x)   #['100.5']
print(y)
'''
y = re.findall("(\$[0-9.]+)",x)   #['$100.5']
print(y)

print("")
x = 'From: Using the : character'
y = re.findall('^F.+:', x)  #['From: Using the :']
print(y)
y = re.findall('^F.*:', x)  #['From: Using the :']
print(y)
y = re.findall('^F+:', x)  #[]
print(y)
y = re.findall('^F*:', x)  #[]
print(y)
y = re.findall('^F.:', x)  #[]
print(y)
y = re.findall('^F.', x)  #['Fr']
print(y)
y = re.findall('^F...', x)  #['From']
print(y)
y = re.findall('^F....', x)  #['From:']
print(y)

print("")
x = "From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008"
y = re.findall('\S+?@\S+', x)  #['stephen.marquard@uct.ac.za']
print(y)
y = re.findall('\S+?@\S+?', x)  #['stephen.marquard@u']
print(y)

y = re.findall('\S+@\S+', x)  #['stephen.marquard@uct.ac.za']
print(y)

y = re.findall('\S+?@\S*?', x)  #['stephen.marquard@']
print(y)
y = re.findall('\S+?@\S?', x)  #['stephen.marquard@u']
print(y)
y = re.findall('\S+?@\S', x)  #['stephen.marquard@u']
print(y)

print("")
# An HTTP request in Python

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)
while True:
    data = mysock.recv(512)
    if len(data) < 1: break
    print(data.decode(),end='')
mysock.close()


# Assignment
print("")
mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)
while True:
    data = mysock.recv(512)
    if len(data) < 1: break
    print(data.decode(),end='')
mysock.close()


print("")
# Python code to demonstate Byte Decoding

# initialising a String
a = 'GeeksforGeeks'

# initialising a byte object
c = b'GeeksforGeeks'

# using decode() to decode the Byte object
# decoded version of c is stored in d
# using ASCII mapping
d = c.decode('ASCII')
print(a)
print(c)
print(d)

print("")
fhand = urllib.request.urlopen("http://data.pr4e.org/romeo.txt ")
for line in fhand:
    print(line.decode().strip())

print("")
print("count words usning Dict")
counts = 0
dic = dict()
fhand = urllib.request.urlopen("http://data.pr4e.org/romeo.txt ")
for line in fhand:
    words = line.decode().split()
    for word in words:
        dic[word] = dic.get(word,0)+1
print(dic)


#url
print("")
fhand = urllib.request.urlopen("http://www.dr-chuck.com/page1.htm")
for line in fhand:
    print(line.decode().strip())


print("#########################################")
# Getting BeautifulSoup instaled through Pycharm
# File -> Settings -> Project Interpreter -> manage inside each project packages and install
# beautifulsoup
# Ignore SSL certificate errors

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#url = input('Enter http://data.pr4e.org/romeo.txt  http://dr-chuck.com/ http://www.dr-chuck.com/page1.htm:- ')
html = urllib.request.urlopen("http://www.dr-chuck.com/page1.htm", context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

print("list of URLS searched: ")
# Retrieve all of the anchor tags
tags = soup('a')
print("tags:" ,tags)
for tag in tags:
    print(tag.get('href', None))
