import json


#JSON represents data as nested lists or dictionaries

input = '''[
    {   "id" : "001",
        "x" : "2",
        "name": "Chuck"
    },    
    {   "id" : "009",
        "x" : "7",
        "name": "Chuck"
    }   
]'''

info = json.loads(input)
print("user Count in list:", len(info))
print("list;", info)
for item in info:
    print("id:",item["id"])
    print("attribute:",item["x"])
    print("name",item["name"])
    print('---------------------------')

