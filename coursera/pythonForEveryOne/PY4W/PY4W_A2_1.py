import re
import socket
import os
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl


'''

Scraping Numbers from HTML using BeautifulSoup In this assignment you will write a Python program similar to 
http://www.py4e.com/code3/urllink2.py. The program will use urllib to read the HTML from the data files 
below, and parse the data, extracting numbers and compute the sum of the numbers in the file.

We provide two files for this assignment. One is a sample file where we give you the sum for your testing 
and the other is the actual data you need to process for the assignment.

    Sample data: http://py4e-data.dr-chuck.net/comments_42.html (Sum=2553)
    Actual data: http://py4e-data.dr-chuck.net/comments_601571.html (Sum ends with 83)

You do not need to save these files to your folder since your program will read the data directly 
from the URL. Note: Each student will have a distinct data url for the assignment - so only use your 
own data url for analysis.

Data Format

The file is a table of names and comment counts. You can ignore most of the data in the 
file except for lines like the following:

<tr><td>Modu</td><td><span class="comments">90</span></td></tr>
<tr><td>Kenzie</td><td><span class="comments">88</span></td></tr>
<tr><td>Hubert</td><td><span class="comments">87</span></td></tr>

You are to find all the <span> tags in the file and pull out the numbers from the tag and sum the numbers.

Look at the sample code provided. It shows how to find all of a certain kind of tag, loop 
through the tags and extract the various aspects of the tags.

...
# Retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
   # Look at the parts of a tag
   print 'TAG:',tag
   print 'URL:',tag.get('href', None)
   print 'Contents:',tag.contents[0]
   print 'Attrs:',tag.attrs

You need to adjust this code to look for span tags and pull out the text content of the span tag, 
convert them to integers and add them up to complete the assignment. 
'''



#        sval = re.findall("^X-DSPAM-Confidence: ([0-9.]+)", line)   #['0.8475']
#y = re.findall("^F.+",s)
#print("6", y)    #['From: Using the : characte
#
#ival = int(lst[0])

s1 = 'gfgfdAAA1234ZZZuijjk'
ss = re.findall('AAA(.+)ZZZ', s1)    #['1234']
print(ss)

#   [arn] 	Returns a match where one of the specified characters (a, r, or n) are present
txt = "The rain in Spain"
#Check if the string has any a, r, or n characters:
x = re.findall("[arn]", txt)
print(x)    #['r', 'a', 'n', 'n', 'a', 'n']
# https://www.w3schools.com/python/python_regex.asp

txt = "The rain in Spain"
#Check if the string has other characters than a, r, or n:
x = re.findall("[^arn]", txt)
print(x)    #['T', 'h', 'e', ' ', 'i', ' ', 'i', ' ', 'S', 'p', 'i']

string = """Hello my Number is 123456789 and  
             my friend's number is 987654321"""
# A sample regular expression to find digits.
regex = '\d+'
match = re.findall(regex, string)
print(match)    #['123456789', '987654321']




#test, extract number out between span tags
# txt = "We are the so-called \"Vikings\" from the north."  # escape character
s = "<tr><td>Modu</td><td><span class=\"comments\">90</span></td></tr>"
sval = ""
ival = 0
lst=list()
sumlist = list()


lst = re.findall("<span.",s)    #['<span ']
print(lst)
lst = re.findall("<span(.+)span>",s)    #[' class="comments">90</'] 0
print(lst, ival)

lst = re.findall("<span .+ span>$",s)    #[' class="comments">90</'] 0
print(lst, ival)

lst = re.findall("([1-9].)",s)    #['90']
print(lst)


print("#########################################")
# Getting BeautifulSoup instaled through Pycharm
# File -> Settings -> Project Interpreter -> manage inside each project packages and install
# beautifulsoup
# Ignore SSL certificate errors


#https://www.crummy.com/software/BeautifulSoup/bs4/doc/
print('###############################################################################')
print('###############################################################################')

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#url = input('http://py4e-data.dr-chuck.net/comments_42.html ')
html = urllib.request.urlopen("http://py4e-data.dr-chuck.net/comments_42.html", context=ctx).read()
#html = urllib.request.urlopen("http://www.dr-chuck.com/page1.htm", context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
#tags = soup('a')   # Retrieve all of the anchor tags
tags = soup()
#print("tags:" ,tags)    # <tr><td>Inika</td><td><span class="comments">2</span></td></tr>
tags = soup('span')
for tag in tags:
    # Look at the parts of a tag
    print('###############################################################################')
    print("type of tag:", type(tag))
    print("name of tag:", tag.name)
    print('TAG:', tag)
    print('URL:', tag.get('href', None))
    print('Contents:', tag.contents[0])
    print('Attrs:', tag.attrs)
    #print(tag.get('href', None))

sum =0
s=""
for tag in tags:
    #print(type(tag))
    #lst = re.findall("([1-9].)", tag)  #
    #print(lst)
    #print(tag)
    #print('Contents:',tag.contents[0])
    s = re.findall("([1-9].)", tag.contents[0])
    if s == []: continue
    sum = sum + int(s[0])
print(sum)  # 2553

print("")
########################################################################
########################################################################
############################ Exercise actual
# Actual data: http://py4e-data.dr-chuck.net/comments_601571.html (Sum ends with 2183)


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

html = urllib.request.urlopen("http://py4e-data.dr-chuck.net/comments_601571.html", context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
tags = soup('span')
sum =0
s=""
i =0
for tag in tags:
    s = re.findall("([1-9].*)", tag.contents[0])
    i=i+1
    if s == []: continue
    sum = sum + int(s[0])
    print(i, tag, s, int(s[0]),sum)
    #print(i, tag, s,sum)
print(sum)  #2183

