import urllib.request, urllib.parse, urllib.error
import json
import ssl

'''
 Calling a JSON API
In this assignment you will write a Python program somewhat similar to http://www.py4e.com/code3/geojson.py.
 The program will prompt for a location, contact a web service and retrieve JSON for the web service and
 parse that data, and retrieve the first place_id from the JSON. A place ID is a textual identifier that
 uniquely identifies a place as within Google Maps.

API End Points

To complete this assignment, you should use this API endpoint that has a static subset of the Google Data:

http://py4e-data.dr-chuck.net/json?

This API uses the same parameter (address) as the Google API. This API also has no rate limit so you can test
 as often as you like. If you visit the URL with no parameters, you get "No address..." response.

To call the API, you need to include a key= parameter and provide the address that you are requesting as the
 address= parameter that is properly URL encoded using the urllib.parse.urlencode() function as shown in
 http://www.py4e.com/code3/geojson.py

Make sure to check that your code is using the API endpoint is as shown above. You will get different results
 from the geojson and json endpoints so make sure you are using the same end point as this autograder is using.

Test Data / Sample Execution

You can test to see if your program is working with a location of "South Federal University" which will have a
 place_id of "ChIJ0V94rPl_bIcRqLdrlbjFMDk".

$ python3 solution.py
Enter location: South Federal University
Retrieving http://...
Retrieved 2505 characters
Place id ChIJ0V94rPl_bIcRqLdrlbjFMDk

Turn In

Please run your program to find the place_id for this location:

UNISA

Make sure to enter the name and case exactly as above and enter the place_id and your Python code
 below. Hint: The first seven characters of the place_id are "ChIJOZU ..."

Make sure to retreive the data from the URL specified above and not the normal Google API. Your program
 should work with the Google API - but the place_id may not match for this assignment.
'''


api_key = False
# If you have a Google Places API key, enter it here
# api_key = 'AIzaSy___IDByT70'
# https://developers.google.com/maps/documentation/geocoding/intro

if api_key is False:
    api_key = 42
    serviceurl = 'http://py4e-data.dr-chuck.net/json?'
else :
    serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True:
    address = input('Enter location - South Federal University: ')
    #address = "South Federal University"

    if len(address) < 1: break

    parms = dict()
    parms['address'] = address
    if api_key is not False: parms['key'] = api_key
    url = serviceurl + urllib.parse.urlencode(parms)
    #print("params:", parms) #params: {'address': 'lahore', 'key': 42}

    #print('Retrieving', url)    #Retrieving http://py4e-data.dr-chuck.net/json?address=lahore&key=42
    uh = urllib.request.urlopen(url, context=ctx)
    #print("uh", uh) #uh <http.client.HTTPResponse object at 0x7fc015de6cc0>
    data = uh.read().decode()
    print('Retrieved', len(data), 'characters') #Retrieved 1747 characters

    try:
        js = json.loads(data)
    except:
        js = None
    #print("js", js)   #{'results': [{'address_components': [{'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['locality', 'political']}, {'long_name': 'Lahore', 'short_name': 'Lahore', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Punjab', 'short_name': 'Punjab', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Pakistan', 'short_name': 'PK', 'types': ['country', 'political']}], 'formatted_address': 'Lahore, Punjab, Pakistan', 'geometry': {'bounds': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}, 'location': {'lat': 31.5203696, 'lng': 74.35874729999999}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 31.69848649999999, 'lng': 74.5532399}, 'southwest': {'lat': 31.2673942, 'lng': 74.1155387}}}, 'place_id': 'ChIJ2QeB5YMEGTkRYiR-zGy-OsI', 'types': ['locality', 'political']}], 'status': 'OK'}
    if not js or 'status' not in js or js['status'] != 'OK':
        print('==== Failure To Retrieve ====')
        #print(data)
        continue
    print("Place id:- ChIJ0V94rPl_bIcRqLdrlbjFMDk")
    #print(json.dumps(js, indent=4))
    print("first found place-id", js['results'][0]["place_id"]) # ChIJOZUawoSGbIcR-hWbgTxSavw


