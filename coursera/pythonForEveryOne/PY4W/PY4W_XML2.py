import xml.etree.ElementTree as ET

print("##############################################################")

input = '''<stuff>
    <users>
        <user x="2">
            <id>001</id>
            <name>Chuck</name>
        </user>        
        <user x="7">
            <id>009</id>
            <name>Brent</name>
        </user>
    </users>
</stuff>'''
lst = list()
stuff = ET.fromstring(input)
lst = stuff.findall('users/user')
print("len: & list:", len(lst), lst)

for item in lst:
    print(item)
    print('Name:', item.find('name').text)
    print('id:', item.find('id').text)
    print('Attribute:', item.get('x'))
    print("-------------------")

