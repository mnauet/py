import json
import urllib.request, urllib.parse, urllib.error
import ssl
'''
 Extracting Data from JSON

In this assignment you will write a Python program somewhat similar to 
 http://www.py4e.com/code3/json2.py. The program will prompt for a URL, read the 
 JSON data from that URL using urllib and then parse and extract the comment counts from 
 the JSON data, compute the sum of the numbers in the file and enter the sum below:

We provide two files for this assignment. One is a sample file where we give you the sum for 
 your testing and the other is the actual data you need to process for the assignment.

    Sample data: http://py4e-data.dr-chuck.net/comments_42.json (Sum=2553)
    Actual data: http://py4e-data.dr-chuck.net/comments_601574.json (Sum ends with 27)

You do not need to save these files to your folder since your program will read the data directly 
 from the URL. Note: Each student will have a distinct data url for the assignment - 
     so only use your own data url for analysis.

Data Format

The data consists of a number of names and comment counts in JSON as follows:

{
  comments: [
    {
      name: "Matthias"
      count: 97
    },
    {
      name: "Geomer"
      count: 97
    }
    ...
  ]
}

The closest sample code that shows how to parse JSON and extract a list is json2.py. 
 You might also want to look at geoxml.py to see how to prompt for a URL and retrieve data from a URL.

Sample Execution

$ python3 solution.py
Enter location: http://py4e-data.dr-chuck.net/comments_42.json
Retrieving http://py4e-data.dr-chuck.net/comments_42.json
Retrieved 2733 characters
Count: 50
Sum: 2...

'''

lst = list()
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
lst = list()
counts = list()
sum = 0


url = "http://py4e-data.dr-chuck.net/comments_42.json"    # (Sum=2553)
d = urllib.request.urlopen(url, context=ctx).read()
data = d.decode()
j = json.loads(data)

sum = 0
for i in range(len(j['comments'])):
#    print("count: ", j['comments'][i]["count"])
    sum = sum + int(j['comments'][i]["count"])
print(sum)  #2553



lst = list()
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
lst = list()
counts = list()
sum = 0


url = "http://py4e-data.dr-chuck.net/comments_601574.json"
d = urllib.request.urlopen(url, context=ctx).read()
data = d.decode()
j = json.loads(data)
print(j)   #{'note': 'This file contains the actual data for your assignment', 'comments': [{'name': 'Maisi', 'count': 99}, {'name': 'Bogdan', 'count': 95}, {'name': 'Macaully', 'count': 94}, {'name': 'Lanakai', 'count': 94}, {'name': 'Kayley', 'count': 93}, {'name': 'Faiza', 'count': 92}, {'name': 'Raimee', 'count': 88}, {'name': 'Zeid', 'count': 88}, {'name': 'Xida', 'count': 88}, {'name': 'Khaia', 'count': 85}, {'name': 'Zella', 'count': 81}, {'name': 'Carragh', 'count': 76}, {'name': 'Lileidh', 'count': 75}, {'name': 'Hosea', 'count': 73}, {'name': 'Chu', 'count': 72}, {'name': 'Oluwateniolami', 'count': 70}, {'name': 'Haghdann', 'count': 66}, {'name': 'Annmarie', 'count': 64}, {'name': 'Mysha', 'count': 64}, {'name': 'Isira', 'count': 59}, {'name': 'Shannyn', 'count': 57}, {'name': 'Evangeline', 'count': 56}, {'name': 'Courtneylee', 'count': 52}, {'name': 'Wynn', 'count': 51}, {'name': 'Tatiana', 'count': 49}, {'name': 'Justin', 'count': 49}, {'name': 'Mckenzie', 'count': 45}, {'name': 'Willum', 'count': 39}, {'name': 'Bethlin', 'count': 38}, {'name': 'Ishwar', 'count': 38}, {'name': 'Orlaithe', 'count': 37}, {'name': 'Karmen', 'count': 36}, {'name': 'Kelsay', 'count': 34}, {'name': 'Honey', 'count': 33}, {'name': 'Chintu', 'count': 31}, {'name': 'Stefany', 'count': 27}, {'name': 'Mirran', 'count': 26}, {'name': 'Darrie', 'count': 25}, {'name': 'Nicole', 'count': 24}, {'name': 'Myrna', 'count': 24}, {'name': 'Micaylah', 'count': 24}, {'name': 'Joy', 'count': 24}, {'name': 'Wabuya', 'count': 18}, {'name': 'Monty', 'count': 16}, {'name': 'Aila', 'count': 12}, {'name': 'Teghan', 'count': 12}, {'name': 'Vladimir', 'count': 11}, {'name': 'Aman', 'count': 9}, {'name': 'Leiten', 'count': 8}, {'name': 'Heidar', 'count': 6}]}

sum = 0
for i in range(len(j['comments'])):
#    print("count: ", j['comments'][i]["count"])
    sum = sum + int(j['comments'][i]["count"])
print(sum)  #2527