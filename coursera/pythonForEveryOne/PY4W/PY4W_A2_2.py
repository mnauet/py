import re
import socket
import os
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

'''
Welcome malik naveed akram from Using Python to Access Web Data

Following Links in Python

In this assignment you will write a Python program that expands on http://www.py4e.com/code3/urllinks.py. 
The program will use urllib to read the HTML from the data files below, extract the href= vaues from the 
anchor tags, scan for a tag that is in a particular position relative to the first name in the list, 
follow that link and repeat the process a number of times and report the last name you find.

We provide two files for this assignment. One is a sample file where we give you the name for your 
testing and the other is the actual data you need to process for the assignment

    Sample problem: Start at http://py4e-data.dr-chuck.net/known_by_Fikret.html
    Find the link at position 3 (the first name is 1). Follow that link. Repeat this process 4 times. 
    The answer is the last name that you retrieve.
    Sequence of names: Fikret Montgomery Mhairade Butchi Anayah
    Last name in sequence: Anayah
    Actual problem: Start at: http://py4e-data.dr-chuck.net/known_by_Elis.html
    Find the link at position 18 (the first name is 1). Follow that link. Repeat this process 7 times. 
    The answer is the last name that you retrieve.
    Hint: The first character of the name of the last page that you will load is: T

Strategy

The web pages tweak the height between the links and hide the page after a few seconds to make 
it difficult for you to do the assignment without writing a Python program. But frankly with a 
little effort and patience you can overcome these attempts to make it a little harder to complete 
the assignment without writing a Python program. But that is not the point. The point is to write 
a clever Python program to solve the program.

Sample execution

Here is a sample execution of a solution:

$ python3 solution.py
Enter URL: http://py4e-data.dr-chuck.net/known_by_Fikret.html
Enter count: 4
Enter position: 3
Retrieving: http://py4e-data.dr-chuck.net/known_by_Fikret.html
Retrieving: http://py4e-data.dr-chuck.net/known_by_Montgomery.html
Retrieving: http://py4e-data.dr-chuck.net/known_by_Mhairade.html
Retrieving: http://py4e-data.dr-chuck.net/known_by_Butchi.html
Retrieving: http://py4e-data.dr-chuck.net/known_by_Anayah.html

The answer to the assignment for this execution is "Anayah". 
'''

'''
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

html = urllib.request.urlopen("http://py4e-data.dr-chuck.net/comments_601571.html", context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
tags = soup('span')
sum =0
s=""
i =0
for tag in tags:
    s = re.findall("([1-9].*)", tag.contents[0])
    i=i+1
    if s == []: continue
    sum = sum + int(s[0])
    #print(i, tag, s, int(s[0]),sum)
print(sum)  #2183



# CTRL + D  duplicate line
# CTRL + Y  delete line
'''

#equence of names: Fikret Montgomery Mhairade Butchi Anayah
#ast name in sequence: Anayah
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = "http://py4e-data.dr-chuck.net/known_by_Fikret.html"  #actual file
#url = " http://py4e-data.dr-chuck.net/known_by_Elis.html"   #sample file
lst = list()
for x in range (1,5):
    #print(x)
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    tags = soup('a')
    i = 1
    for tag in tags:
        if i==3 :
            url = (tag.get('href', None))
            break
        i+=1
    #print (url)
    #print(re.findall("\S+known_by_\S+",url))
    pieces = url.split("known_by_")
    pieces = pieces[1].split(".")
    #print(pieces[0])  # uct.ac.za
    lst.append(pieces[0])
#print(lst)
print(lst[-1])  #Anayah


# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#url = "http://py4e-data.dr-chuck.net/known_by_Fikret.html"  #actual file
url = " http://py4e-data.dr-chuck.net/known_by_Elis.html"   #sample file
lst = list()
for x in range (1,8):
    #print(x)
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    tags = soup('a')
    i = 1
    for tag in tags:
        if i==18 :
            url = (tag.get('href', None))
            break
        i+=1
    #print (url)
    #print(re.findall("\S+known_by_\S+",url))
    pieces = url.split("known_by_")
    pieces = pieces[1].split(".")
    #print(pieces[0])  # uct.ac.za
    lst.append(pieces[0])
#print(lst)
print(lst[-1])  #Thomas