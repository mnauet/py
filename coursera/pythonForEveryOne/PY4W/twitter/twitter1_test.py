import urllib.request, urllib.parse, urllib.error
import twurl
import ssl

# https://apps.twitter.com/
# Create App and get the four strings, put them in hidden.py

# twitter mnauet , imrankhanpti, drchuck realdonaldtrump
TWITTER_URL = 'https://api.twitter.com/1.1/statuses/user_timeline.json'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True:
    print('')
    acct = input('Enter Twitter Account:')
    if (len(acct) < 1): break
    url = twurl.augment(TWITTER_URL,
                        {'screen_name': acct, 'count': '2'})
    print('Retrieving url: ', url)
    connection = urllib.request.urlopen(url, context=ctx)
    d = connection.read()
    print('raw data: ', d)
    data = d.decode()
    print('decoded data: ', data[:250])
    headers = dict(connection.getheaders())

    print('======================================')
    print('print headers')
    print(headers)
    for h in headers:
        print('key: ', h, 'value: ', headers[h])



    # print headers
    print('Remaining x-rate-limit-limit', headers['x-rate-limit-limit'])
    print('Remaining x-rate-limit-remaining', headers['x-rate-limit-remaining'])
