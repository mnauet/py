import re
#Phttps://www.py4e.com/tools/python-data/?PHPSESSID=63fef14948fd3c2887dcd40ca96d811b
'''
 Finding Numbers in a Haystack

In this assignment you will read through and parse a file with text and numbers. 
You will extract all the numbers in the file and compute the sum of the numbers.
Data Files

We provide two files for this assignment. One is a sample file where we give you the sum 
for your testing and the other is the actual data you need to process for the assignment.

    Sample data: http://py4e-data.dr-chuck.net/regex_sum_42.txt (There are 90 values with 
    a sum=445833)
    Actual data: http://py4e-data.dr-chuck.net/regex_sum_601569.txt (There are 69 values 
    and the sum ends with 889)

These links open in a new window. Make sure to save the file into the same folder as you 
will be writing your Python program. Note: Each student will have a distinct data file for
 the assignment - so only use your own data file for analysis. 
'''

#/home/malik/repos/py/coursera/pythonForEveryOne/data_Py4Web/regex_sum_27468.txt
print()
sval =""
ival =0
numlist = list()
handle = open("data/regex_sum_27468.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    sval = re.findall("([0-9]+)", line)
    if len(sval) == 0: continue
    for s in sval:
        ival = int(s)
        numlist.append(ival)
print("sum of integers:", sum(numlist))
#print(numlist)

#Sample data: http://py4e-data.dr-chuck.net/regex_sum_42.txt
# (There are 90 values with a
# sum=445833
print()

sval =""
ival =0
numlist = list()
handle = open("data/regex_sum_42.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    sval = re.findall("([0-9]+)", line)
    if len(sval) == 0: continue
    for s in sval:
        ival = int(s)
        numlist.append(ival)
print("sum of integers:", sum(numlist)) #sum of integers: 445833
#print(numlist)

#Actual data: http://py4e-data.dr-chuck.net/regex_sum_601569.txt
# (There are 69 values and the sum ends with 889)
print()

sval =""
ival =0
numlist = list()
handle = open("data/regex_sum_601569.txt")
for line in handle:
    line = line.strip()
    if len(line) < 1:
        continue
    sval = re.findall("([0-9]+)", line)
    if len(sval) == 0: continue
    for s in sval:
        ival = int(s)
        numlist.append(ival)
print("sum of integers:", sum(numlist)) #sum of integers: 321889