




'''
 7.1 Write a program that prompts for a file name, then opens that file and
 reads through the file, and print the contents of the file in upper case.
 Use the file words.txt to produce the output below.
You can download the sample data at http://www.py4e.com/code3/words.txt
'''
'''
fname = input("enter file name:")
xfile = open(fname, "r")
text = xfile.read()
text = text.strip()
print(text.upper())
'''

'''
 7.2 Write a program that prompts for a file name, then opens that file and reads 
 through the file, looking for lines of the form:
X-DSPAM-Confidence:    0.8475
Count these lines and extract the floating point values from each of the 
lines and compute the average of those values and produce an output as shown below. 
Do not use the sum() function or a variable named sum in your solution.
You can download the sample data at http://www.py4e.com/code3/mbox-short.txt when 
you are testing below enter mbox-short.txt as the file name.
'''

'''
count = 0
fvalue = 0.0
value = 0.0
average = 0.0

fname = input("enter file name - mbox-short.txt: ")
xfile = open(fname, "r")
data = xfile.readlines()
for each in data :
    if 'X-DSPAM-Confidence:' in each:
        count = count +1
        pos= each.find(':')
        svalue = each[pos+1:]
        fvalue = float(svalue.strip())
        value = value + fvalue
print("Average spam confidence:", value/count)
'''

'''

'''
# sum, len of num list
'''
numlist = []
while True:
    sval = input("enter an number [write done to exit]")
    if sval == 'done':
        break
    try :
        fval = float(sval)
    except ValueError:
        continue
    numlist.append(fval)
print("count:", len(numlist), "average:", sum(numlist)/len(numlist))
'''

#Strings and lists
'''
abc = "Malik Naveed Akram"
abc2 = "Malik;Naveed;Akram"
print (abc)
words = abc.split()
words2=abc2.split(';')
print("list", words)
print("list len", len(words))
for word in words:
    print("words in list, iterate in for looop:", word)
print("list2", words2)
'''

'''
 8.4 Open the file romeo.txt and read it line by line. For each line, split the line into a
list of words using the split() method. The program should build a list of words. For each
word on each line check to see if the word is already in the list and if not append it to
the list. When the program completes, sort and print the resulting words in 
alphabetical order.

You can download the sample data at http://www.py4e.com/code3/romeo.txt
'''
'''
wordlist = []
words = []
xfile = open("romeo.txt", "r")
for line in xfile.readlines():
    words = line.split()
    for word in words:
        if word  not in wordlist:
            wordlist.append(word)
wordlist.sort()
print(wordlist)
'''
'''
 8.5 Open the file mbox-short.txt and read it line by line. When you find a line 
 that starts with 'From ' like the following line:
From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008
You will parse the From line using split() and print out the second word in the line
 (i.e. the entire address of the person who sent the message). Then print out a count 
 at the end.
Hint: make sure not to include the lines that start with 'From:'.
You can download the sample data at http://www.py4e.com/code3/mbox-short.txt
'''
'''
wordlist = []
words = list()
xfile = open("mbox-short.txt", "r")
for line in xfile.readlines(): # even empty line has /n, one character
    words = line.split()
    print(len(line), len(wordlist))
    #if words == []: # ignore line, otherwise it bring out of range error
    if len(words) < 1 :
        continue
    if words[0] == "From": # 0 caused crash, as empty list does not have 0 index
        wordlist.append(words[1])  # i and only 1 also works inside this for loop
for each in wordlist:
    print(each)

print("There were", len(wordlist), "lines in the file with From as the first word")
'''

# Dictionaries
'''
d = {}
dd = {}
names = ["a", "b", "c", "d", "a", "a", "b"]
for name in names:
    dd[name] = dd.get(name, 0)+1
    if name not in d:
        d[name] = 1
    else:
        d[name] = d[name] + 1
for key in dd:
    print(key, dd[key])
print(list(dd))
print(d)
print(dd)
print(dd.keys())
print(d.values)
print(d.items())
print(d.keys())
print(len(d))
'''
'''
dict = {}
bigword = None
bigcount = 0
xfile = open("words.txt", "r")
#xfile = open("clown.txt")
for line in xfile:
    words = line.split()
    for word in words:
        dict[word] = dict.get(word,0) +1
print(dict)
for k,v in dict.items():
    if bigword is None or v > bigcount :
        bigword = k
        bigcount = v
print(bigword, bigcount)
'''
'''
9.4 Write a program to read through the mbox-short.txt and figure out who has sent 
the greatest number of mail messages. The program looks for 'From ' lines and takes 
    the second word of those lines as the person who sent the mail. The program creates 
    a Python dictionary that maps the sender's mail address to a count of the number of ' \
'times they appear in the file. After the ' \
'dictionary is produced, the program reads ' \
'through the dictionary using a maximum loop to ' \
'find the most prolific committer.
'''
'''
bigcount = 0
bigword = None
words = []
counts = {}
handle = open("mbox-short.txt", "r")
lines = handle.readlines()
for  line in lines:
    words = line.split()
    if words  == [] :
        continue
    if  words[0] == "From" :
            counts[words[1]] = counts.get(words[1],0) + 1
#print(counts)

for k,v in counts.items() :
    if bigword is None or  v > bigcount:
        bigword = k
        bigcount = v
print(bigword,bigcount)
'''

# 10 most common words
''' 
dic = dict()
handle = open("romeo.txt")
#lines = handle.read()
for line in handle:
    if len(line) < 1:
        continue
    words = line.split()
    #print(words)
    for word in words:
        dic[word] = dic.get(word,0)+1

print(dic)
print(dic.items())
lst = list()
for k,v in dic.items():
    newtup = (v,k)
    print(newtup)
    lst.append(newtup)
lst = sorted(lst,reverse=True)
print(lst)
for k in lst[:10]:
    print(k)
'''
'''
x,y = 3,4
print (x,y)
ldays = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') # list
ddays = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') # dict
tdays = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') # tuple

print ("print item in list:",ldays[2])
print ("print item in dict:", ddays[2])
print ("print item in tuples:", tdays[2])
'''

'''
10.2 Write a program to read through the mbox-short.txt and figure out the distribution 
by hour of the day for each of the messages. You can pull the hour out from the 'From ' 
line by finding the time and then splitting the string a second time using a colon.
From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008
Once you have accumulated the counts for each hour, print out the counts, 
sorted by hour as shown below.
04 3
06 1
07 1
09 2
10 3
11 6
14 1
15 2
16 4
17 2
18 1
19 1
'''

'''
dic = dict()
t = ""
hour = ""
handle = open("mbox-short.txt", "r")
lst = list()
for line in handle:
    words = line.split()
    if words == []:
        continue
    for word in words:
        if word == "From":
            t = words[5]
            hour = t[:2]
            dic[hour] = dic.get(hour,0) +1
for k,v in dic.items():
    newtup = (k,v)
    lst.append(newtup)
lst = sorted(lst,reverse=False)
for l in lst:
    print(l[0], l[1])


'''

# use library search function.
'''
wordlist = []
words = list()
xfile = open("mbox-short.txt", "r")
for line in xfile.readlines(): # even empty line has /n, one character
    line = line.rstrip()
    words = line.split()
    print(len(line), len(wordlist))
    #if words == []: # ignore line, otherwise it bring out of range error
    if len(words) < 1 :
        continue
    # 1st way,
    #if words[0] == "From": # 0 caused crash, as empty list does not have 0 index
    # 2nd way using search function
    #if re.search("From:", words[0]): # 0 caused crash, as empty list does not have 0 index

    # 3rd way #using re.search and puting condition if it is the starting expression ^
    #if re.search("^From:", words[0]):

    # 4th way, by using starts with in a line built in string function
    if line.startswith("From:"):
        wordlist.append(words[1])  # i and only 1 also works inside this for loop
for each in wordlist:
    print(each)

print("There were", len(wordlist), "lines in the file with From as the first word")
'''
