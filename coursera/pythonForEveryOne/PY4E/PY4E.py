import os
import re
# Assignment 3.1
'''
hrs = input("Enter Hours:")
rph = input("Enter rate per hour:")
try:
    rate = float(rph)
    h = float(hrs)
    result = h * rate
    if h > 40 :
        result = result + (h -40)* 0.5 * rate
    print(result)
except :
    print("wrong input")
'''

'''

3.3 Write a program to prompt for a score between 0.0 
and 1.0. If the score is out of range, print an error. 
If the score is between 0.0 and 1.0, print a grade 
using the following table:
Score Grade
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F
If the user enters a value out of range, print a 
suitable error message and exit. For the test, 
enter a score of 0.85. 
'''
'''
score = float(input("input score"))
if score in [0.0 , 0.85, 1.0]: # this range did not work, find a better way
    if score >= 0.9 :
        print("A")
    elif score >= 0.8 :
        print("B")
    elif score >= 0.7 :
        print("C")
    elif score >= 0.6 :
        print("D")
    else:
        print("F")
else:
    print("score is out of range")
'''


'''
4.6 Write a program to prompt the user for hours and 
rate per hour using input to compute gross pay. 
Pay should be the normal rate for hours up to 40 
nd time-and-a-half for the hourly rate for all hours 
worked above 40 hours. Put the logic to do the computation 
of pay in a function called computepay() and use the function 
to do the computation. The function should return a value. 
Use 45 hours and a rate of 10.50 per hour to test the program 
(the pay should be 498.75). You should use input to read a
string and float() to convert the string to a number. 
Do not worry about error checking the user input unless you want to - 
you can assume the user types numbers properly. 
Do not name your variable sum or use the sum() function. 
'''

'''
def computepay(h,rate):
    result = h * rate
    if h > 40 :
        result = result + (h -40)* 0.5 * rate
    return result

hrs = input("Enter Hours:")
rph = input("Enter rate per hour:")
try:
    rate = float(rph)
    h = float(hrs)
    result = h * rate
    if h > 40.0 :
        result = result + (h -40)* 0.5 * rate
    print(result)
except :
    print("wrong input")
'''

# 5.1 sum total
'''
count = 0
sum = 0.0
average = 0.0
while True :
    sval = input("Enter a number:, write done to exit ")
    if sval == "done":
        break
    try:
        fval = float(sval)
    except ValueError as err:
        print(err, " invalid input, please retry:")
        continue
    count +=1
    sum = sum + fval
    average = sum/count
    print("count:",count,  "sum:",sum,"average:",average)
'''

'''
5.2 Write a program that repeatedly prompts a user for integer numbers until the user enters 'done'.
 Once 'done' is entered, print out the largest and smallest of the numbers. If the user enters 
 anything other than a valid number catch it with a try/except and put out an appropriate 
 message and ignore the number. Enter 7, 2, bob, 10, and 4 and match the output below. 
Invalid input
Maximum is 10
Minimum is 2
'''

'''
max_nr = None
min_nr = None

while True :
    sval = input("Enter a number:, write done to exit ")
    if sval == "done":
        break
    try:
        ival = int(sval)
    except ValueError as err:
        print("Invalid input")
        continue
    if max_nr is None:
        max_nr = ival
        min_nr = ival
    if ival > max_nr:
        max_nr = ival
    if ival < min_nr:
        min_nr = ival
print("Maximum is",max_nr)
print("Minimum is", min_nr)
'''

# May 27, 2020
# Data Structure

#str = "Hallo World"
#print(type(str))
#print(dir(str))

# Parsing and Extracting
# extract between 21(@) -31 (next space)
# i.e. uct.ac.za
'''
data = "From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008"
spos = data.find('@')
print(spos)
epos = data.find(' ' , spos)
print(epos)
s = data [(spos+1):epos]
print(s)
'''


# x = 'From marquard@uct.ac.za'
# print(x[14:17]) # prints uct

'''
6.5 Write code using find() and string slicing (see section 6.10) to extract 
the number at the end of the line below. Convert the extracted value to a 
floating point number and print it out.
'''
'''
text = "X-DSPAM-Confidence:    0.8475";
spos = text.find('0')
print(float(text[spos:]))
'''
'''
file = open("employee.txt", "+r")
file2 = open("emp1.txt", "w") # create a new file
file.write("\nmalik - customer service\n")
print(file.read())

file = open("./employee.txt", "r")     # r = read, w = write, a = append at end of file. r+ = read & write
if file.readable():
    print("file is readable")
lines = file.readlines()
'''


'''

cwd = os.getcwd()  # Get the current working directory (cwd)
files = os.listdir(cwd)  # Get all the files in that directory
print("Files in %r: %s" % (cwd, files))

count=0
#xfile = open("mbox.txt", "r")
xfile = open("mbox.txt", "r")
for each in xfile:
    count = count+1
    if each.startswith("From:"):
        #print(each[:len(each)-1]) # remove /n character in each line end
        print(each.rstrip())  # remove white spaces, /n is a white space
print("Nr. of line:", count)

'''



