import xml.etree.ElementTree as ET
import sqlite3

conn = sqlite3.connect('trackdb2.sqlite')
cur = conn.cursor()

# Make some fresh tables using executescript()
cur.executescript('''
DROP TABLE IF EXISTS Artist;
DROP TABLE IF EXISTS Album;
DROP TABLE IF EXISTS Track;

CREATE TABLE Artist (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    name    TEXT UNIQUE
);

CREATE TABLE Genre (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    name    TEXT UNIQUE
);

CREATE TABLE Album (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    artist_id  INTEGER,
    title   TEXT UNIQUE
);

CREATE TABLE Track (
    id  INTEGER NOT NULL PRIMARY KEY 
        AUTOINCREMENT UNIQUE,
    title TEXT  UNIQUE,
    album_id  INTEGER,
    genre_id  INTEGER,
    len INTEGER, rating INTEGER, count INTEGER
);
'''

#fname = input('Enter file name: ')
#if ( len(fname) < 1 ) : fname = 'Library.xml'
fname = './tracks/Library.xml'
# <key>Track ID</key><integer>369</integer>
# <key>Name</key><string>Another One Bites The Dust</string>
# <key>Artist</key><string>Queen</string>
def lookup(d, key):
    found = False
    for child in d:
        if found : return child.text
        if child.tag == 'key' and child.text == key :
            found = True
    return None

stuff = ET.parse(fname)
all = stuff.findall('dict/dict/dict')
print('Dict count:', len(all))
for entry in all:
    if ( lookup(entry, 'Track ID') is None ) : continue

    name = lookup(entry, 'Name')
    artist = lookup(entry, 'Artist')
    album = lookup(entry, 'Album')
    genre = lookup(entry, 'Genre')
    count = lookup(entry, 'Play Count')
    rating = lookup(entry, 'Rating')
    length = lookup(entry, 'Total Time')

    if name is None or artist is None or album is None or genre is None:
        continue

    print(name, artist, album, genre, count, rating, length)

    cur.execute('''INSERT OR IGNORE INTO Artist (name) 
        VALUES ( ? )''', ( artist, ) )
    cur.execute('SELECT id FROM Artist WHERE name = ? ', (artist, ))
    artist_id = cur.fetchone()[0]

    cur.execute('''INSERT OR IGNORE INTO Album (title, artist_id) 
        VALUES ( ?, ? )''', ( album, artist_id ) )
    cur.execute('SELECT id FROM Album WHERE title = ? ', (album, ))
    album_id = cur.fetchone()[0]

# add Genre
    cur.execute('''INSERT OR IGNORE INTO Genre (name) 
        VALUES ( ? )''', ( genre, ) )
    cur.execute('SELECT id FROM Genre WHERE name = ? ', (genre, ))
    genre_id = cur.fetchone()[0]

    cur.execute('''INSERT OR REPLACE INTO Track
        (title, album_id,genre_id, len, rating, count) 
        VALUES ( ?, ?, ?, ?, ?, ? )''',
        ( name, album_id, genre_id, length, rating, count ) )

    conn.commit()


'''

<key>369</key>
		<dict>
			<key>Track ID</key><integer>369</integer>
			<key>Name</key><string>Another One Bites The Dust</string>
			<key>Artist</key><string>Queen</string>
			<key>Composer</key><string>John Deacon</string>
			<key>Album</key><string>Greatest Hits</string>
			<key>Genre</key><string>Rock</string>
			<key>Kind</key><string>MPEG audio file</string>
			<key>Size</key><integer>4344295</integer>
			<key>Total Time</key><integer>217103</integer>
			<key>Disc Number</key><integer>1</integer>
			<key>Disc Count</key><integer>1</integer>
			<key>Track Number</key><integer>3</integer>
			<key>Track Count</key><integer>17</integer>
			<key>Year</key><integer>1980</integer>
			<key>Date Modified</key><date>2006-02-14T16:13:02Z</date>
			<key>Date Added</key><date>2006-02-14T16:12:53Z</date>
			<key>Bit Rate</key><integer>160</integer>
			<key>Sample Rate</key><integer>44100</integer>
			<key>Play Count</key><integer>55</integer>
			<key>Play Date</key><integer>3518868190</integer>
			<key>Play Date UTC</key><date>2015-07-04T19:23:10Z</date>
			<key>Skip Count</key><integer>1</integer>
			<key>Skip Date</key><date>2015-10-14T23:31:47Z</date>
			<key>Rating</key><integer>100</integer>
			<key>Album Rating</key><integer>100</integer>
			<key>Album Rating Computed</key><true/>
			<key>Normalization</key><integer>1511</integer>
			<key>Compilation</key><true/>
			<key>Persistent ID</key><string>21130E105F3B8845</string>
			<key>Track Type</key><string>File</string>
			<key>File Type</key><integer>1297106739</integer>
			<key>Location</key><string>file:///Users/csev/Music/iTunes/iTunes%20Music/Compilations/Greatest%20Hits/03%20Another%20One%20Bites%20The%20Dust.mp3</string>
			<key>File Folder Count</key><integer>4</integer>
			<key>Library Folder Count</key><integer>1</integer>
		</dict>

'''