import re

'''
install sq lite browser
https://launchpad.net/~linuxgndu/+archive/ubuntu/sqlitebrowser
sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
sudo apt-get update
sudo apt-get install sqlitebrowser

CRUD Operations

ELECT * FROM
Users

DELETE from Users
WHERE email = 'a@web.de'

UPDATE Users
set name = 'cc'
where email =  'c@web.de'

INSERT INTO Users (name,email)
VALUES ('c', 'c@web.de')

SELECT * FROM Users
where name = 'cc'

SELECT * FROM Users
ORDER by email DESC
'''


str = "zqian@umich.edu"
s = re.findall('@.',str)    #['@u']
print(s)
s = re.findall('@*',str)    #['', '', '', '', '', '@', '', '', '', '', '', '', '', '', '', '']
print(s)
s = re.findall('@+',str)    #['@']
print(s)

s = re.findall('@.+',str)    #['@umich.edu']
print(s)

s = re.findall('@\S.',str)    #
print(s)

#double split
s = "zqian@umich.edu"
words = s.split()

pieces = s.split("@")
print(pieces[1])    #uct.ac.za
pieces = pieces[1].split(".")
print(pieces[0])    #uct.ac.za