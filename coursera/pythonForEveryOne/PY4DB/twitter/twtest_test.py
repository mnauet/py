import urllib.request, urllib.parse, urllib.error
from twurl import augment
import ssl
import json

# https://apps.twitter.com/
# Create App and get the four strings, put them in hidden.py

print('* Calling Twitter...')
url = augment('https://api.twitter.com/1.1/statuses/user_timeline.json',
              {'screen_name': 'drchuck', 'count': '2'})
print('url: ', url)

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

connection = urllib.request.urlopen(url, context=ctx)
d = connection.read()
print('connection.read(): ', d)
#print(data.decode)

data = d.decode()
lst = json.loads(data)
print('length of list' ,len( lst))
print('list: ', lst)
dic1 = lst[0].keys()
dic2 = lst[1].keys()
print('dict1:', dic1)
print('dict2', dic2)

values =  [x.values() for x in lst]
keys =  [x.keys() for x in lst]
print('keys: ', keys)
print('values: ', values)

print('#####################################')
keys = []
vals = []
for l in lst:
    val = []
    for k,v in l.items():
        keys.append(k)
        val.append(v)
        print('key:', k, 'value:', v)
    vals.append(val)
print(list(dict.fromkeys(keys)))
for v in vals:
    print(v)




print ('======================================')
print('print headers')
headers = dict(connection.getheaders())
print(headers)
for h in headers:
    print('key: ',h, 'value: ', headers[h])

print( '######################')

print('x-rate-limit-limit: ', headers['x-rate-limit-limit'])
print('rate limit remaining: ', headers['x-rate-limit-remaining'])