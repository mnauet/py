'''
CREATE TABLE User(
    id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    name    TEXT,
    email   TEXT
)
CREATE TABLE Course(

    id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    title   TEXT
)
CREATE TABLE MEMBER (
    user_id INTEGER,
    course_id  INTEGER,
    role    INTEGER,
    PRIMARY KEY (user_id, course_id)
)

# insert users and courses
INSERT INTO User (name,email) VALUES ('Jane', 'jane@tsugi.org');
INSERT INTO User (name,email) VALUES ('Ed', 'ed@tsugi.org');
INSERT INTO User (name,email) VALUES ('Sue', 'Sue@tsugi.org');


INSERT INTO Course (title) VALUES ('PYTHON');
INSERT INTO Course (title) VALUES ('SQL');
INSERT INTO Course (title) VALUES ('PHP');


INSERT INTO Member (user_id,course_id,role) VALUES (1,1,0);
INSERT INTO Member (user_id,course_id,role) VALUES (2,1,0);
INSERT INTO Member (user_id,course_id,role) VALUES (3,1,0);

INSERT INTO Member (user_id,course_id,role) VALUES (1,2,0);
INSERT INTO Member (user_id,course_id,role) VALUES (2,2,1);

INSERT INTO Member (user_id,course_id,role) VALUES (2,3,1);
INSERT INTO Member (user_id,course_id,role) VALUES (3,3,0);


#Query
SELECT User.name, MEMBER.role, Course.title
FROM User
join MEMBER join Course
on user.id = MEMBER.user_id and  Course.id=MEMBER.course_id
ORDER BY Course.title, MEMBER.role DESC , user.name

'''
