import sqlite3

conn = sqlite3.connect("emaildb.sqlite")
cur = conn.cursor()

cur.execute('DROP TABLE if EXISTS Counts ')
cur.execute('CREATE TABLE Counts (email TEXT, Count INTEGER)')

fh = open('./data/mbox-short.txt')
for line in fh:
    if not line.startswith('From: '): continue
    pieces = line.split()
    email = pieces[1]
    #print((email,1))
    cur.execute('SELECT count FROM Counts WHERE email = ? ', (email,))
    row = cur.fetchone()
    #print(row)
    if row is None :
        cur.execute('INSERT INTO Counts (email,count) VALUES (?,1)', (email,))
    else:
        cur.execute('UPDATE  Counts SET Count = Count + 1 WHERE email = ?', (email,))
    conn.commit()
sqlstr = 'SELECT email, count from Counts ORDER BY Count DESC LIMIT 10'
for row in cur.execute(sqlstr):
    print(row[0], row[1])
cur.close()