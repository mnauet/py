import sqlite3
conn = sqlite3.connect("emaildb.sqlite")
cur = conn.cursor()

cur.execute('DROP TABLE if EXISTS Counts ')
cur.execute('CREATE TABLE Counts (org TEXT, Count INTEGER)')

fh = open('./data/mbox.txt')
for line in fh:
    if not line.startswith('From: '): continue
    pieces = line.split()
    email = pieces[1]

    pieces = email.split("@")
    #print(pieces[1])  # uct.ac.za
    #pieces = pieces[1].split(".")
    org = pieces[1]  # uct.ac.za

    #print((email,1))
    cur.execute('SELECT count FROM Counts WHERE org = ? ', (org,))
    row = cur.fetchone()
    #print(row)
    if row is None :
        cur.execute('INSERT INTO Counts (org,count) VALUES (?,1)', (org,))
    else:
        cur.execute('UPDATE  Counts SET Count = Count + 1 WHERE org = ?', (org,))
conn.commit()

sqlstr = 'SELECT org, count from Counts ORDER BY Count DESC LIMIT 10'
for row in cur.execute(sqlstr):
    print(row[0], row[1])
cur.close()

