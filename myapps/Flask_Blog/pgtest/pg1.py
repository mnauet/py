from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/flaskdb'
db = SQLAlchemy(app)

migrate = Migrate (app,db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(120), unique=True)

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<User %r>' % self.username

class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    pets = db.relationship('Pet', backref='owner', lazy='dynamic')

class Pet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    new3 = db.Column(db.String(20))
    new4 = db.Column(db.String(20))
    new5 = db.Column(db.String(20))
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))


@app.route('/')
def index():
    return "<h1 style='color: red'>hello flask </h1>"

if __name__ == "__main__":
    #app.run(debug=True)
    manager.run()


'''

https://www.youtube.com/watch?v=BAOfjPuVby0&t=47s

$ export FLASK_APP=pg1.py
$ echo $FLASK_APP
$ python pg1.py db init
$ python pg1.py db migrate
$ python pg1.py db upgrade
$ flask db --help
$ flask db init
$ flask db migrate -m "Initial migration."

sudo apt install python3-dev libpq-dev
pip install psycopg2
$ pip install flask_migrate
pip install Flask-Script





file_template = %%(year)d-%%(month).2d-%%(day).2d-%%(hour).2d-%%(minute).2d-%%(second).2d _%%(rev)s_%%(slug)s
----------------------------------------------------------------------------------
$ python
>>> from pg1 import db
>>> db.create_all()

$ sudo -u postgres psql test
\q exit
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

-

INSERT INTO public.User (id, username, email) VALUES (21, 'malik', 'malik@gmail.com');
INSERT INTO public.User (id, username, email) VALUES (22, 'naveed', 'naveed@gmail.com');
INSERT INTO public.User (id, username, email) VALUES (23, 'akram', 'akram@gmail.com');


INSERT INTO public.person (id, name) VALUES (1, 'malik');
INSERT INTO public.person (id, name) VALUES (2, 'naveed');
INSERT INTO public.person (id, name) VALUES (3, 'akram');


INSERT INTO public.pet (id, name) VALUES (1, 'sundari');
INSERT INTO public.pet (id, name) VALUES (2, 'thunder');
INSERT INTO public.pet (id, name) VALUES (3, 'silver');
flaskdb=# SELECT * FROM public.user;

flaskdb=# Update public.pet set owner_id = 1 where id=2;

flaskdb=# select * from person join pet on person.id = pet.owner_id;
 id | name  | id |  name   | new | owner_id 
----+-------+----+---------+-----+----------
  1 | malik |  2 | thunder |     |        1
(1 row)

flaskdb=# select * from person join pet on person.id = pet.owner_id;
 id |  name  | id |  name   | new | owner_id 
----+--------+----+---------+-----+----------
  1 | malik  |  2 | thunder |     |        1
  2 | naveed |  3 | silver  |     |        2
(2 rows)



$ pip install flask_migrate
Successfully installed Mako-1.1.3 alembic-1.4.3 flask-migrate-2.5.3 python-dateutil-2.8.1 python-editor-1.0.4
$ pip install Flask-Script
Successfully installed Flask-Script-2.0.6


$ python pg1.py db init
  Creating directory /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations ...  done
  Creating directory /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/versions ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/alembic.ini ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/script.py.mako ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/env.py ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/README ...  done
  Please edit configuration/connection/logging settings in '/home/malik/repos/py/coreySchafer_flask/Flask_Blog/pgtest/migrations/alembic.ini' before proceeding.

$ python pg1.py db migrate

[alembic]
# template used to generate migration files
# file_template = %%(rev)s_%%(slug)s

# file_template = %%(year)d-%%(month).2d-%%(day).2d_%%(rev)s_%%(slug)s
---------------------------------------------------------------
# drop new , add new2 and migrate and update db.

flaskdb=# select * from public.pet;
 id |  name   | new | owner_id 
----+---------+-----+----------al
  1 | sundari |     |         
  2 | thunder |     |        1
  3 | silver  |     |        2
(3 rows)

flaskdb=# select * from public.pet;
 id |  name   | owner_id | new2 
----+---------+----------+------
  1 | sundari |          | 
  2 | thunder |        1 | 
  3 | silver  |        2 | 
(3 rows)
---------------------------------------------------------------
flaskdb-# select * from alembic_version;

flaskdb=# delete from alembic_version where version_num='4d3a5a7f90e0';
DELETE 1

'''
