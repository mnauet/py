"""empty message

Revision ID: 49b9c1b84e67
Revises: 49df79f04121
Create Date: 2020-10-23 07:56:11.201904

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '49b9c1b84e67'
down_revision = '49df79f04121'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('pet', sa.Column('new5', sa.String(length=20), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('pet', 'new5')
    # ### end Alembic commands ###
