from flask import Flask
from bookreadings.config import Config

def bookreadings_create_app(config_class=Config):
    bookreadings_app = Flask(__name__)

    from bookreadings.myapp import simple_page
    bookreadings_app.register_blueprint(simple_page, url_prefix="/bookreadings")

# http://127.0.0.1:5000/bookreadings/
# http://127.0.0.1:5000/static/test.txt
    return bookreadings_app