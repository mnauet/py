from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from farmdiary import db, login_manager       # __main__
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    first_name = db.Column(db.String(20), unique=True, nullable=True)
    last_name = db.Column(db.String(20), unique=True, nullable=True)
    email = db.Column(db.String(120), nullable=False)
    #phone1
    #phone2
    #address
    #email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)
    animals = db.relationship('Animal', backref='owner', lazy=True)
    user_spender = db.relationship('Expense', foreign_keys='Expense.spender_id', backref='spender', lazy=True)
    user_submitter = db.relationship('Expense', foreign_keys='Expense.submitter_id', backref='submitter', lazy=True)

    exp_creater = db.relationship('ExpenseCategory', backref='creator', lazy=True)


    # https://docs.sqlalchemy.org/en/14/orm/join_conditions.html#handling-multiple-join-paths
    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    #   >>> post= Post.query.first()
    #   >>> post
    #   Post('Blog_1', '2020-07-27 17:32:51.222995')
    #   >>> post.author
    #   User('malik', 'm@blog.com', 'default.jpg')

    def __repr__(self):
        return f"Post('{self.title}', '{self.date_posted}')"

class Farm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)

class FarmType(db.Model):
    __tablename__ = 'farmtype'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)


class Breed(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(100), nullable=False, default='Makki Chini') #
    description = db.Column(db.Text, nullable=True)
    animals = db.relationship("Animal", backref = db.backref("breed", lazy="select"))

class Mandi(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(100), nullable=False, default='Haroon Abad Madrissa Mandi')
    description = db.Column(db.Text, nullable=True)
    animals = db.relationship("Animal", backref = db.backref("mandi", lazy="select"))

class Symptom(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(200), nullable=False, default='Fever')
    description = db.Column(db.Text, nullable=True)

class Vaccinetypes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, default='None')
    name_urdu = db.Column(db.String(100), nullable=False, default='None')

    dose = db.Column(db.Numeric(4,1), nullable=True) # ml
    dose_week = db.Column(db.Integer, nullable=True) # Week - 1 (first week), 2, 3, 4, 5
    dose_month = db.Column(db.Integer, nullable=True) # Month  1 - 12 (January -December)
    dose_place = db.Column(db.String(100), nullable=False, default='Under skin')
    immunity_duration = db.Column(db.Integer, nullable=True) # in months 1-12 months
    description = db.Column(db.Text, nullable=True)
    vaccines = db.relationship("Vaccine", backref=db.backref("vaccinetypes", lazy="joined"), lazy="select")

class Vaccine(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    vaccinetypes_id = db.Column(db.Integer, db.ForeignKey('vaccinetypes.id'), nullable=False)
    animal_id = db.Column(db.Integer, db.ForeignKey('animal.id'), nullable=False)
    vaccine_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow) # when vaccined
    #vaccine_person =

class Doctor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, default='None')
    description = db.Column(db.Text, nullable=True)

class Animalseller(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, default='None')
    description = db.Column(db.Text, nullable=True)
    animals = db.relationship("Animal", backref = db.backref("animalseller", lazy="select"))

class Animal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag_id = db.Column(db.Integer, nullable=False)    # registration nr.
    sex = db.Column(db.Integer, nullable=False, default='1') # 1= Female, 2= Male
    breed_id = db.Column(db.Integer, db.ForeignKey('breed.id'), nullable=True)
    color = db.Column(db.String(100), nullable=True, default='black')

    purchased_batch = db.Column(db.Integer, nullable=True, default=9999)
    purchased_date = db.Column(db.Date, nullable=True, default=datetime.utcnow().date())
    purchased_from = db.Column(db.Integer, db.ForeignKey('mandi.id'), nullable=True)
    purchased_price = db.Column(db.Integer, nullable=True, default='9999')
    current_price = db.Column(db.Integer, nullable=True, default='9999') # based on some calculations
    target_sell_price = db.Column(db.Integer, nullable=True, default='9999') # at what to sell
    purchased_from_person = db.Column(db.Integer, db.ForeignKey('animalseller.id'), nullable=True)    # old owner
    PO_Nr = db.Column(db.String(100), nullable=True, default='9999')


    #owner_id = db.Column(db.Integer, nullable=True)    # Investor/Owner
    farm_id = db.Column(db.Integer, db.ForeignKey('farm.id'), nullable=True)
    pen = db.Column(db.Integer, nullable=True)
    herd = db.Column(db.Integer, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)    # new owner
    start_weight = db.Column(db.Numeric(8,3), nullable=True) # birth or at purchase
    current_weight = db.Column(db.Numeric(8,3), nullable=True)

    buck_id = db.Column(db.Integer, nullable=True)  # Male
    doe_id = db.Column(db.Integer, nullable=True)  # Female
    birth_date = db.Column(db.Date, nullable=True, default=datetime.utcnow().date())
    birth_type = db.Column(db.Integer, nullable=True, default=1) # 1 single 2= double, 3= three, 4 =quad

    description = db.Column(db.Text, nullable=True) #

    image_file = db.Column(db.String(20), nullable=True, default='default.jpg')
    # animals = db.relationship('Animal', backref='owner', lazy=True)
    vaccines = db.relationship('Vaccine', backref='patient', lazy=True)
    weights = db.relationship('Weight', backref='growth', lazy=True)


''' 
        def __repr__(self):
        return f"Animal('{self.id}', '{self.description}')"
'''

class ExpenseCategory(db.Model):
    __tablename__ = 'expensecategory'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    created_by =  db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    created_on = db.Column(db.Date, nullable=True, default=datetime.utcnow().date())
    expenses = db.relationship("Expense", backref = db.backref("categories", lazy="select"))


class Expense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    spender_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)    # who entered data
    submitter_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)    # who entered data
    name = db.Column(db.String(100), nullable=True, default='9999')
    amount = db.Column(db.Numeric(10,3), nullable=True)
    expensecategory_id = db.Column(db.Integer, db.ForeignKey('expensecategory.id'), nullable=True)
    created_on = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    spent_on = db.Column(db.Date, nullable=True, default=datetime.utcnow().date())
    description = db.Column(db.Text, nullable=True)


class Weight(db.Model): # Animal Weight
    id = db.Column(db.Integer, primary_key=True)
    animal_id = db.Column(db.Integer, db.ForeignKey('animal.id'), nullable=False)    # animal
    weight = db.Column(db.Numeric(7,3), nullable=False) # animal weight
    created_on = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    submitter_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)    # who entered data
'''

    user_spender = db.relationship('Expense', foreign_keys=[spender_id], backref='spender', lazy=True)
    user_submitter = db.relationship('Expense', foreign_keys=[submitter_id], backref='submitter', lazy=True)'''

'''
# Movie
#    director_id = db.Column(db.Integer, db.ForeignKey("director.id"))
#    actors = db.relationship("Actor", secondary=actors, backref="movies", lazy="select")


# Director
    movies = db.relationship(
        "Movie", backref=db.backref("director", lazy="joined"), lazy="select"
    )
    guild = db.relationship(
        "GuildMembership", backref="director", lazy="select", uselist=False
    )

# actor
    guild = db.relationship(
        "GuildMembership", backref="actor", lazy="select", uselist=False
    )    
 
 
# GuildMembership
    direcotr_id = db.Column(db.Integer, db.ForeignKey("director.id"))
    actor_id = db.Column(db.Integer, db.ForeignKey("actor.id"))
    
actors = db.Table(
    "actors",
    db.Column("actor_id", db.Integer, db.ForeignKey("actor.id")),
    db.Column("movie_id", db.Integer, db.ForeignKey("movie.id")),
)
'''


'''



'''

