from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt     # p6/4:20
from flask_login import LoginManager    #p5/20:00
from flask_mail import Mail     #25.28
from farmdiary.config import Config
from flask_script import Manager    # 2020 10 27
from flask_migrate import Migrate, MigrateCommand   # 2020 10 27

db = SQLAlchemy()
migrate = Migrate()   # 2020 10 27

bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
mail = Mail()


def farmdiary_create_app(config_class=Config):
    """Application-factory pattern"""
    # app = Flask('flaskapp', static_url_path='/public', static_folder='public')
    # app = Flask(__name__, static_url_path='/some/static/path')
    app = Flask(__name__, static_folder='/home/malik/static',  static_url_path='',
                template_folder='./templates')
    #app = Flask(__name__)
    # app = Flask(__name__,static_folder='./flask_news/static',template_folder='./flask_news/templates')
    # app = Flask(__name__, static_folder=os.path.join(os.getcwd(),'web','static'), static_url_path='')
    app.config.from_object(Config)

    db.init_app(app)
    migrate.init_app(app,db)    # 2020 10 27

    manager = Manager(app)  # 2020 10 27  ??????????????????????????????????
    manager.add_command('db', MigrateCommand)    # 2020 10 27

    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    from farmdiary.users.routes import users
    from farmdiary.posts.routes import posts
    from farmdiary.main.routes import main
    from farmdiary.errors.handlers import errors

    from farmdiary.animals.routes import animals

    from farmdiary.cmd_line import  init_db_cmd

    from farmdiary.finance.routes import  finance

    app.register_blueprint(users)
    app.register_blueprint(posts)
    app.register_blueprint(main)
    app.register_blueprint(errors)
    app.register_blueprint(animals)

    app.register_blueprint(init_db_cmd)

    app.register_blueprint(finance)

    return app


'''
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ flask db init
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ flask db migrate -m "2020 10 30"
$ flask db upgrade

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ sudo -u postgres psql farmdiary
farmdiary=# select * from alembic_version;

flaskdb=# delete from alembic_version where version_num='4d3a5a7f90e0';


(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ flask db init
/home/malik/repos/py/coreySchafer_flask/venv/lib/python3.6/site-packages/flask_sqlalchemy/__init__.py:834: FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future.  Set it to True or False to suppress this warning.
  'SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and '
  Creating directory /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations ...  done
  Creating directory /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/versions ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/alembic.ini ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/script.py.mako ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/env.py ...  done
  Generating /home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/README ...  done
  Please edit configuration/connection/logging settings in '/home/malik/repos/py/coreySchafer_flask/Flask_Blog/migrations/alembic.ini' before proceeding.
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ flask db --help
Usage: flask db [OPTIONS] COMMAND [ARGS]...

  Perform database migrations.

Options:
  --help  Show this message and exit.

Commands:
  branches   Show current branch points
  current    Display the current revision for each database.
  downgrade  Revert to a previous version
  edit       Edit a revision file
  heads      Show current available heads in the script directory
  history    List changeset scripts in chronological order.
  init       Creates a new migration repository.
  merge      Merge two revisions together, creating a new revision file
  migrate    Autogenerate a new revision file (Alias for 'revision...
  revision   Create a new revision file.
  show       Show the revision denoted by the given symbol.
  stamp      'stamp' the revision table with the given revision; don't run...
  upgrade    Upgrade to a later version


'''