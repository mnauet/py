from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from farmdiary import db
from farmdiary.models import Post, User
from farmdiary.posts.forms import PostForm
import json

posts = Blueprint('posts', __name__)



@posts.route('/post/new', methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        #form.title.data = cu
        #form.content.data =
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')

@posts.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)

@posts.route('/post/<int:post_id>/update', methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your message has been updated', 'success')
        return redirect(url_for('posts.post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title= 'Update Post',
                           form=form, legend='Update Post')

#  http://127.0.0.1:5000/post/load_json
@posts.route('/post/<int:post_id>/delete', methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user :
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Post deleted successfully!', 'success')
    return redirect(url_for('main.home'))

@posts.route('/post/load_json', methods=['GET', 'POST'])
def load_json():

# step 1 http://127.0.0.1:5000/post/load_json
# step 2 press button on post page, data gets populated.

    form = PostForm()
    fname = 'farmdiary/posts/posts.json'
    str_data = open(fname).read()
    json_data = json.loads(str_data)

    if form.validate_on_submit():
        for item in json_data:
            form.title.data = item["title"]
            form.content.data = item["content"]
            author = current_user
            post = Post(title=form.title.data, content=form.content.data, author=current_user)
            db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        form.title.data = 'post-data_1'
        form.content.data = 'content-data-1'
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title='New Post', form=form, legend='New Post')

'''
    if form.validate_on_submit():
        for item in json_data:
            form.title.data = item["title"]
            form.content.data =item["content"]
            author = current_user
            post = Post(title=form.title.data, content=form.content.data, author=current_user)
            db.session.add(post)
        db.session.commit()
        flash('Your message has been created', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        form.title.data = 'post-data_1'
        form.content.data ='content-data-1'
        flash('Your message has been created', 'success')
    return render_template('create_post.html', title= 'New Post', form=form, legend='New Post')
'''

'''
@posts.cli.command("initdb")
def post_from_json():
    """Drops and Creates fresh database"""

    #db.drop_all()
    #db.create_all()
    fname = 'farmdiary/posts/posts.json'
    str_data = open(fname).read()
    json_data = json.loads(str_data)
    for item in json_data:
        print(item["title"])
    user = User.query.filter_by(email='malik.akram@gmail.com').first()
    print(' Hallo User')
    print('user.username')
    post = Post()
    #user = User.query.filter_by(email='malik.akram@gmail.com').first()
    for item in json_data:
        post.title = item["title"]
        post.content = item["content"]
        #post.author = current_user
        db.session.add(post)
        db.session.commit()
    print("Posts - initdb command")
'''


