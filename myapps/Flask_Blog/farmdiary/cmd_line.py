from flask import  Blueprint
from datetime import datetime
from farmdiary import db
from farmdiary.models import (User, Post,Animal, User, Vaccine, Vaccinetypes, Breed,
                              Doctor,Farm, FarmType,Mandi,Symptom,
                              ExpenseCategory, Expense)

init_db_cmd = Blueprint('init_db_cmd', __name__)


# $ flask init_db_cmd  db_fill
@init_db_cmd.cli.command("db_fill")
def db_fill():
    bootstrap_db()
    addUser()
    #doVaccination()
    addFarm()
    addFarmtype()
    addMandi()
    addSymptom()
    addDoctor()
    addBreed()
    addVaccinetype()
    addAnimalKhurram()
    addAnimalNadeem()
    addAnimalNaeem()
    addAnimalSumaira()
    addAnimalHuzaifa()
    addAnimalAhmad()
    addAnimalMajeed()
    addAnimalSana()
    addVaccination()
    addExpensecategory()
    addExpense()
    addPosts()
    print("db filled")

#@init_db_cmd.cli.command("bootstrap")
def bootstrap_db():
    """Drops and Creates fresh database"""
    db.drop_all()
    db.create_all()
    print("tables droped and re created")

def addPosts():
    user = User.query.filter_by(username='naeemakram').first_or_404()
    post1 = Post(title='Akram Agri & Goat Farm',
                date_posted=datetime.utcnow(),
                content='https://www.facebook.com/Apniqurbani/',
                 author=user)
    post2 = Post(title='Commercial Goat Farming Training Workshop',
                date_posted=datetime.utcnow(),
                content='''Malik Naveed Akram attending the training session of Commercial Goat & Sheep Farming. Very informative training session.
کمرشل بکری اینڈ بھیڑ فارمنگ کے ٹریننگ سیشن میں شریک ملک نوید اکرم بہت معلوماتی تربیتی سیشن۔
https://www.youtube.com/watch?v=Xx896kUZLWQ
''',
                 author=user)

    db.session.add(post1)
    db.session.add(post2)
    db.session.commit()
    print("Blog Post data posted added")

    '''
        id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    
    
    Commercial Goat Farming Training Workshop
Malik Naveed Akram attending the training session of Commercial Goat & Sheep Farming. Very informative training session.
کمرشل بکری اینڈ بھیڑ فارمنگ کے ٹریننگ سیشن میں شریک ملک نوید اکرم بہت معلوماتی تربیتی سیشن۔
https://www.youtube.com/watch?v=Xx896kUZLWQ

Akram Agri & Goat Farm
https://www.facebook.com/Apniqurbani/
    '''
#@init_db_cmd.cli.command("addUser")
def addUser():
    """Drops and Creates fresh database - adds users"""
    u22 =  User(username='mna',
                email='malik.akram@gmx.de',
                password='$2b$12$VN2zvKzv4QJZ5qHxa4StHe5nqDbvDhsLv767Wq/qOIeGLUI5Da6D2',
               image_file='42dc_naveed1.jpg')

    u1 = User(username='malikakram',
                email='malik.akram@gmail.com',
                password='$2b$12$7QNWwsShWnBrZh19BPQVKuChjLUF/9x81DQoKZ.SOB11cln48GxIW',
               image_file='42dc_naveed1.jpg')

    u2 =  User(username='khurramakram',
                email='khurrammalick123@gmail.com',
                password='$2b$12$cwsbR9Opb5powZL4O6rEmOoSLmfrIYm/HmUl8tjF8zC7cDnuSnr8.',
               image_file='e969_khurram.jpg')

    u3 =  User(username='nadeemakram',
                email='mnamalik@yahoo.com',
                password='$2b$12$ZnzCB6HKsKEB5UaCZIwwA.HOf435.fn3w5P/FO6GsUZ0lJR791e42')

    u4 =  User(username='naeemakram',
                email='mnakram81@gmail.com',
                password='$2b$12$ZVPdH6moD.UbVVxkRDqx4.GWmSVcjJXyhODL2DPqb3VI0CEcPZC6S',
               image_file='7068_naeem.jpg')

    u5 =  User(username='sumairaakram',
                email='sumairamudasar@hotmail.com',
                password='$2b$12$vgBctty1hI4apOUKRMRUdujbOYmcsqaSfa9BNGuViyP2wwNEhbOQy')
    u6 =  User(username='huzaifaakram',
                email='mhuzaifaakram2@gmail.com',
                password='$2b$12$4u2MXrllnnZ9TidxVNJ9zOXHGBZWDdzbBuz4UFpVul50zMj1gfJ5m')

    u7 =  User(username='ahmadakram',
                email='ahmadakramtcs@gmail.com',
                password='$2b$12$pahWF7aYn1HQ4Ho9K71.huf5uAvi440s/szIuj.A/OnHZB6riRODK')

    u8 =  User(username='sananayab',
                email='emaan_40@yahoo.com',
                password='$2b$12$VN2zvKzv4QJZ5qHxa4StHe5nqDbvDhsLv767Wq/qOIeGLUI5Da6D2')

    u9 =  User(username='malikmajeed',
                email='mnauet@yahoo.com',
                password='$2b$12$fX5N6yRLXgyUD5YFpNQuNex4tPePnmEuxbdhGqoSL2tEsd7jy63KG')

    db.session.add(u1)
    db.session.add(u2)
    db.session.add(u3)
    db.session.add(u4)
    db.session.add(u5)
    db.session.add(u6)
    db.session.add(u7)
    db.session.add(u8)
    db.session.add(u9)


    db.session.commit()
    print("Users added")

##############################################################################
##############################################################################

##############################################################################
def addFarm():
    farm1 = Farm(title='Akram Agri & Goat Farm')
    db.session.add(farm1)

    db.session.commit()
    print("Farm - added")
##############################################################
def addFarmtype():
    farmtype1 = FarmType(title='Goat Far,')
    db.session.add(farmtype1)

    db.session.commit()
    print("Farm Type - added")
##############################################################
def addMandi():
    mandi1 = Mandi(title='Madrissa Mandi')
    mandi2 = Mandi(title='Lutton Mandi')
    mandi3 = Mandi(title='Hasil Pur Mandi')
    mandi4 = Mandi(title='Khushab Mandi')
    mandi5 = Mandi(title='Purchased from Local Farms')
    db.session.add(mandi1)
    db.session.add(mandi2)
    db.session.add(mandi3)
    db.session.add(mandi4)
    db.session.add(mandi5)

    db.session.commit()
    print("Mandi - added")
##############################################################
def addSymptom():
    sym1 = Symptom(title='Fever')
    sym2 = Symptom(title='Not taking feed')
    sym3 = Symptom(title='fast breathing')
    db.session.add(sym1)
    db.session.add(sym2)
    db.session.add(sym3)

    db.session.commit()
    print("Symptoms - added")

##############################################################
def addDoctor():
    doc1 = Doctor(name='doctor 1')
    doc2 = Doctor(name='doctor 2')
    doc3 = Doctor(name='doctor 3')
    db.session.add(doc1)
    db.session.add(doc2)
    db.session.add(doc3)

    db.session.commit()
    print("Doctor - added")
##############################################################
def addBreed():
    br1 = Breed(title='Makki Chini',description='Makki Chini')
    br2 = Breed(title='Beetal',description='Beetal')
    br3 = Breed(title='Rajan Puri',description='Rajan Puri')
    br4 = Breed(title='Desi',description='Desi')
    br5 = Breed(title='Tedi',description='Tedi')
    br6 = Breed(title='Dogli',description='Dogli')
    br7 = Breed(title='Ablak',description='Ablak')
    br8 = Breed(title='Nachi',description='Nachi')
    br9 = Breed(title='Nagri',description='Nagri')
    br10 = Breed(title='Faisalabi Beetal',description='Faisalabi Beetal')
    br11 = Breed(title='Sialkoti Beetal',description='Sialkoti Beetal')
    db.session.add(br1)
    db.session.add(br2)
    db.session.add(br3)
    db.session.add(br4)
    db.session.add(br5)
    db.session.add(br6)
    db.session.add(br7)
    db.session.add(br8)
    db.session.add(br9)
    db.session.add(br10)
    db.session.add(br11)

    db.session.commit()
    print("Breed - added")
#####################################################################

'''
def addVaccinetypes():
    print("Animal added - Vaccinetypes")
class Vaccinetypes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, default='None')
    description = db.Column(db.Text, nullable=True)
    dose = db.Column(db.Text, nullable=True)
    dose_week = db.Column(db.Integer, nullable=True) # Week - 1 (first week), 2, 3, 4, 5
    dose_month = db.Column(db.Integer, nullable=True) # Month  1 - 12 (January -December)
    immunity_duration = db.Column(db.Integer, nullable=True) # in months 1-12 months
    vaccines = db.relationship("Vaccine", backref=db.backref("vaccinetypes", lazy="joined"), lazy="select")
'''

def addVaccinetype():
    vt1=Vaccinetypes(name='Entrerotoxemia',name_urdu='DAST - Antaryoo ka zahr',dose='3.0',
                     dose_week=2,dose_month=1,immunity_duration=6,
        description=' - twice a year, 1st dose: jan 2nd week, 2nd dose:  June last week ')
    vt2 = Vaccinetypes(name='Entrerotoxemia', name_urdu='DAST - Antaryoo ka zahr', dose='3.0', dose_week=2,
                       dose_month=7, immunity_duration=6,
                       description=' - twice a year, 1st dose: jan 2nd week, 2nd dose:  july 2nd week ')
    vt3 = Vaccinetypes(name='Entrerotoxemia',name_urdu='DAST - Antaryoo ka zahr', dose='3.0', dose_week=2,
                       dose_month=12, immunity_duration=6,
                       description=' - three x a year, 1st dose: jan 2nd week, 2nd dose:  july 2nd week ')
    vt4=Vaccinetypes(name='Anthrax',name_urdu='Sitt',dose='0.5',dose_week=2,dose_month=2,immunity_duration=12,
        description='SITT - once a year dose: Feb 2nd week ')


    vt5=Vaccinetypes(name='Anthrax',name_urdu='Sitt',dose='1.0',dose_week=2,dose_month=2,immunity_duration=12,
        description='Goly Yast - once a year dose: Feb 2nd week ')


    vt6=Vaccinetypes(
        name='Goat Pox',name_urdu='Cheechak',dose='1.0',dose_week=3,dose_month=3,immunity_duration=9,
        description='Cheechak  - March 3rd week , sep last week, immunity duration 9 months')
    vt7=Vaccinetypes(
        name='Goat Pox',name_urdu='Cheechak',dose='1.0',dose_week=4,dose_month=9,immunity_duration=9,
        description='Cheechak  - March 3rd week , sep last week, immunity duration 9 months')

    vt8=Vaccinetypes(name='PPR',name_urdu='Kata',dose='1.0',dose_week=1,dose_month=4,immunity_duration=0,
        description=' april first week, oct first week')
    vt9=Vaccinetypes(name='PPR',name_urdu='Kata',dose='1.0',dose_week=1,dose_month=10,immunity_duration=0,
        description=' ')

    vt10=Vaccinetypes(name='Pleuropneumonia',name_urdu='Phaproo ki sooshish',dose='1.0',dose_week=1,dose_month=5,immunity_duration=12,
        description=' ')
    vt11=Vaccinetypes(name='Pleuropneumonia',name_urdu='Phaproo ki sooshish',dose='1.0',dose_week=1,dose_month=5,immunity_duration=12,
        description=' ')
    vt12=Vaccinetypes(name='Pleuropneumonia',name_urdu='Phaproo ki sooshish',dose='1.0',dose_week=1,dose_month=11,immunity_duration=12,
        description=' ')

    db.session.add(vt1)
    db.session.add(vt2)
    db.session.add(vt3)
    db.session.add(vt4)
    db.session.add(vt5)
    db.session.add(vt6)
    db.session.add(vt7)
    db.session.add(vt8)
    db.session.add(vt9)
    db.session.add(vt10)
    db.session.add(vt11)
    db.session.add(vt12)

    db.session.commit()
    print("Vaccine types- added")

#####################################################################
def addAnimalKhurram():
    # khurramakram
    user = User.query.filter_by(username='khurramakram').first_or_404()
    a1 = Animal(tag_id=1,owner=user,sex=1,purchased_price=14250)
    a2 = Animal(tag_id=2,owner=user,sex=1,purchased_price=14250)
    a6 = Animal(tag_id=6,owner=user,sex=1,purchased_price=11500)
    a7 = Animal(tag_id=7,owner=user,sex=1,purchased_price=11500)
    a9 = Animal(tag_id=9,owner=user,sex=1,purchased_price=16250)
    a22 = Animal(tag_id=22,owner=user,sex=1,purchased_price=17500)
    a23 = Animal(tag_id=23,owner=user,sex=1,purchased_price=17500)
    a24 = Animal(tag_id=24,owner=user,sex=1,purchased_price=12500)
    a25 = Animal(tag_id=25,owner=user,sex=1,purchased_price=12500)
    a26 = Animal(tag_id=26,owner=user,sex=2,purchased_price=47000)
    a27 = Animal(tag_id=27,owner=user,sex=1,purchased_price=22000)
    a28 = Animal(tag_id=28,owner=user,sex=1,purchased_price=17500)
    a29 = Animal(tag_id=29,owner=user,sex=1,purchased_price=17500)
    a30 = Animal(tag_id=30,owner=user,sex=1,purchased_price=35000)
    a31 = Animal(tag_id=31,owner=user,sex=2,purchased_price=25000)
    a32 = Animal(tag_id=32,owner=user,sex=1,purchased_price=25000)
    a33 = Animal(tag_id=33,owner=user,sex=1,purchased_price=25000)
    a34 = Animal(tag_id=34,owner=user,sex=1,purchased_price=25000)
    a35 = Animal(tag_id=35,owner=user,sex=1,purchased_price=25000)
    a36 = Animal(tag_id=36,owner=user,sex=1,purchased_price=25000)
    a38 = Animal(tag_id=38,owner=user,sex=1,purchased_price=25000)
    a39 = Animal(tag_id=39,owner=user,sex=1,purchased_price=25000)
    a41 = Animal(tag_id=41,owner=user,sex=1,purchased_price=25000)
    a42 = Animal(tag_id=42,owner=user,sex=1,purchased_price=25000)

    db.session.add(a1)
    db.session.add(a2)
    db.session.add(a6)
    db.session.add(a7)
    db.session.add(a9)
    db.session.add(a2)
    db.session.add(a22)
    db.session.add(a23)
    db.session.add(a24)
    db.session.add(a25)
    db.session.add(a26)
    db.session.add(a27)
    db.session.add(a28)
    db.session.add(a29)
    db.session.add(a30)
    db.session.add(a31)
    db.session.add(a32)
    db.session.add(a33)
    db.session.add(a34)
    db.session.add(a35)
    db.session.add(a36)
    db.session.add(a38)
    db.session.add(a39)
    db.session.add(a41)
    db.session.add(a42)

    # 2020 11 01 - kids
    '''
    6	61	36	31	Male	Khurram			15/10/2020
    11	66	30	31	Male	Khurram			21/10/2020
    12	67	37	31	Male	Khurram			21/10/2020
    13	68	37	31	Female	Khurram			23/10/2020
    19	74	39	31	Male	Khurram			24/10/2020
    20	75	39	31	Female	Khurram			24/10/2020
    23	78	32	31	Male	Khurram			30/10/2020
    24	79	34	31	Male	Khurram			30/10/2020
    
    '''
    # farmdiary=# select id, tag_id, user_id, sex, buck_id, doe_id, birth_date, birth_type from animal where tag_id > 60;
    a61 = Animal(tag_id=61,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-15 12:00:00',birth_type=1 )
    a66 = Animal(tag_id=66,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-21 12:00:00',birth_type=1 )
    a67 = Animal(tag_id=67,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-21 12:00:00',birth_type=2 )
    a68 = Animal(tag_id=68,owner=user,sex=1,buck_id=31, doe_id=36, birth_date='2020-10-23 12:00:00',birth_type=2 )
    a74 = Animal(tag_id=74,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-24 12:00:00',birth_type=2 )
    a75 = Animal(tag_id=75,owner=user,sex=1,buck_id=31, doe_id=36, birth_date='2020-10-24 12:00:00',birth_type=2 )
    a78 = Animal(tag_id=78,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-30 12:00:00',birth_type=1 )
    a79 = Animal(tag_id=79,owner=user,sex=2,buck_id=31, doe_id=36, birth_date='2020-10-30 12:00:00',birth_type=1 )
    db.session.add(a61)
    db.session.add(a66)
    db.session.add(a67)
    db.session.add(a68)
    db.session.add(a74)
    db.session.add(a75)
    db.session.add(a78)
    db.session.add(a79)

    db.session.commit()
    print("Animal added - Khurram")
    '''
    farmdiary=# select id, tag_id, user_id, sex, buck_id, doe_id, birth_date, birth_type from animal where tag_id > 60;
     id | tag_id | user_id | sex | buck_id | doe_id |     birth_date      | birth_type 
    ----+--------+---------+-----+---------+--------+---------------------+------------
     25 |     61 |       2 |   2 |      31 |     36 | 2020-10-15 12:00:00 |          1
     26 |     66 |       2 |   2 |      31 |     36 | 2020-10-21 12:00:00 |          1
     27 |     67 |       2 |   2 |      31 |     36 | 2020-10-21 12:00:00 |          2
     28 |     68 |       2 |   1 |      31 |     36 | 2020-10-23 12:00:00 |          2
     29 |     74 |       2 |   2 |      31 |     36 | 2020-10-24 12:00:00 |          2
     30 |     75 |       2 |   1 |      31 |     36 | 2020-10-24 12:00:00 |          2
     31 |     78 |       2 |   2 |      31 |     36 | 2020-10-30 12:00:00 |          1
     32 |     79 |       2 |   2 |      31 |     36 | 2020-10-30 12:00:00 |          1

    '''
    #####################################################################
def addAnimalNadeem():
    # nadeemakram
    '''
    Sr. No	Tag No	Mother Id 	Father ID	Sex	Owner 	Breed	Status 	Born Date	birth type 1/2/3
    8	63	33	31	Male	Nadeem			19/10/2020
    21	76	35	31	Male	Nadeem			25/10/2020
    22	77	35	31	Female	Nadeem			25/10/2020

    :return:
    '''
    user = User.query.filter_by(username='nadeemakram').first_or_404()

    a63 = Animal(tag_id=63,owner=user,sex=1,buck_id=31, doe_id=33, birth_date='2020-10-19 12:00:00',birth_type=1 )
    a76 = Animal(tag_id=76,owner=user,sex=1,buck_id=31, doe_id=35, birth_date='2020-10-25 12:00:00',birth_type=2 )
    a77 = Animal(tag_id=77,owner=user,sex=1,buck_id=31, doe_id=35, birth_date='2020-10-25 12:00:00',birth_type=2 )


    db.session.add(a63)
    db.session.add(a76)
    db.session.add(a77)
    db.session.commit()
    print("Animal added - Nadeem")
    '''
    farmdiary=# select id, tag_id, user_id, sex, buck_id, doe_id, birth_date, birth_type from animal where tag_id > 50 and user_id=3;
     id | tag_id | user_id | sex | buck_id | doe_id |     birth_date      | birth_type 
    ----+--------+---------+-----+---------+--------+---------------------+------------
     33 |     63 |       3 |   1 |      31 |     33 | 2020-10-19 12:00:00 |          1
     34 |     76 |       3 |   1 |      31 |     35 | 2020-10-25 12:00:00 |          2
     35 |     77 |       3 |   1 |      31 |     35 | 2020-10-25 12:00:00 |          2
    (3 rows)

    '''
    #####################################################################
def addAnimalNaeem():
    # naeemakram
    '''
    1	56	46	31	Male	Naeem			12/10/2020
    2	57	46	31	Male	Naeem			12/10/2020
    3	58	45	31	Male	Naeem			13/10/2020
    4	59	45	31	Female	Naeem			13/10/2020
    5	60	40	31	Male	Naeem			14/10/2020
    7	62	43	31	Female	Naeem			17/10/2020
    9	64	55	31	Female	Naeem			20/10/2020
    10	65	55	31	Male	Naeem		Born Dead	20/10/2020
    14	69	47	31	Male	Naeem			23/10/2020
    15	70	47	31	Female	Naeem			23/10/2020
    16	71	44	31	Male	Naeem		Died	24/10/2020
    17	72	44	31	Male	Naeem		Died	24/10/2020
    18	73	44	31	Female	Naeem			24/10/2020
    '''

    user = User.query.filter_by(username='naeemakram').first_or_404()
    # farmdiary=# select id, tag_id, user_id, sex, buck_id, doe_id, birth_date, birth_type from animal where tag_id > 60;
    a56 = Animal(tag_id=56,owner=user,sex=2,buck_id=31, doe_id=46, birth_date='2020-10-12 12:00:00',birth_type=2 )
    a57 = Animal(tag_id=57,owner=user,sex=2,buck_id=31, doe_id=46, birth_date='2020-10-12 12:00:00',birth_type=2 )
    a58 = Animal(tag_id=58,owner=user,sex=2,buck_id=31, doe_id=45, birth_date='2020-10-13 12:00:00',birth_type=2 )
    a59 = Animal(tag_id=59,owner=user,sex=1,buck_id=31, doe_id=45, birth_date='2020-10-13 12:00:00',birth_type=2 )
    a60 = Animal(tag_id=60,owner=user,sex=2,buck_id=31, doe_id=40, birth_date='2020-10-14 12:00:00',birth_type=1 )
    a62 = Animal(tag_id=62,owner=user,sex=1,buck_id=31, doe_id=43, birth_date='2020-10-17 12:00:00',birth_type=1 )
    a64 = Animal(tag_id=64,owner=user,sex=1,buck_id=31, doe_id=55, birth_date='2020-10-20 12:00:00',birth_type=2 )
    a65 = Animal(tag_id=65,owner=user,sex=2,buck_id=31, doe_id=55, birth_date='2020-10-20 12:00:00',birth_type=2 )
    a69 = Animal(tag_id=69,owner=user,sex=2,buck_id=31, doe_id=47, birth_date='2020-10-23 12:00:00',birth_type=2 )
    a70 = Animal(tag_id=70,owner=user,sex=1,buck_id=31, doe_id=47, birth_date='2020-10-23 12:00:00',birth_type=2 )
    a71 = Animal(tag_id=71,owner=user,sex=2,buck_id=31, doe_id=44, birth_date='2020-10-24 12:00:00',birth_type=3 )
    a72 = Animal(tag_id=72,owner=user,sex=2,buck_id=31, doe_id=44, birth_date='2020-10-24 12:00:00',birth_type=3 )
    a73 = Animal(tag_id=73,owner=user,sex=1,buck_id=31, doe_id=44, birth_date='2020-10-24 12:00:00',birth_type=3 )


    db.session.add(a56)
    db.session.add(a57)
    db.session.add(a58)
    db.session.add(a59)
    db.session.add(a60)
    db.session.add(a62)
    db.session.add(a64)
    db.session.add(a65)
    db.session.add(a69)
    db.session.add(a70)
    db.session.add(a71)
    db.session.add(a72)
    db.session.add(a73)

    db.session.commit()
    print("Animal added - Naeem")

    '''
    farmdiary=# select id, tag_id, user_id, sex, buck_id, doe_id, birth_date, birth_type from animal where tag_id > 50 and user_id=4;
     id | tag_id | user_id | sex | buck_id | doe_id |     birth_date      | birth_type 
    ----+--------+---------+-----+---------+--------+---------------------+------------
     33 |     56 |       4 |   2 |      31 |     46 | 2020-10-12 12:00:00 |          2
     34 |     57 |       4 |   2 |      31 |     46 | 2020-10-12 12:00:00 |          2
     35 |     58 |       4 |   2 |      31 |     45 | 2020-10-13 12:00:00 |          2
     36 |     59 |       4 |   1 |      31 |     45 | 2020-10-13 12:00:00 |          2
     37 |     60 |       4 |   2 |      31 |     40 | 2020-10-14 12:00:00 |          1
     38 |     62 |       4 |   1 |      31 |     43 | 2020-10-17 12:00:00 |          1
     39 |     64 |       4 |   1 |      31 |     55 | 2020-10-20 12:00:00 |          2
     40 |     65 |       4 |   2 |      31 |     55 | 2020-10-20 12:00:00 |          2
     41 |     69 |       4 |   2 |      31 |     47 | 2020-10-23 12:00:00 |          2
     42 |     70 |       4 |   1 |      31 |     47 | 2020-10-23 12:00:00 |          2
     43 |     71 |       4 |   2 |      31 |     44 | 2020-10-24 12:00:00 |          3
     44 |     72 |       4 |   2 |      31 |     44 | 2020-10-24 12:00:00 |          3
     45 |     73 |       4 |   1 |      31 |     44 | 2020-10-24 12:00:00 |          3

    '''
    #####################################################################
def addAnimalSumaira():
    # sumairaakram
    user = User.query.filter_by(username='sumairaakram').first_or_404()
    #####################################################################
def addAnimalHuzaifa():
    # huzaifaakram
    user = User.query.filter_by(username='huzaifaakram').first_or_404()
    #####################################################################
def addAnimalAhmad():
    # ahmadakram
    user = User.query.filter_by(username='ahmadakram').first_or_404()
    #####################################################################
def addAnimalMajeed():
    # malikmajeed
    user = User.query.filter_by(username='malikmajeed').first_or_404()
    a3 = Animal(tag_id=3,owner=user,sex=1,purchased_price=17500)
    a4 = Animal(tag_id=4,owner=user,sex=1,purchased_price=12000)
    a8 = Animal(tag_id=8,owner=user,sex=2,purchased_price=25500)
    a10 = Animal(tag_id=10,owner=user,sex=1,purchased_price=16250)
    a11 = Animal(tag_id=11,owner=user,sex=1,purchased_price=17500)
    a12 = Animal(tag_id=12,owner=user,sex=1,purchased_price=17500)
    a13 = Animal(tag_id=13,owner=user,sex=1,purchased_price=16500)
    a14 = Animal(tag_id=14,owner=user,sex=1,purchased_price=16500)
    a15 = Animal(tag_id=15,owner=user,sex=1,purchased_price=17000)
    a16 = Animal(tag_id=16,owner=user,sex=1,purchased_price=20750)
    a17 = Animal(tag_id=17,owner=user,sex=1,purchased_price=20750)
    a18 = Animal(tag_id=18,owner=user,sex=1,purchased_price=17500)
    a19 = Animal(tag_id=19,owner=user,sex=1,purchased_price=16100)
    a20 = Animal(tag_id=20,owner=user,sex=1,purchased_price=21000)
    a21 = Animal(tag_id=21,owner=user,sex=1,purchased_price=21000)

    db.session.add(a3)
    db.session.add(a4)
    db.session.add(a8)
    db.session.add(a10)
    db.session.add(a11)
    db.session.add(a12)
    db.session.add(a13)
    db.session.add(a14)
    db.session.add(a15)
    db.session.add(a16)
    db.session.add(a17)
    db.session.add(a18)
    db.session.add(a19)
    db.session.add(a20)
    db.session.add(a21)

    db.session.commit()
    print("Animal added -  Malik Majeed")
#####################################################################
def addAnimalSana():
    # sananayab
    user = User.query.filter_by(username='sananayab').first_or_404()
    a5 = Animal(tag_id=5,owner=user,sex=1,purchased_price=12000)

    db.session.add(a5)
    db.session.commit()
    print("Animal  added - Sana")

#####################################################################

#####################################################################
#####################################################################
def addVaccination():
    # Do vaccination
    animal = Animal.query.filter_by(tag_id=1).first_or_404()
    vaccine_type = Vaccinetypes.query.filter_by(name='Entrerotoxemia').first_or_404()
    va1 = Vaccine(vaccinetypes_id = vaccine_type.id,patient = animal)
    animal = Animal.query.filter_by(tag_id=1).first_or_404()
    vaccine_type = Vaccinetypes.query.filter_by(name='Anthrax').first_or_404()
    va2 = Vaccine(vaccinetypes_id = vaccine_type.id,patient = animal)
    vaccine_type = Vaccinetypes.query.filter_by(name='Goat Pox').first_or_404()
    va3 = Vaccine(vaccinetypes_id = vaccine_type.id,patient = animal)
    vaccine_type = Vaccinetypes.query.filter_by(name='Pleuropneumonia').first_or_404()
    va4 = Vaccine(vaccinetypes_id = vaccine_type.id,patient = animal)
    animal = Animal.query.filter_by(tag_id=2).first_or_404()
    vaccine_type = Vaccinetypes.query.filter_by(name='Entrerotoxemia').first_or_404()
    va5 = Vaccine(vaccinetypes_id = vaccine_type.id,patient = animal)
    db.session.add(va1)
    db.session.add(va2)
    db.session.add(va3)
    db.session.add(va4)
    db.session.add(va5)
    db.session.commit()
    print("Animal - vaccine done")

def addExpensecategory():
    cat1 = ExpenseCategory(name='Animals', description='Animals')
    cat2 = ExpenseCategory(name='Fodder', description='Fodder')
    cat3 = ExpenseCategory(name='Fertilizers', description='Fertilizers')
    cat4 = ExpenseCategory(name='Machines', description='Machines')
    cat5 = ExpenseCategory(name='Vaccination goats', description='Vaccination goats')
    cat6 = ExpenseCategory(name='Maintenance', description='Maintenance')
    cat7 = ExpenseCategory(name='Electricity', description='Electricity')
    cat8 = ExpenseCategory(name='Salaries', description='Salaries')
    cat9 = ExpenseCategory(name='Hens', description='Hens')
    cat10 = ExpenseCategory(name='Vaccination hens', description='Vaccination hens')
    db.session.add(cat1)
    db.session.add(cat2)
    db.session.add(cat3)
    db.session.add(cat4)
    db.session.add(cat5)
    db.session.add(cat6)
    db.session.add(cat7)
    db.session.add(cat8)
    db.session.add(cat9)
    db.session.add(cat10)
    db.session.commit()
    print("Expense - Categories added")

def addExpense():

    user = User.query.filter_by(username='naeemakram').first_or_404()
    user = User.query.filter_by(username='khurramakram').first_or_404()

    exp_cat = ExpenseCategory.query.filter_by(name='Animals').first_or_404()
    exp1 = Expense(name='expense 1', categories=exp_cat, amount=1001, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Fodder').first_or_404()
    exp2 = Expense(name='expense 2', categories=exp_cat, amount=1002, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Fertilizers').first_or_404()
    exp3 = Expense(name='expense 3', categories=exp_cat, amount=1003, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Machines').first_or_404()
    exp4 = Expense(name='expense 4', categories=exp_cat, amount=1004, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Vaccination goats').first_or_404()
    exp5 = Expense(name='expense 5', categories=exp_cat, amount=1005, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Maintenance').first_or_404()
    exp6 = Expense(name='expense 6', categories=exp_cat, amount=1006, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Electricity').first_or_404()
    exp7 = Expense(name='expense 7', categories=exp_cat, amount=1007, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Salaries').first_or_404()
    exp8 = Expense(name='expense 8', categories=exp_cat, amount=1008, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Hens').first_or_404()
    exp9 = Expense(name='expense 9', categories=exp_cat, amount=1009, spender_id=user.id)
    exp_cat = ExpenseCategory.query.filter_by(name='Vaccination hens').first_or_404()
    exp10 = Expense(name='expense 10', categories=exp_cat, amount=1010, spender_id=user.id)

    db.session.add(exp1)
    db.session.add(exp2)
    db.session.add(exp3)
    db.session.add(exp4)
    db.session.add(exp5)
    db.session.add(exp6)
    db.session.add(exp7)
    db.session.add(exp8)
    db.session.add(exp9)
    db.session.add(exp10)


    db.session.commit()

    print("Expense - Expense added")
    #####################################################################
    #####################################################################
    #####################################################################
    #####################################################################
    #####################################################################




'''
# export FLASK_APP=run.py
# $ flask animals initdb

https://flask.palletsprojects.com/en/1.1.x/cli/
from flask import Blueprint
bp = Blueprint('students', __name__)
@bp.cli.command('create')
@click.argument('name')
def create(name):
app.register_blueprint(bp)
$ flask students create alice

popolate sqlite

DROP TABLE if EXISTS animal;
CREATE TABLE "animal" (
	"id"	INTEGER UNIQUE,
	"tag_id"	INTEGER,
	"owner_id"	INTEGER,
	"sex"	INTEGER,
	"purchased_price"	INTEGER,
	"image_file"	TEXT,
	"purchased_from"	TEXT,
	"description"	TEXT,
	"source"	TEXT,
	PRIMARY KEY("id")
);

mna
malik.akram@gmx.de
6ccb_saifulmalook.jpeg
$2b$12$52DQu6VwKoMp1UVbzFVsUOKvwHjpF.Uq/PIiLMyP8G2MOJ2j2kxRG

mna_gmail
malik.akram@gmail.com
5661_ansooLake.jpeg
$2b$12$7QNWwsShWnBrZh19BPQVKuChjLUF/9x81DQoKZ.SOB11cln48GxIW

'''


'''
SQL QUERIES
SELECT * from user
SELECT * from animal
SELECT * from vaccinetypes
select * from vaccine

SELECT tag_id, user_id, username, purchased_price from animal
inner JOIN user
on animal.user_id = user.id;



select  vaccinetypes_id,name, animal_id, tag_id, animal.purchased_price from vaccine
inner JOIN vaccinetypes
on vaccine.vaccinetypes_id = vaccinetypes.id
inner join animal
on vaccine.animal_id = animal.id
;
1	Entrerotoxemia	1	1	14250


'''


'''
$ flask init_db_cmd  bootstrap



sananayab
10
emaan_40@yahoo.com
default.jpg
$2b$12$VN2zvKzv4QJZ5qHxa4StHe5nqDbvDhsLv767Wq/qOIeGLUI5Da6D2


https://flask.palletsprojects.com/en/1.1.x/cli/
from flask import Blueprint
bp = Blueprint('students', __name__)
@bp.cli.command('create')
@click.argument('name')
def create(name):
app.register_blueprint(bp)
$ flask students create alice

'''



'''

----------------------------------



#@init_db_cmd.cli.command("doVaccination")
def doVaccination():
    animal = Animal.query.filter_by(tag_id=1).first_or_404()
    vaccine_type = Vaccinetypes.query.filter_by(name='Anthrax').first_or_404()
    va2 = Vaccine(
        vaccinetypes_id = vaccine_type.id,
        patient = animal
    )
    vaccine_type = Vaccinetypes.query.filter_by(name='Sheep Pox').first_or_404()
    va3 = Vaccine(
        vaccinetypes_id = vaccine_type.id,
        patient = animal
    )
    vaccine_type = Vaccinetypes.query.filter_by(name='Pleuropneumonia').first_or_404()
    va4 = Vaccine(
        vaccinetypes_id = vaccine_type.id,
        patient = animal
    )

    animal = Animal.query.filter_by(tag_id=2).first_or_404()
    vaccine_type = Vaccinetypes.query.filter_by(name='Entrerotoxemia').first_or_404()
    va5 = Vaccine(
        vaccinetypes_id = vaccine_type.id,
        patient = animal
    )
    db.session.add(va2)
    db.session.add(va3)
    db.session.add(va4)
    db.session.add(va5)


    db.session.commit()
    print("Animal - vaccine done")

    vaccinetypes_id = db.Column(db.Integer, db.ForeignKey('vaccinetypes.id'), nullable=False)
    animal_id = db.Column(db.Integer, db.ForeignKey('animal.id'), nullable=False)



'''