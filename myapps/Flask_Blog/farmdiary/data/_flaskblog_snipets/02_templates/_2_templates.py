from flask import Flask, render_template, url_for
app = Flask(__name__)

posts =[
    {
        'author': 'Naveed',
        'title': 'Blog Post 1',
        'content': 'First Blog Post',
        'date_posted': 'July 24, 2020'
    },
    {
        'author': 'Akram',
        'title': 'Blog Post 2',
        'content': '2nd  Blog Post',
        'date_posted': 'July 24, 2020'
    }
]

@app.route('/')
@app.route('/home')
def home():
    return render_template('/_2/home.html' , posts=posts)

@app.route('/about')
def about():
    return render_template('_2/about.html', title='About')

if __name__ == "__main__":
    app.run(debug=True)     # option 1 to restart server automatically as code changed.

    # 7.55