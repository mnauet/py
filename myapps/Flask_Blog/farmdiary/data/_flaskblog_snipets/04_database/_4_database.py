from datetime import datetime
from flask import Flask, render_template, url_for, flash, redirect
from flask_sqlalchemy import SQLAlchemy
from forms import RegistrationForm, LoginForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'f8fd49081affe715d0ca75c1efadf716'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)
# >>> user.posts
    # [Post('Blog_1', '2020-07-27 17:32:51.222995'),
    # Post('Blog_2', '2020-07-27 17:32:51.224079'),
    # Post('Blog_3', '2020-07-27 17:32:51.224362'),
    # Post('Blog_4', '2020-07-28 13:35:01.192444'),
    # Post('Blog_4', '2020-07-28 13:36:12.554814')]
    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default= datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
#   >>> post= Post.query.first()
#   >>> post
#   Post('Blog_1', '2020-07-27 17:32:51.222995')
#   >>> post.author
#   User('malik', 'm@blog.com', 'default.jpg')

    def __repr__(self):
        return f"Post('{self.title}', '{self.date_posted }')"


posts =[
    {
        'author': 'Naveed',
        'title': 'Blog Post 1',
        'content': 'First Blog Post',
        'date_posted': 'July 24, 2020'
    },
    {
        'author': 'Akram',
        'title': 'Blog Post 2',
        'content': '2nd  Blog Post',
        'date_posted': 'July 24, 2020'
    }
]

@app.route('/')
@app.route('/home')
def home():
    return render_template('/_3/home.html' , posts=posts)

@app.route('/about')
def about():
    return render_template('_3/about.html', title='About')

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form=LoginForm()
    if form.validate_on_submit():
        if form.email.data == 'admin@blog.com' and form.password.data == 'password':
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

if __name__ == "__main__":
    app.run(debug=True)     # option 1 to restart server automatically as code changed.








'''
# 2020 07 27 - Montag
pip install flask-sqlalchemy      SQLAlchemy-1.3.18 flask-sqlalchemy-2.4.4
python
from _4_database import db  # from flaskblog import db
db.create.all()

from _4_database import User, Post
user_1 = User(username='malik', email='m@demo.com', password='password')
user_2 = User(username='naveed', email='n@demo.com', password='password')
user_3 = User(username='akram', email='a@demo.com', password='password')
db.session.add(user_1)
db.session.add(user_2)
db.session.add(user_3)
db.session.commit()

User.query.all()
User.query.first()
User.query.filter_by(username='malik').all()
user=User.query.filter_by(username='malik').first()
user.id
    1
user = User.query.get(1)
User.query.get(user.id)

user
post_1= Post(title='Blog_1', content='first Post Content', user_id=user.id)
post_2= Post(title='Blog_2', content='seconud Post Content', user_id=user.id)
post_3= Post(title='Blog_3', content='third Post Content', user_id=user.id)
db.session.add(post_1)
db.session.add(post_2)
db.session.add(post_3)
db.session.commit()


user.posts
    []
for post in user.posts:
    print(post.title)
post= Post.query.first()
post    # Post('Blog_1', '2020-07-27 17:32:51.222995')
post.author # User('malik', 'm@blog.com', 'default.jpg')


db.drop_all
db.create_all()
Post.query.all()

>>> user.posts
[Post('Blog_1', '2020-07-27 17:32:51.222995'), Post('Blog_2', '2020-07-27 17:32:51.224079'), Post('Blog_3', '2020-07-27 17:32:51.224362'), Post('Blog_4', '2020-07-28 13:35:01.192444'), Post('Blog_4', '2020-07-28 13:36:12.554814')]

Lazy
This keyword is about how to load your DB Models into the memory, especially in the case of a 
one to many relationship.
By default sqlalchemy will load all child elements in a list when you first access/request 
the parent element.
The best use of lazy keyword, is to set it to 'dynamic', that way it'll load child elements 
only when you want to access them.

    
Backref
When you define a relationship in the parent model, you add the "backref" keyword 
only if you want to access a parent element from a requested child element.



'''