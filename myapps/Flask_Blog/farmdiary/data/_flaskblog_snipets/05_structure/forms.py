from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import  DataRequired, Length, Email, EqualTo

class RegistrationForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired(), Length(min=3, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


# Flask WTF, WT Forms, Validators documentation
#   https://flask-wtf.readthedocs.io/en/stable/
#   https://wtforms.readthedocs.io/en/2.3.x/            # API WTForms
#   https://wtforms.readthedocs.io/en/2.3.x/fields/     # API  fields
#   https://wtforms.readthedocs.io/en/2.3.x/validators/ # API  Validators
#   https://wtforms.readthedocs.io/en/2.3.x/widgets/    # API Widgets

#   https://flask.palletsprojects.com/en/1.1.x/patterns/flashing/
'''
pip install email_validator
####################################################################
# generate secret key
####################################################################

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ python
Python 3.6.9 (default, Apr 18 2020, 01:56:04) 
[GCC 8.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import secret
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'secret'
>>> import secrets
>>> secrets.token_hex(16)
'f8fd49081affe715d0ca75c1efadf716'
>>> exit()
'''