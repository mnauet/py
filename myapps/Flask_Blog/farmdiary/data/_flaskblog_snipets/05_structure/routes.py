from flask import  render_template, url_for, flash, redirect
from flaskblog import app
from flaskblog.forms import RegistrationForm, LoginForm
from flaskblog.models import User, Post

posts = [
    {
        'author': 'Naveed',
        'title': 'Blog Post 1',
        'content': 'First Blog Post',
        'date_posted': 'July 24, 2020'
    },
    {
        'author': 'Akram',
        'title': 'Blog Post 2',
        'content': '2nd  Blog Post',
        'date_posted': 'July 24, 2020'
    }
]


@app.route('/')
@app.route('/home')
def home():
    return render_template('/_3/home.html', posts=posts)


@app.route('/about')
def about():
    return render_template('_3/about.html', title='About')


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.email.data == 'admin@blog.com' and form.password.data == 'password':
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

'''

from flaskblog import db
from flaskblog.models import User, Post
db.create_all()
user.query.all()
'''