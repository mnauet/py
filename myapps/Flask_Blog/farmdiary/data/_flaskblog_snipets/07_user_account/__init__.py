from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt     # p6/4:20
from flask_login import LoginManager    #p5/20:00

app = Flask(__name__)
app.config['SECRET_KEY'] = 'f8fd49081affe715d0ca75c1efadf716'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

from flaskblog import routes
