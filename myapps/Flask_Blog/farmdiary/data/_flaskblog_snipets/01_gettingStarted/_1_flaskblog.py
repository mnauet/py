from flask import Flask
app = Flask(__name__)

@app.route('/')
@app.route('/home')
def home():
    return '<h1>Home Page -  Naveed Akram! </h1>'

@app.route('/about')
def about():
    return "<h1>About Page</h1>"

if __name__ == "__main__":
    app.run(debug=True)     # option 1 to restart server automatically as code changed.
#  export FLASK_DEBUG=1     # option 2 to restart server automatically as code changed.


# https://flask.palletsprojects.com/en/1.1.x/quickstart/#a-minimal-application

'''
#################################################################
# How to run it using export command.
#################################################################
#################################################################
#################################################################

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ export Flask_APP=_1_flaskblog.py    # no gap in comand
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ python -m flask run
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
Usage: python -m flask run [OPTIONS]

#################################################################
# method two 
#################################################################

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ python _1_flaskblog.py 

if __name__ == "__main__":
    app.run(debug=true)



'''