import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt     # p6/4:20
from flask_login import LoginManager    #p5/20:00
from flask_mail import Mail     #25.28

app = Flask(__name__)
app.config['SECRET_KEY'] = 'f8fd49081affe715d0ca75c1efadf716'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
#app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587  #TLS
app.config['MAIL_USE_TLS'] = True
#app.config['MAIL_PORT'] = 465   # SSL
#app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = os.environ.get('EMAIL_USER')
app.config['MAIL_PASSWORD'] = os.environ.get('EMAIL_PASS')
#app.config['MAIL_DEFAULT_SENDER'] = ('Malik Akram', 'dev.malik.akram@gmail.com')
mail = Mail(app)

from flaskblog import routes

