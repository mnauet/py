

from admin import create_app

app = create_app()
if __name__ == "__main__":
    app.run(debug=True)  # option 1 to restart server automatically as code changed.