from flask import Flask
from admin.config import Config

def create_app(config_class=Config):
    """Application-factory pattern"""
    app = Flask(__name__)

    from admin.myapp import simple_page

    #from farmdiary.users.routes import users
    app.register_blueprint(simple_page)

    return app