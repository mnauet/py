from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound

#users = Blueprint('users', __name__)
simple_page = Blueprint('simple_page', __name__)
#simple_page = Blueprint('simple_page', __name__, template_folder='templates')



#@users.route('/register', methods=['GET', 'POST'])
#def register():

#@simple_page.route('/')
#def show():
@simple_page.route('/', defaults={'page': 'index'})
@simple_page.route('/<page>')
def show(page):
    try:
        return render_template('index.html')
        #return render_template('pages/%s.html' % page)
    except TemplateNotFound:
        abort(404)

'''
http://127.0.0.1:5000/static/test.txt

#farmdiary/users/routes.py
users = Blueprint('users', __name__)
----------------------------------------------------------------------------
farmdiary/__init__.py

def create_app(config_class=Config):
    """Application-factory pattern"""
    app = Flask(__name__)
    db.init_app(app)

    from farmdiary.users.routes import users
    app.register_blueprint(users)
    return app
----------------------------------------------------------------------------
----------------------------------------------------------------------------
Flask_Blog/run.py
from farmdiary import create_app
app = create_app()
if __name__ == "__main__":
    app.run(debug=True)  # option 1 to restart server automatically as code changed.
----------------------------------------------------------------------------


'''