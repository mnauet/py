import psycopg2
import pprint

def main():
    #conn_string = " dbname='test' user='postgres' password='postgres'"
    #conn_string = " dbname='test' user='malik' password='malik123'" # working
    #conn_string = "dbname='farmdiary' user='malik' password='malik123'"  # working
    # conn_string = "dbname='farmdiary' user='postgres' password='postgres'" # not working as peer authentification fails
    #conn_string = "dbname='farmdiary' user='malik' password='malik123'" # working
    #conn_string = "dbname='test' user='malik' password='malik123'" # working and even fetching record

    #conn_string = "dbname='test' user='malik'" # working # working and even fetching record
    conn_string = "dbname='farmdiary' user='malik'" # working # working and even fetching record

    #print (" Connecting to database\n	->%s" % (conn_string) )



    try:
        conn = psycopg2.connect(conn_string)
        print("connected to the database")
        cursor = conn.cursor()
        #cursor.execute('SELECT * FROM person')
        cursor.execute('SELECT * FROM user')
        records = cursor.fetchall()
        pprint.pprint(records)


    except Exception as e:
        print("unable to connect to the database.")
        print(type(e))
        print(e.args)
        print(e)

if __name__ == "__main__":
    main()

'''

g/pgtest$ export FLASK_APP=pg1.py


import psycopg2
import pprint
conn_string = "dbname='farmdiary' user='postgres' password='postgres'"
# conn_string = "dbname='farmdiary' user='malik' password='malik123'"
conn = psycopg2.connect(conn_string)

cursor = conn.cursor()
cursor.execute('SELECT * FROM person')

sudo systemctl restart postgresql.service



(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/pgtest$ flask db --help
Usage: flask db [OPTIONS] COMMAND [ARGS]...

  Perform database migrations.

Options:
  --help  Show this message and exit.

Commands:
  branches   Show current branch points
  current    Display the current revision for each database.
  downgrade  Revert to a previous version
  edit       Edit a revision file
  heads      Show current available heads in the script directory
  history    List changeset scripts in chronological order.
  init       Creates a new migration repository.
  merge      Merge two revisions together, creating a new revision file
  migrate    Autogenerate a new revision file (Alias for 'revision...
  revision   Create a new revision file.
  show       Show the revision denoted by the given symbol.
  stamp      'stamp' the revision table with the given revision; don't run...
  upgrade    Upgrade to a later version

'''

'''
https://wiki.postgresql.org/wiki/Using_psycopg2_with_PostgreSQL

'''