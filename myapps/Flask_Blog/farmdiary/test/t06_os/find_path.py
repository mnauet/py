import os

from flask import Flask, current_app, send_from_directory,url_for, redirect
from PIL import Image
from flask import current_app



def save_picture(form_picture_data):
    random_hex = secrets.token_hex(2)
    f_name, f_ext = os.path.splitext(form_picture_data.filename)
    # _, f_ext = os.path.splitext(form_picture_data.filename)   # if a variable is not desired, use _
    picture_fn = random_hex + '_'+ f_name + f_ext
    # /home/malik/repos/py/myapps/Flask_Blog/farmdiary/static/profile_pics
    #picture_path = os.path.join(current_app.root_path, 'static/profile_pics', picture_fn)
    picture_path = os.path.join('/home/malik/MEDIA_FOLDER/animals_pics/', picture_fn)

    output_size=(125,125)
    i = Image.open(form_picture_data)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn



# configgure your app
app = Flask(__name__)

@app.route('/')
def hello_world():
    picture_path = os.path.join('/home/malik/MEDIA_FOLDER/animals_pics/', 'default.jpg')
    image = Image.open(picture_path)
    picture_path = os.path.join(current_app.root_path, 'default.jpg')
    return picture_path

@app.route('/MEDIA_PATH/<path:filename>')
def get_file(filename):
    return send_from_directory('/home/malik/MEDIA_FOLDER/', 'animals_pics/default.jpg', as_attachment=False)

@app.route('/showpath')
def showpath():
    return get_file('abc') # working, downloading file.

# Custom static data
# http://127.0.0.1:5000/cdn_animals/default.jpg
@app.route('/cdn_animals/<filename>')
def static_animals(filename):
    f = filename
    p1 = '/home/malik/MEDIA_FOLDER/'
    p2 = 'animals_pics'
    p= p1 + p2
    return send_from_directory(p , f) # working
@app.route('/getAnimalPic')
def get_animal_pic():
    return redirect( url_for('static_animals', filename='default.jpg')) # working

# http://127.0.0.1:5000/cdn_profie/default.jpg
@app.route('/cdn_profie/<filename>')
def static_profile(filename):
    f = filename
    p1 = '/home/malik/MEDIA_FOLDER/'
    # /home/malik/MEDIA_FOLDER/profile_pics
    p2 = 'profile_pics'
    p= p1 + p2
    return send_from_directory(p , f) # working
@app.route('/getProfilePic')
def get_profile_pic():
    return redirect( url_for('static_profile', filename='default.jpg')) # working


# image_file = url_for('static', filename='profile_pics/' + current_user.image_file)  # good

if __name__ == "__main__":
    app.run(debug=True)



