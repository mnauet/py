from flask import Flask

# configgure your app
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

if __name__ == "__main__":
    app.run(debug=True)

'''
2020 10 28

https://flask.palletsprojects.com/en/1.1.x/quickstart/

'''