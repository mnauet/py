from flask_script import Manager, Command, Option

from myapp import app

manager = Manager(app)

##############################################################

@manager.command
def hello1():
    print('hello from flask scirpt - command ')

##############################################################

class Hello2(Command):
    def run(self):
        print('Hello 2 from command class')
manager.add_command('hello2', Hello2())

##############################################################

#=============================================================

class Hello3(Command):
    option_list = (
        Option('--name', '-n', dest='name', default='joe'),
    )

    def run(self,name):
        print('Hello 3 %s' % name)

manager.add_command('hello3', Hello3())
7
#=============================================================

# https://flask-script.readthedocs.io/en/latest/
#=============================================================


@manager.command
def hello4(name):   # positional arguments, extracted directly from parameters with @command decorator
    print('hello 4 - command ', name)

#=============================================================
# Optional arguments

@manager.command
def hello5(name = "Fred"):   # positional arguments, extracted directly from parameters with @command decorator
    print('hello 5 with optional name - command ', name)

    # The short form -n is formed from the first letter of the argument
    # $ python manage.py hello5 -n malik
    # $ python manage.py hello5 --name malik
#=============================================================

if __name__ == '__main__':
    manager.run()


'''
https://flask-script.readthedocs.io/en/latest/
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello
hello from flask scirpt - command 

$ python manage.py -?
usage: manage.py [-?] {hello1,hello2,shell,runserver} ...

$ python manage.py 
usage: manage.py [-?] {hello1,hello2,shell,runserver} ...

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello1
hello from flask scirpt - command 
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello2
Hello 2 from command class

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello3 --name joe
Hello 3 joe
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello3 -n joe
Hello 3 joe

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello4 malik
hello 4 - command  malik

(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello5
hello 5 with optional name - command  Fred
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello5 --name malik
hello 5 with optional name - command  malik
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog/farmdiary/test/pg2_flask_script$ python manage.py hello5 -n malik
hello 5 with optional name - command  malik

'''