from datetime import datetime
from flask_wtf import FlaskForm
from wtforms import (StringField, SubmitField, TextAreaField, IntegerField,BooleanField,
                     RadioField,SelectField, DateField, DateTimeField, DecimalField)

from wtforms.validators import DataRequired, Length, NumberRange, Optional






class AddExpenseCategoryForm(FlaskForm):
    name = StringField('Expense Category Name:',validators=[DataRequired()]) # exp name
    description = TextAreaField('Description:', validators=[Optional()])
    #created_by = SelectField(u'Created By:', coerce=int, validate_choice=False ) # one doing entry
    created_on = DateField('Expense Date', validators=[Optional()], format='%Y-%m-%d') # when expense done, time
    submit = SubmitField('Add Expense')

    # constructor
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.created_on.data:
            self.created_on.data= datetime.utcnow()








class AddExpenseForm(FlaskForm):
    submitter = SelectField(u'Submitted By:', coerce=int, validate_choice=False ) # one doing entry
    spender = SelectField(u'Spended By:', coerce=int, validate_choice=False )
    name = StringField('Expense Name:',validators=[DataRequired()]) # exp name
    amount = DecimalField('Expense Amount in Rs:', places=0, rounding=None,  validators=[Optional()], default=99.990)
    category = SelectField(u'Expense Category:', coerce=int, validate_choice=False)
    created_on = DateTimeField('Expense created Date', validators=[Optional()], format='%Y-%m-%d %H:%M:%S') # system time
    spent_on = DateField('Expense Date', validators=[Optional()], format='%Y-%m-%d') # when expense done, time
    description = TextAreaField('Description:', validators=[Optional()])
    submit = SubmitField('Add Expense')

    # constructor
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.created_on.data:
            self.created_on.data= datetime.utcnow()
        if not self.spent_on.data:
            self.spent_on.data = datetime.utcnow().date()