from datetime import datetime
from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from farmdiary import db
from flask_login import  current_user
from farmdiary.models import (Animal, User, Vaccine, Vaccinetypes, Breed, Doctor,Farm,
                              FarmType,Mandi,Symptom, Animalseller, Breed, ExpenseCategory, Expense)
from farmdiary.finance.forms import AddExpenseForm,AddExpenseCategoryForm
from farmdiary.users.utils import save_picture

finance = Blueprint('finance', __name__)


# TEST
@finance.route('/finance/test', methods=['GET', 'POST'])
def test():
    return render_template('finance/test.html', title='Finance Test')



@finance.route('/finance/add_exp_cat', methods=['GET', 'POST'])
def add_expense_category():
    form = AddExpenseCategoryForm()
    exp_cat = ExpenseCategory()
    if form.validate_on_submit():
        #exp_cat.created_by = current_user.id
        exp_cat.creator = current_user
        exp_cat.name = form.name.data
        exp_cat.created_on = datetime.utcnow().date()
        exp_cat.description = form.description.data
        db.session.add(exp_cat)
        db.session.commit()
        flash('Add another Expense Category - info', 'info')
        flash('Expense Category is added', 'success')
        return redirect(url_for('finance.add_expense_category'))
    #elif request.method == 'GET':
        #flash('get successful', 'success')

    return render_template('finance/add_expense_category.html', title='Add Expense Category', form=form)


@finance.route('/finance/add_exp', methods=['GET', 'POST'])
def add_expense():
    form = AddExpenseForm()
    owner_list = User.query.all()
    owner = [(i.id, i.username) for i in owner_list]
    form.submitter.choices = current_user
    form.spender.choices = owner

    cat_list = ExpenseCategory.query.all()
    cat = [(i.id, i.name) for i in cat_list]
    form.category.choices = cat
    expense = Expense()
    if form.validate_on_submit():
        user = User.query.filter_by(username='malikakram').first_or_404()
        expense.submitter = user
        user = User.query.filter_by(id=form.spender.data).first_or_404()
        expense.spender = user
        expense.name = form.name.data
        cat = ExpenseCategory.query.filter_by(id=form.category.data).first_or_404()
        expense.categories = cat
        expense.created_on = form.created_on.data
        expense.spent_on = form.spent_on.data
        expense.description = form.description.data
        db.session.add(expense)
        db.session.commit()
        flash('Add another Expense - info!', 'info')
        #flash('Add another Expense warning!', 'warning')
        #flash('Add another Expense error!', 'error')
        flash('Expense is added', 'success')
        return redirect(url_for('finance.add_expense'))
    #elif request.method == 'GET':
        #flash('get successful', 'success')

    return render_template('finance/add_expense.html', title='Add Expense', form=form, legend='Add Expense')

@finance.route('/finance/show_exp_cat', methods=['GET', 'POST'])
def show_expense_category():
    page = request.args.get('page', 1, type=int)
    form = AddExpenseCategoryForm()
    # result = session.query(Customers).filter(Customers.id == 2)
    # result = session.query(Customers).filter(Customers.id! = 2)
    # result = session.query(Customers).filter(Customers.name.like('Ra%'))
    # result = session.query(Customers).filter(Customers.id.in_([1,3]))
    # result = session.query(Customers).filter(Customers.id>2, Customers.name.like('Ra%'))
    # result = session.query(Customers).filter(or_(Customers.id > 2, Customers.name.like('Ra%')))

    exp_cats = ExpenseCategory.query.filter(ExpenseCategory.id>0) \
        .order_by(ExpenseCategory.id.asc())  \
        .paginate(page=page, per_page=25)
    return render_template('finance/show_expense_category.html', title='Add Expense',
                           form=form, exp_cats=exp_cats, legend='Add Expense')

@finance.route('/finance/show_exp', methods=['GET', 'POST'])
def show_expense():
    form = AddExpenseForm()
    page = request.args.get('page', 1, type=int)
    exp = Expense.query.filter(Expense.id>2) \
        .order_by(Expense.id.asc())  \
        .paginate(page=page, per_page=25)
    return render_template('finance/show_expense.html', title='Add Expense',
                           form=form, exp=exp, legend='Add Expense')

