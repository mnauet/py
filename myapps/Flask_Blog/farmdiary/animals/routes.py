from datetime import datetime
from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from farmdiary import db
#from farmdiary.models import Post
from farmdiary.models import Animal, User, Vaccine, Vaccinetypes, Breed, Doctor,Farm, FarmType,Mandi,Symptom, Animalseller, Breed, Weight
#from farmdiary.posts.forms import PostForm
from farmdiary.animals.forms import (AddAnimalForm,testAnimalForm, AddAnimalKidForm,
                                     SearchAnimalForm, AddAnimalWeightForm, SearchAnimalForm3,
                                     AddAnimalWeightListForm, AddPictureForm)
from farmdiary.users.utils import save_picture

animals = Blueprint('animals', __name__)


# TEST
@animals.route('/animal/test', methods=['GET', 'POST'])
def test():
    form = testAnimalForm()
    #form = AddAnimalForm()
    animal = Animal.query.get_or_404(1)
    if form.validate_on_submit():
        animal.sex = form.sex.data
        db.session.commit()
        flash('POSTed', 'success')
        return redirect(url_for('animals.test'))
        #return redirect(url_for('main.home'))
    elif request.method == 'GET':
        flash('GETed', 'success')

    return render_template('animals/test.html', title=animal.tag_id, animal=animal, form=form)

# TEST 2
@animals.route('/animal/test2', methods=['GET', 'POST'])
def test2():
    form = testAnimalForm()
    #form = AddAnimalForm()
    animal = Animal.query.get_or_404(1)
    if form.validate_on_submit():
        animal.sex = form.sex.data
        db.session.commit()
        flash('POSTed', 'success')
        return redirect(url_for('animals.test'))
        #return redirect(url_for('main.home'))
    elif request.method == 'GET':
        flash('GETed', 'success')

    return render_template('animals/test2.html', title=animal.tag_id, animal=animal, form=form)
# TEST 3
@animals.route('/animal/test3', methods=['GET', 'POST'])
def test3():
    form = testAnimalForm()
    #form = AddAnimalForm()
    animal = Animal.query.get_or_404(1)
    if form.validate_on_submit():
        animal.sex = form.sex.data
        db.session.commit()
        flash('POSTed', 'success')
        return redirect(url_for('animals.test'))
        #return redirect(url_for('main.home'))
    elif request.method == 'GET':
        flash('GETed', 'success')

    return render_template('animals/test3.html', title=animal.tag_id, animal=animal, form=form)

@animals.route('/animal/test_pic', methods=['GET', 'POST'])
#@login_required
def test_picture():
    form = AddPictureForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            flash('Picture is added', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        True
    return render_template('animals/test_pic.html', title='New Animal', form=form, legend='Add New Animal')

# Add an Animal
@animals.route('/animal/add', methods=['GET', 'POST'])
#@login_required
def add_animal():
    # mandi = SelectField(u'Purchased from Mandi', coerce=int, validate_choice=False ) #

    form = AddAnimalForm()
    sex = [(1,'Female'), (2,'Male')] # # 1= Female, 2= Male
    form.sex.choices = sex

    mandi_list = Mandi.query.all()
    mandi = [(i.id,i.title) for i in mandi_list]
    form.mandi.choices = mandi

    owner_list =  User.query.all()
    owner = [(i.id, i.username) for i in owner_list]
    form.owner.choices = owner

    seller_list = Animalseller.query.all()
    seller = [(i.id, i.name) for i in seller_list]
    form.seller.choices = seller

    farm_list = Farm.query.all()
    farm = [(i.id, i.title) for i in farm_list]
    form.farm.choices = farm

    breed_list = Breed.query.all()
    breed = [(i.id, i.title) for i in breed_list]
    form.breed.choices = breed

    #form.purchased_date = datetime.utcnow().date()

    animal =Animal()

    if form.validate_on_submit():

        animal.tag_id = form.tag_id.data
        animal.sex = form.sex.data

        breed = Breed.query.filter_by(id=form.breed.data).first_or_404()
        animal.breed_id = breed.id

        mandi = Mandi.query.filter_by(id=form.mandi.data).first_or_404()
        animal.purchased_from = mandi.id

        animal.purchased_batch = form.purchased_batch.data
        animal.purchased_date = form.purchased_date.data
        animal.purchased_price = form.purchased_price.data

        animal.description = form.description.data

        farm = Farm.query.filter_by(id=form.farm.data).first_or_404()
        animal.farm_id = farm.id

        animal.pen = form.pen.data
        animal.herd = form.herd.data
        user = User.query.filter_by(id=form.owner.data).first_or_404()
        animal.owner = user
        animal.start_weight = form.start_weight.data

        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            animal.image_file = picture_file
        db.session.add(animal)
        db.session.commit()
        flash('Animal is added', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        #form.title.data = cu
        #form.content.data =
        flash('Your message has been created', 'success')
    return render_template('animals/add_animal.html', title='New Animal', form=form, legend='Add New Animal')

# Add an Animal - new birth - A Kid
@animals.route('/animal/addkid', methods=['GET', 'POST'])
#@login_required
def add_animal_kid():
    form = AddAnimalKidForm()

    sex = [(1,'Female'), (2,'Male')] # # 1= Female, 2= Male
    form.sex.choices = sex

    owner_list =  User.query.all()
    owner = [(i.id, i.username) for i in owner_list]
    form.owner.choices = owner

    farm_list = Farm.query.all()
    farm = [(i.id, i.title) for i in farm_list]
    form.farm.choices = farm

    breed_list = Breed.query.all()
    breed = [(i.id, i.title) for i in breed_list]
    form.breed.choices = breed

    animal = Animal()

    if form.validate_on_submit():

        animal.tag_id = form.tag_id.data
        animal.sex = form.sex.data

        breed = Breed.query.filter_by(id=form.breed.data).first_or_404()
        animal.breed_id = breed.id

        animal.description = form.description.data

        farm = Farm.query.filter_by(id=form.farm.data).first_or_404()
        animal.farm_id = farm.id

        animal.pen = form.pen.data
        animal.herd = form.herd.data

        animal.buck_id = form.buck_id.data
        animal.doe_id = form.doe_id.data

        user = User.query.filter_by(id=form.owner.data).first_or_404()
        animal.owner = user

        animal.start_weight = form.start_weight.data

        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            animal.image_file = picture_file

        db.session.add(animal)
        db.session.commit()
        flash('Animal is added', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        # form.title.data = cu
        # form.content.data =
        flash('Your message has been created', 'success')
    return render_template('animals/add_animal_kid.html', title='New Birth', form=form, legend='Add new kid')



# Show an Animal
@animals.route('/animal/<int:animal_id>')
def show_animal(animal_id):
    animal = Animal.query.get_or_404(animal_id)
    return render_template('animals/animal.html', title=animal.tag_id, animal=animal)

# Show an Animals
@animals.route('/animals')
def animals_list():
    page = request.args.get('page', 1, type=int)
    #animals = Animal.query.all()

    username = 'khurramakram'
    user = User.query.filter_by(username=username).first_or_404()
    animals = Animal.query.filter_by(owner=user) \
        .order_by(Animal.tag_id.asc())  \
        .paginate(page=page, per_page=25)

    # .order_by(Animal.tag_id.asc())\
    # .paginate(page=page, per_page=25)
    return render_template('animals/animals.html', title='some farm Animals', animals=animals, user=user)

# Update individual Animal Information
@animals.route('/animal/<int:animal_id>/update', methods=['GET', 'POST'])
#@login_required
def update_animal(animal_id):
    animal = Animal.query.get_or_404(animal_id)
    #animal = Animal.query.get_or_404(animal_id)
    #if post.author != current_user :
    #    abort(403)
    form = AddAnimalForm()
    if form.validate_on_submit():
        animal.tag_id = form.tag_id.data
        #animal.owner = form.owner
        animal.sex = form.sex.data
        animal.purchased_price = form.purchased_price.data
        animal.description = form.description.data

        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            animal.image_file = picture_file

        db.session.commit()
        flash('Animal data  has been updated', 'success')
        return redirect(url_for('main.home'))
        #return redirect(url_for('Animal Description', animal_id=animal.id))
        #return redirect(url_for('posts.post', post_id=ble post.id))
    elif request.method == 'GET':
         form.tag_id.data = animal.tag_id
         #form.owner_id.data = animal.owner_id
         form.sex.data = animal.sex
         form.purchased_price.data = animal.purchased_price
         form.description.data = animal.description

    return render_template('animals/add_animal.html', title= 'Update Animal Information',
                           form=form, legend='Update Animal')


@animals.route('/animal/show_animalmap', methods=['GET', 'POST'])
#@login_required
def show_animal_map():
    page = request.args.get('page', 1, type=int)
    animals = Animal.query.order_by(Animal.tag_id.asc()) \
        .paginate(page=page, per_page=100)
    return render_template('animals/animal_map.html', title='Map',animals=animals)

@animals.route('/animal/addWeightAnimalSearch', methods=['GET', 'POST'])
#@login_required
def add_animal_weight_search():
    form = SearchAnimalForm()
    if form.validate_on_submit():
        animal = Animal.query.filter_by(tag_id=form.tag_id.data).first()
        if animal is None:
            flash('Animal with this Tag Number does not exist!', 'danger')
            return redirect(url_for('animals.add_animal_weight_search', tag=1))  #
        tag = animal.tag_id
        return redirect(url_for('animals.add_animal_weight', tag=tag))#
    return render_template('animals/add_animal_weight_search.html', title='add Animal weight',
                           form=form, legend='search Animal to add Weight')


@animals.route('/animal/addWeightList/<tag>', methods=['GET', 'POST'])
#@login_required
def add_animal_weight_list(tag):
    form = AddAnimalWeightForm()
    form.tag_id.data = tag
    animal = Animal.query.filter_by(tag_id=tag).first_or_404()
    if form.validate_on_submit():
        wt = Weight()
        wt.growth = animal
        wt.weight = form.weight.data
        db.session.add(wt)
        db.session.commit()
        flash('Add Weight to another Animal', 'info')
        return redirect(url_for('animals.add_animal_weight_list', tag=1))
    return render_template('animals/add_animal_weight_list.html', title='Add Weight',
                           form=form, animal=animal, legend='Add  Weight')

@animals.route('/animal/addWeight/<tag>', methods=['GET', 'POST'])
#@login_required
def add_animal_weight(tag):
    form = AddAnimalWeightForm()
    form.tag_id.data = tag
    animal = Animal.query.filter_by(tag_id=tag).first_or_404()
    if form.validate_on_submit():
        animal.current_weight = form.weight.data
        wt = Weight()
        wt.growth = animal
        wt.weight = form.weight.data
        db.session.add(wt)
        db.session.commit()
        flash('Add Weight to another Animal', 'info')
        #return redirect(url_for('animals.add_animal_weight_search'))
        return redirect(url_for('animals.show_animal', animal_id=animal.id))
    return render_template('animals/add_animal_weight.html', title='Add Weight',
                           form=form, animal=animal, legend='Add  Weight')

@animals.route('/animal/searchAnimalTag', methods=['GET', 'POST'])
#@login_required
def search_animal_tag():
    page = request.args.get('page', 1, type=int)
    animals = Animal.query.order_by(Animal.tag_id.asc()) \
        .paginate(page=page, per_page=100)

    form = SearchAnimalForm3()
    tag_list = Animal.query.order_by(Animal.tag_id.asc())
    cat = [(i.tag_id, i.tag_id) for i in tag_list]
    form.tag_id.choices = cat
    if form.validate_on_submit():
        animal = Animal.query.filter_by(tag_id=form.tag_id.data).first()

        if animal is None:
            flash('Animal with this Tag Number does not exist!', 'danger')
            return redirect(url_for('animals.add_animal_weight_search_map'))  #
        tag = animal.tag_id
        #return redirect(url_for('animals.add_animal_weight_map', tag=tag))#
        return redirect(url_for('animals.add_animal_weight', tag=tag))#
    return render_template('animals/add_animal_weight_search_map.html', title='add Animal weight',
                           form=form,animals=animals, legend='search Animal to add Weight')


@animals.route('/animal/addWeightAnimalSearch_map', methods=['GET', 'POST'])
#@login_required
def add_animal_weight_search_map():
    page = request.args.get('page', 1, type=int)
    animals = Animal.query.order_by(Animal.tag_id.asc()) \
        .paginate(page=page, per_page=100)

    form = SearchAnimalForm3()

    return render_template('animals/add_animal_weight_search_map.html', title='add Animal weight',
                           form=form,animals=animals, legend='search Animal to add Weight')



@animals.route('/animal/addWeight_map/<tag>', methods=['GET', 'POST'])
#@login_required
def add_animal_weight_map(tag):
    form = AddAnimalWeightForm()
    form.tag_id.data = tag
    animal = Animal.query.filter_by(tag_id=tag).first_or_404()
    if form.validate_on_submit():
        animal.current_weight = form.weight.data
        wt = Weight()
        wt.growth = animal
        wt.weight = form.weight.data
        db.session.add(wt)
        db.session.commit()
        flash('Add Weight to another Animal', 'info')
        return redirect(url_for('animals.add_animal_weight_search_map'))
    return render_template('animals/add_animal_weight_map.html', title='Add Weight',
                           form=form, animal=animal, legend='Add  Weight')

@animals.route('/animal/addWeightMulti/', methods=['GET', 'POST'])
#@login_required
def add_animal_weight_multi():
    results = []
    # lst = [{'tag_id': 1, 'weight': 1.2},{'tag_id': 2, 'weight': 1.2}]
    #ani_dict = {}
    ani_list = []
    animals = Animal.query.order_by(Animal.tag_id.asc())
    for i in animals:
        ani_dict = {"tag_id": i.tag_id, "weight": 0.0}
        ani_list.append(ani_dict)
    form = AddAnimalWeightListForm(list=ani_list)
    results = ani_list
    if form.validate_on_submit():
        #for  data in enumerate(form.list.data): # 1: [1.000]:[1] # output
        for field in form.list: # 1: [1.000]:[1] # output
            wt = Weight()
            #ani = field["tag_id"]
            #weight = field["weight"]
            tag_value = field.tag_id.data
            weight_value = field.weight.data
            if weight_value > 0.0: # only add a tag, if weight is entered i.e not =0.0
                animal = Animal.query.filter_by(tag_id=tag_value).first_or_404()
                wt.growth = animal
                wt.weight = weight_value
                db.session.add(wt)
                db.session.commit()
        flash('Add Weight to another Animal', 'info')
        #return redirect(url_for('animals.animals/add_animal_weight_list_2', form=form))
        #return redirect(url_for('animals.animals/add_animal_weight_list_2', results=results, form=form))
        #return render_template('animals/results.html', results=results)
    return render_template('animals/add_animal_weight_multi.html', title='Add Weight - multi',
                           form=form, legend='Add  Weight - multi',
                           results=results)
##############################################################################

##############################################################################
# https://stackoverflow.com/questions/35465283/how-to-get-form-data-from-input-as-variable-in-flask?lq=1
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
