from datetime import datetime
from flask_wtf import FlaskForm
from farmdiary.models import User, Animal
from flask_wtf.file import FileField, FileAllowed
from wtforms import (StringField, SubmitField, TextAreaField, IntegerField,
                     BooleanField,
                     RadioField,SelectField, DateField, DateTimeField,
                     DecimalField, FieldList,FormField, Form)
from wtforms.validators import DataRequired, Length, NumberRange, Optional, ValidationError, InputRequired

# And, if you want to access it from Jinja2, you can do: {% if form.my_field.render_kw.readonly } readonly blabla {% endif %}

class AddPictureForm(FlaskForm):
    picture = FileField('Update Profile Picture:', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    submit = SubmitField('Add Picture')

class AddAnimalForm(FlaskForm):
    tag_id = IntegerField('Tag ID:',validators=[DataRequired()])
    sex = SelectField(u'Sex:', coerce=int, validate_choice=False ) # 1= Female, 2= Male
    breed = SelectField(u'Breed:', coerce=int, validate_choice=False )
    purchased_batch = IntegerField('Purchased Batch Nr.:', validators=[Optional()])
    purchased_date = DateField('Purchased Date', validators=[Optional()], format='%Y-%m-%d')
    mandi = SelectField(u'Mandi Name:', coerce=int, validate_choice=False ) # where it is purchased from
    purchased_price = IntegerField('Purchased Price in Rs:', validators=[Optional()])
    seller = SelectField(u'Purchased from Person:', coerce=int, validate_choice=False ) # old owner - purchased from person
    farm = SelectField(u'Farm Name:', coerce=int, validate_choice=False ) # new farm name
    pen = IntegerField('Pen:', validators=[Optional()])
    herd = IntegerField('Herd:', validators=[Optional()])
    owner = SelectField(u'Owner:', coerce=int, validate_choice=False ) # new owner name
    start_weight = DecimalField('Birth/Start Weight in Kg:', places=2, rounding=None,  validators=[Optional()], default=99.990)
    description = TextAreaField('Description:', validators=[Optional()])
    picture = FileField('Update Profile Picture:', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    submit = SubmitField('Add Animal')
    # constructor
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.purchased_date.data:
            self.purchased_date.data= datetime.utcnow().date()

class AddAnimalKidForm(FlaskForm):
    tag_id = IntegerField('Tag ID:',validators=[DataRequired()])
    sex = SelectField(u'Sex:', coerce=int, validate_choice=False ) # 1= Female, 2= Male
    breed = SelectField(u'Breed:', coerce=int, validate_choice=False )
    farm = SelectField(u'Farm Name:', coerce=int, validate_choice=False ) # new farm name
    pen = IntegerField('Pen:', validators=[Optional()])
    herd = IntegerField('Herd:', validators=[Optional()])
    owner = SelectField(u'Owner:', coerce=int, validate_choice=False ) # new owner name
    start_weight = DecimalField('Birth/Start Weight in Kg:', places=2, rounding=None,  validators=[Optional()], default=99.990)
    buck_id = IntegerField('Buck/Father tag nr. :', validators=[Optional()])
    doe_id = IntegerField('Doe/Kid mother tag nr. :', validators=[Optional()])
    birth_date = DateField('Birth Date', validators=[Optional()], format='%Y-%m-%d')
    birth_type = IntegerField('Nr. Of Kids:', validators=[Optional()]) # 1 single 2= double, 3= three, 4 =quad
    description = TextAreaField('Description:', validators=[Optional()])
    picture = FileField('Update Profile Picture:', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    submit = SubmitField('Add  Kid')
    # constructor
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.birth_date.data:
            self.birth_date.data= datetime.utcnow().date()

class testAnimalForm(FlaskForm):
    id = IntegerField('Tag ID',validators=[Optional()])
    sex = SelectField(u'Sex', coerce=int, choices=[('1', 'Female'), ('2', 'Male') ], validate_choice=False ) # 1= Female, 2= Male
    submit = SubmitField('Test')

class SearchAnimalForm(FlaskForm):
    tag_id = IntegerField('Enter Animal Tag',validators=
            [InputRequired(),NumberRange(min=1, max=9999)]) # DataRequired
    submit = SubmitField('Search Anmial')

    def validate_tagnr(self, tag_id):
        animal = Animal.query.filter_by(tag_id=tag_id.data).first()
        #if animal.tag_id != tag_id.data :
        if animal is None:
            raise ValidationError('No animal with this tag number')

class SearchAnimalForm3(FlaskForm):
    tag_id = SelectField(u'Tag Nr.:', coerce=int, validate_choice=False )
    submit = SubmitField('Search Anmial')

class AddAnimalWeightForm(FlaskForm):
    tag_id = IntegerField('Animal Tag', validators=[Optional()],  render_kw={'readonly': True})
    weight = DecimalField('Weight in Kg:', places=3, rounding=None,
                          validators=[InputRequired(), NumberRange(min=0, max=99)], default=0.000)
    submit = SubmitField('Add Animal Weight ')

class WeightForm(Form):
    tag_id = IntegerField('Animal Tag', validators=[Optional()],  render_kw={'readonly': True})
    weight = DecimalField('Weight in Kg:', places=3, rounding=None,
                          validators=[Optional(), NumberRange(min=0, max=99)], default=0.000)

class AddAnimalWeightListForm(FlaskForm):
    list = FieldList(FormField(WeightForm), min_entries=2)
    submit = SubmitField('Add Animals Weight ')

'''
class MyItem(Form):
    name = TextField('Name')
    quantity = IntegerField('Quantity')
class ShoppingListForm(Form):
    items = FieldList(FormField(MyItem))
    

shopping_form = ShoppingListForm(items=[{'name': 'Bread', 'quantity': 1}])
    
    for item in shopping_form.items:
    print('Need to buy {}x{}'.format(item.name, item.quantity))
'''