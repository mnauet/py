#from bookreadings import bookreadings_create_app
#bookreadings_app = bookreadings_create_app()

from farmdiary import farmdiary_create_app
farmdiary_app = farmdiary_create_app()

if __name__ == "__main__":
    #bookreadings_app.run(debug=True)  # option 1 to restart server automatically as code changed.
    farmdiary_app.run(debug=True)  # option 1 to restart server automatically as code changed.



'''
2020 08 23
#######################################################################
# quick server start
ssh malik@172.104.182.148

#######################################################################

2020 08 23
(venv) malik@ubuntu11:~/repos/py/coreySchafer_flask/Flask_Blog$ python run.py

'''