create table car (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	make VARCHAR(100) NOT NULL,
	model VARCHAR(100) NOT NULL,
	price NUMERIC(19,0) NOT NULL
);

create table person (
    id BIGSERIAL NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
    email VARCHAR(50),
	gender VARCHAR(50) NOT NULL,
	date_of_birth DATE NOT NULL,
	country_of_birth VARCHAR(50) NOT NULL,
	car_id BIGINT REFERENCES car (id), UNIQUE (car_id)

);

insert into car (id, make, model, price) values (1, 'Porsche', 'Boxster', '1658995.34');
insert into car (id, make, model, price) values (2, 'GMC', 'Yukon XL 2500', '4869561.28');
insert into car (id, make, model, price) values (3, 'Pontiac', 'Grand Am', '8233398.50');


insert into person (first_name, last_name, gender, date_of_birth, country_of_birth, email) values ('Hobie', 'Demangel', 'Male', '2019-10-29', 'New Zealand', 'hdemangel9@xrea.com');
insert into person (first_name, last_name, gender, date_of_birth, country_of_birth, email) values ('Maude', 'Spry', 'Female', '2019-11-11', 'France', 'msprya@buzzfeed.com');
